<?php
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
error_reporting(0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" >
<head>
<?php
unset(
     //   $this->_scripts[$this->baseurl.'/media/system/js/mootools-core.js'], 
        $this->_scripts[$this->baseurl.'/media/system/js/mootools-more.js'],
        $this->_scripts[$this->baseurl.'/media/system/js/core.js'],
        $this->_scripts[$this->baseurl.'/media/system/js/caption.js']
);
if( isset($this->_script['text/javascript']) ) {
        $this->_script['text/javascript'] = preg_replace('%window\.addEvent\(\'load\',\s*function\(\)\s*{\s*new\s*JCaption\(\'img.caption\'\);\s*}\);\s*%', '', $this->_script['text/javascript']);
        if( empty($this->_script['text/javascript']) )
        unset( $this->_script['text/javascript'] );
};

$document = JFactory::getDocument();
?>
<!--[if lt IE 7]>
<script type="text/javascript" src="templates/urbis/js/unitpngfix.js"></script>
<![endif]--> 
<script type="text/javascript" src="templates/urbis/js/jquery.min.js"></script>  
<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/urbis/js/jquery-ui.min.js"></script>

 <script type="text/javascript" src="templates/urbis/js/highslide/highslide-full.js"></script>
   <link rel="stylesheet" href="templates/urbis/js/highslide/highslide.css" type="text/css" />
  <script type="text/javascript">
                        hs.graphicsDir = '<?php echo $this->baseurl ?>/templates/urbis/js/highslide/graphics/';
                        hs.align = 'center';
                        hs.transitions = ['expand', 'crossfade'];
                        hs.outlineType = 'rounded-white';
                        hs.wrapperClassName = 'dark';
                        hs.wrapperClassName = 'draggable-header';
                        hs.showCredits = false;
                        hs.fadeInOut = true;
                     
  </script>
<?php $this->setGenerator(null); ?>
 

<jdoc:include type="head" />
<script type="text/javascript">
jQuery.noConflict();
jQuery(document).ready(function(){
jQuery('.tabs #system-message').hide();
jQuery('#agreed_field').addClass("required");
jQuery('.agreedf2 #agreed_field').addClass("required");
jQuery('#enterbut').click(function(){
if(jQuery('#login-form').hasClass('active')){
jQuery('.moduletablelog #login-form').hide(180);
jQuery('.moduletablelog #login-form').removeClass('active');
}
else{
jQuery('.moduletablelog #login-form').show(350);
jQuery('.moduletablelog #login-form').addClass('active');
}  
});

jQuery('ul li.parent ul').hide();
jQuery('ul li.parent.active ul').show();
jQuery('ul.deviderul').show();		
jQuery('ul li.parent div.devider').click(function() {
if(jQuery(this).parents('li').hasClass('active')){
jQuery(this).parents('li').children('ul').slideUp(300,'linear');
jQuery(this).parents('li').removeClass('active');
}
else{
			jQuery('ul li.parent ul').slideUp(400,'linear');
			jQuery('ul li.parent').removeClass('active');
			jQuery(this).parents('li').children('ul').slideDown(500,'linear');
			jQuery(this).parents('li').children('ul.deviderchild').show();	
jQuery('ul.deviderul').show();	
			jQuery(this).parents('li').addClass('active');

}
			
		});
jQuery('#username_field').addClass('numeric');
jQuery('#address_1_field').addClass('numeric');
jQuery('#company_field').addClass('numeric');
jQuery("#email_field").addClass('email');
jQuery("#email_field_simple").addClass('email');

jQuery('.numeric').bind("change keyup input click", function() {
    if (this.value.match(/[^0-9]/g)) {
        this.value = this.value.replace(/[^0-9]/g, '');
    }
});
jQuery('.checkout-button-top a.continue_link').text("Продолжить покупки");
jQuery('.orderlistcontainer .title').text("");
jQuery('.vmorder-done-payinfo').text("Скидка: ");
jQuery('.vmorder-done .vmpayment_description').hide();
setTimeout(function(){
jQuery('.message').fadeOut(500);
},3500);
});
</script>
<link href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/layout.css" rel="stylesheet"  type="text/css" />
</head>
<body id="body">

  <div class="main">
    <div class="main-bg">
      <div class="main-width">
        <div class="header">
          <div class="logo"><a href="<?php echo $this->baseurl;?>"><img src="templates/urbis/images/logo.jpg" title="<?php echo JFactory::getConfig()->getValue('sitename');?>" /></a></div>
	  <jdoc:include type="modules" name="user3" />

<div id="ban">
	<div class="topmap">	
	<jdoc:include type="modules" name="user5" style="xhtml"/>
	</div>
<link rel="stylesheet" href="templates/urbis/cont/contactable.css" type="text/css" />
<div id="contactable"></div>
<script type="text/javascript" src="templates/urbis/cont/jquery.validate.min.js"></script>
<script type="text/javascript" src="templates/urbis/cont/jquery.contactable.js"></script>
<script type="text/javascript">jQuery(function(){jQuery('#contactable').contactable();});</script>
	
</div>
                
        </div>

        
        <div class="content">
              
          <div class="corner-left-bot"><div class="corner-right-bot"> 

<?php if($this->countmodules('right')){?>     
<div class="corner-bot">     
<div class="column-right">
<jdoc:include type="modules" name="right" style="xhtml"/>        
</div>
 <?php } ?>  

<div class="column-left">
<jdoc:include type="modules" name="left" style="xhtml"/>        
</div>
                    


<div class="column-center">

<div class="menu">
	<div class="topsearch">	
	<jdoc:include type="modules" name="position-9" />
	</div>
	<jdoc:include type="modules" name="user4" />
</div>


<jdoc:include type="modules" name="user1"/>     
<div class="post-main">
<jdoc:include type="modules" name="topmain"/>
<jdoc:include type="message" />
<jdoc:include type="component" />
</div>     
<jdoc:include type="modules" name="bottomleft" style="xhtml"/>
</div>   
     
<div style="clear: both;"> </div>


<jdoc:include type="modules" name="position-10"  style="xhtml"/>

<?php if($this->countmodules('right')){?> 
</div>
<?php } ?>
</div>
</div>
          
        </div>
        
      </div>
    </div>
<div class="footer-bg">
<div class="footer">
<div style="float: right; margin-right: 20px;"><a style="font-weight: bold; font-size: 10px; text-decoration: none;" href="http://urbis.by/" target="_blank"><img alt="urbis" src="templates/urbis/images/urbis.png" style="float: left;" />Разработка сайта <br /> Студия URBIS</a></small></div>
<jdoc:include type="modules" name="footer"/>
</div> 
</div>  </div>
  

<p id="back-top">
    <a href="#top"><span></span></a>
</p>

<script>
jQuery(document).ready(function(){
 
    jQuery("#back-top").hide();
 
    jQuery(function () {
        jQuery(window).scroll(function () {
            if (jQuery(this).scrollTop() > 400) 
                        {
                jQuery('#back-top').fadeIn();
            } else {
                jQuery('#back-top').fadeOut();
            }
        });
 

        jQuery('#back-top a').click(function () {
            jQuery('body,html').animate({
                scrollTop: 0
            }, 800); 
            return false;
        });
    });
 
});
</script>    
 <script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/urbis/js/jquery.lazyload.min.js"></script>
<script type="text/javascript">
jQuery("img.lazy").lazyload();
</script> 

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter20338570 = new Ya.Metrika({id:20338570,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/20338570" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<script type="text/javascript">


  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-39426782-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
  

</html>