/*
 * contactable 1.2.1 - jQuery Ajax contact form
 *
 * Copyright (c) 2009 Philip Beel (http://www.theodin.co.uk/)
 * Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php) 
 * and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.
 *
 * Revision: $Id: jquery.contactable.js 2010-01-18 $
 *
 */
 
//extend the plugin
(function($){

	//define the new for the plugin ans how to call it	
	$.fn.contactable = function(options) {
		//set default options  
		var defaults = {
			url: 'http://paperki.by/templates/urbis/cont/mail.php',
			name: 'Ваше имя*',
			email: 'Контактный e-mail',
			phone: 'Контактный телефон*',
			message : 'Сообщение',
			subject : 'Заказ обратного звонка с сайта',
			comment : 'Ваш комментарий к заявке',
           		page : location.href,
			submit : 'Заказать обратный звонок',
			recievedMsg : 'Благодарим Вас за обращение! </br> Наши специалисты перезвонят вам в ближайшее время!',
			notRecievedMsg : 'Извините, но при отправке письма произошла ошибка, попробуйте позже',
			disclaimer: '',
			hideOnSubmit: true

		};

		//call in the default otions
		var options = $.extend(defaults, options);
		//act upon the element that is passed into the design    
		return this.each(function() {
			//construct the form
			var this_id_prefix = '#'+this.id+' ';
			$(this).html('<div id="contactable_inner">Заказать обратный звонок</div><form id="contactForm" method="" action=""><div id="loading"></div><div id="callback"></div><div class="holder"><p><label for="name">'+options.name+'<span class="red"> * </span></label><br /><input id="name" class="contact" name="name"/></p><p><label for="phone">'+options.phone+' <span class="red"> * </span></label><br /><input id="phone" class="contact" name="phone" /></p><p><label for="comment">'+options.comment+' </label><br /><textarea id="comment" class="contact" name="comment"></textarea></p><p><input id="ntovar" class="contact" name="ntovar" type="hidden" value="'+$('#tovar').val()+'"/><input class="submit" type="submit" value="'+options.submit+'"/></p><p class="disclaimer">'+options.disclaimer+'</p></div></form>');
			//show / hide function
			$(this_id_prefix+'div#contactable_inner').toggle(function() {
				$(this_id_prefix+'#overlay').fadeIn(300);
				$(this).html('Закрыть форму'); 
				$(this_id_prefix+'#contactForm').fadeIn(300);
			}, 
			function() {
				$(this_id_prefix+'#contactForm').fadeOut(300);
				$(this).html('Заказать обратный звонок');
				$(this_id_prefix+'#overlay').fadeOut(300);
			});
			
			//validate the form 
			$(this_id_prefix+"#contactForm").validate({
				//set the rules for the fild names
				rules: {
					name: {
						required: true,
						minlength: 2
					},

					phone: {
						required: true,
						minlength: 6
					}
					
				},
				//set messages to appear inline
					messages: {
						name: "",
						phone: "",
comment: "",
						
					},			

				submitHandler: function() {
					$(this_id_prefix+'.holder').hide();
					$(this_id_prefix+'#loading').show();
$.ajax({
  type: 'POST',
  url: options.url,
  data: {subject:options.subject, page:options.page, name:$(this_id_prefix+'#name').val(), phone:$(this_id_prefix+'#phone').val(), comment:$(this_id_prefix+'#comment').val()},
  success: function(data){
						$(this_id_prefix+'#loading').css({display:'none'}); 
						if( data == 'success') {
							$(this_id_prefix+'#callback').show().append(options.recievedMsg);
							if(options.hideOnSubmit == true) {
								//hide the tab after successful submition if requested
								$(this_id_prefix+'#contactForm').animate({dummy:1}, 2000).fadeOut(800);
								$(this_id_prefix+'div#contactable_inner').animate({dummy:1}, 2000).html('Ваша заявка отправлена!'); 
								$(this_id_prefix+'#overlay').css({display: 'none'});	
							}
						} else {
							$(this_id_prefix+'#callback').show().append(options.notRecievedMsg);
							setTimeout(function(){
								$(this_id_prefix+'.holder').show();
								$(this_id_prefix+'#callback').hide().html('');
							},2000);
						}
					},
  error:function(){
						$(this_id_prefix+'#loading').css({display:'none'}); 
						$(this_id_prefix+'#callback').show().append(options.notRecievedMsg);
                                        }
});		
				}
			});
		});
	};
 
})(jQuery);
