<?php
// no direct access
defined('_JEXEC') or die('Restricted access');
$db =& JFactory::getDBO();
$query = "SELECT virtuemart_product_id FROM #__virtuemart_products WHERE  published = 1 AND product_special = 1 ORDER BY created_on DESC LIMIT 30";
$db->setQuery($query);
$ids = $db->loadResultArray();
shuffle($ids); 
if(count($ids) > 1) {
$productModel = VmModel::getModel('Product');
$producters = $productModel->getProducts ($ids, true, true, true, false);
$productModel->addImages($producters);
$categoryModel = VmModel::getModel('category');


$products_per_row = 3 ;
$verticalseparator = " vertical-separator";
$cellwidth = ' width'.floor ( 100 / $products_per_row );
?>
<script type="text/javascript" src="templates/urbis/js/jquery.jcarousel.min.js" ></script>

<script type="text/javascript">

jQuery(document).ready(function() {
    jQuery('#mycarousel').jcarousel({
auto: 5,
wrap: 'circular',
scroll: 1
    });
});

</script>

<div class="akcii">


<ul id="mycarousel" class="jcarousel-skin-simple">

<?php 
$cont = 0;
foreach ($producters as $product) {
$cont++;
if($cont < 11){
 ?>
<li>	
<?php

foreach ($product->categories as $cats)
{$category = $categoryModel->getCategory($cats);
if($category->haschildren){
}else $newcat = $category;
}

$product_sku = str_replace("m","", $product->product_sku);
////image finder
$icode = '00000000';
$len = strlen($product_sku);
if ($len == '1') {
$icode = '0000000'.$product_sku;
}else if ($len == '2') {$icode = '000000'.$product_sku;}
else if ($len == '3') {$icode = '00000'.$product_sku;}
else if ($len == '4') {$icode = '0000'.$product_sku;}
else if ($len == '5') {$icode = '000'.$product_sku;}
else if ($len == '6') {$icode = '00'.$product_sku;}
else if ($len == '7') {$icode = '0'.$product_sku;}
else if ($len == '8') {$icode =  $product_sku;}
$full_image = '';
$pas = 'images/catalog/'.$icode.'.jpg';
$pas2 = 'images/catalog/'.$icode.'.png';
$pas3 = 'images/catalog/'.$icode.'.gif';
$pas4 = 'images/catalog/'.$icode.'.jpeg';

if (file_exists($pas)) {
$full_image = $pas;
}
else if (file_exists($pas2)) {$full_image = $pas2;}
else if (file_exists($pas3)) {$full_image = $pas3;}
else if (file_exists($pas4)) {$full_image = $pas4;}
else{$full_image = 'images/catalog/noimage.jpg';}

$full_image2 = '';
$pas = 'images/catalog/'.$icode.'-1.jpg';
$pas2 = 'images/catalog/'.$icode.'-1.png';
$pas3 = 'images/catalog/'.$icode.'-1.gif';
$pas4 = 'images/catalog/'.$icode.'-1.jpeg';

if (file_exists($pas)) {
$full_image2 = $pas;
}
else if (file_exists($pas2)) {$full_image2 = $pas2;}
else if (file_exists($pas3)) {$full_image2 = $pas3;}
else if (file_exists($pas4)) {$full_image2 = $pas4;}
//////end image finder
?>

<div class="product-main center floatleft width25">
<div class="product-mainimage">
<div class="imagecontainer">
<?php 
if (round($product->prices['basePriceWithTax'],$currency->_priceConfig['salesPrice'][1]) != $product->prices['salesPrice'] && $product->prices['basePriceWithTax'] > 0) {
$sum = ($product->prices['basePriceWithTax'] - $product->prices['salesPrice'])/$product->prices['basePriceWithTax'] * 100 ;
echo '<div class="skidka">-' .round($sum,0) . '%</div> ';
}
?>

<div class="prodimage">
<a href="<?php echo JRoute::_ ( 'index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $product->virtuemart_product_id . '&virtuemart_category_id=' . $newcat->virtuemart_category_id );?>">
<img src="<?php echo $full_image; ?>"   alt="<?php echo $product->product_name ?>" class="lazy" />
</a>
</div>


</div></div>
<div class="center productinfo">


<?php

if(round($product->prices['basePrice'],$currency->_priceConfig['salesPrice'][1]) != $product->prices['salesPrice'])
{
	echo '<span style="color: #D8161F; font-size: 13px; text-decoration:line-through;" >' . $currency->createPriceDiv ('basePrice', 'COM_VIRTUEMART_PRODUCT_SALESPRICE', $product->prices, TRUE, FALSE) . "</span> ";
	echo $currency->createPriceDiv ('salesPrice', 'COM_VIRTUEMART_PRODUCT_SALESPRICE', $product->prices, TRUE, FALSE);
}
else echo $currency->createPriceDiv ('salesPrice', 'COM_VIRTUEMART_PRODUCT_SALESPRICE', $product->prices, TRUE, FALSE);

?>


<?php /* echo '<a href="'.JRoute::_ ( 'index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $product->virtuemart_product_id . '&virtuemart_category_id=' . $newcat->virtuemart_category_id).'">'; 

$db = JFactory::getDBO();
$sql = "SELECT MIN(product_price) FROM #__virtuemart_product_prices WHERE virtuemart_product_id = '".$product->virtuemart_product_id."'";
$db->setQuery($sql);
$prices = $db->loadResult();
if($prices < $product->prices['salesPrice']){
$product->prices['salesPrice'] = $prices;
echo 'от ';
echo $currency->createPriceDiv ('salesPrice', 'COM_VIRTUEMART_PRODUCT_SALESPRICE', $product->prices, TRUE, FALSE);
} 
else echo $currency->createPriceDiv ('salesPrice', 'COM_VIRTUEMART_PRODUCT_SALESPRICE', $product->prices, TRUE, FALSE);
 */?>
</a>
</div>






<div class="clear"></div>
<h3>
<?php // Product Name
//$catnam = mb_substr(strip_tags($newcat->category_name),0,45,'utf-8');
$suffix = '...';
$maxlength = 75;
$string = $newcat->category_name;	
if(strlen($string)<=$maxlength) {$temp = $string;}
else {			$string = substr($string,0,$maxlength);
			$index = strrpos($string, ' ');
			if($index===FALSE) {
				$temp = $string;
			} else {
				$temp = substr($string,0,$index).$suffix;
			}
		}
$catnam = $temp;

echo JHTML::link ( JRoute::_ ( 'index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $product->virtuemart_product_id . '&virtuemart_category_id=' . $newcat->virtuemart_category_id ), $catnam, array ('title' => $product->product_name ) ); 
$maxlength = 100;
$string = $product->product_name;
$string = str_ireplace($newcat->category_name, '', $product->product_name);
	
if(strlen($string)<=$maxlength) {$temp = $string;}
else {			$string = substr($string,0,$maxlength);
			$index = strrpos($string, ' ');
			if($index===FALSE) {
				$temp = $string;
			} else {
				$temp = substr($string,0,$index).$suffix;
			}
		}
echo $temp;
?>
</h3>
</div>





</li>
<?php	}

} ?>
</ul>
<div class="clear"></div>
</div>
<?php } ?>