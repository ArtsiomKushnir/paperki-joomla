<?php
// no direct access
defined('_JEXEC') or die('Restricted access');
$db =& JFactory::getDBO();
$query = "SELECT virtuemart_product_id FROM #__virtuemart_products WHERE  published = 1 AND product_special = 1 ORDER BY created_on DESC LIMIT 6";
$db->setQuery($query);
$ids = $db->loadResultArray();
shuffle($ids); 
$productModel = VmModel::getModel('Product');
$producters = $productModel->getProducts ($ids, true, true, true, false);
$productModel->addImages($producters);


$products_per_row = 3 ;
$verticalseparator = " vertical-separator";
$cellwidth = ' width'.floor ( 100 / $products_per_row );

// Category and Columns Counter
$col = 1;
$nb = 1;
?>
<div class="akcii">
<div class="heading">
<h3><?php echo $module->title ?></h3>
</div>

<?php if ($headerText) { ?>
	<div class="vmheader"><?php echo $headerText ?></div>
<?php }
//print_r($products);
$cont = 0;
foreach ($producters as $product) {
$cont++;
if($cont <4){


	// this is an indicator wether a row needs to be opened or not
if ($col == 1) { ?>
	<div class="row">
	<?php }
if ($nb == $products_per_row or $nb % $products_per_row == 0) {
		$show_vertical_separator = ' ';
	} else {
		$show_vertical_separator = $verticalseparator;
	}
 ?>
	
<div class="product-main center floatleft <?php echo $cellwidth;?>">
<h3>
<?php // Product Name
echo JHTML::link ( JRoute::_ ( 'index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $product->virtuemart_product_id . '&virtuemart_category_id=' . $product->virtuemart_category_id ), $product->product_name, array ('title' => $product->product_name ) ); ?>
</h3>
<div class="product-mainimage">
<?php 
if (round($product->prices['basePriceWithTax'],$currency->_priceConfig['salesPrice'][1]) != $product->prices['salesPrice']) {
$sum = ($product->prices['basePriceWithTax'] - $product->prices['salesPrice'])/$product->prices['basePriceWithTax'] * 100 ;
echo '<div class="skidka">-' .round($sum,0) . '%</div> ';
}?>
<div class="imagecontainer">
<?php // Product Image
if ($product->images) {
?>
<div class="prodimage">
<a href="<?php echo JRoute::_ ( 'index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $product->virtuemart_product_id . '&virtuemart_category_id=' . $product->virtuemart_category_id ); ?>">
<img src="<?php echo $product->images[0]->file_url_thumb; ?>" />
</a>
</div>
<?php
}
?>
</div>
<div class="center productinfo">
<?php echo '<a href="'.JRoute::_ ( 'index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $product->virtuemart_product_id . '&virtuemart_category_id=' . $product->virtuemart_category_id).'">'; 
echo $currency->createPriceDiv ('salesPrice', 'COM_VIRTUEMART_PRODUCT_SALESPRICE', $product->prices, TRUE, FALSE);
 ?>
</a>
</div>
</div>
</div>

		
	<?php
	$nb ++;

	// Do we need to close the current row now?
	if ($col == $products_per_row) { ?>
	<div class="clear"></div>
	</div>
		<?php
		$col = 1;
	} else {
		$col ++;
	}









	} 
}
if ($col != 1) { ?>
	<div class="clear"></div>
	</div>
<?php
}
	if ($footerText) : ?>
	<div class="vmfooter<?php echo $params->get( 'moduleclass_sfx' ) ?>">
		 <?php echo $footerText ?>
	</div>
<?php endif; ?>
</div>