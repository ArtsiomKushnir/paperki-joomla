<?php
// no direct access
defined('_JEXEC') or die('Restricted access');
$db =& JFactory::getDBO();
$query = "SELECT #__virtuemart_products.virtuemart_product_id FROM #__virtuemart_products LEFT JOIN #__virtuemart_product_prices ON #__virtuemart_product_prices.virtuemart_product_id = #__virtuemart_products.virtuemart_product_id WHERE published = 1 AND #__virtuemart_product_prices.override = 1 ORDER BY #__virtuemart_products.created_on DESC LIMIT 36";
$db->setQuery($query);
$ids = $db->loadResultArray();
shuffle($ids); 
$productModel = VmModel::getModel('Product');
$producters = $productModel->getProducts ($ids, true, true, true, false);
$productModel->addImages($producters);


$products_per_row = 3 ;
$verticalseparator = " vertical-separator";
$cellwidth = ' width'.floor ( 100 / $products_per_row );
?>

<script type="text/javascript">

jQuery(document).ready(function() {
    jQuery('#mycarouselsimple').jcarousel({
auto:0,
scroll:2
    });
});

</script>

<div class="akcii">
<div class="heading">
<h3><?php echo $module->title ?></h3>
</div>

<ul id="mycarouselsimple" class="jcarousel-skin-prod">

<?php 
$cont = 0;
foreach ($producters as $product) {
$cont++;
if($cont <15){
 ?>
<li>	
<div class="product-main center floatleft">
<h3>
<?php // Product Name
echo JHTML::link ( JRoute::_ ( 'index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $product->virtuemart_product_id . '&virtuemart_category_id=' . $product->virtuemart_category_id ), $product->product_name, array ('title' => $product->product_name ) ); ?>
</h3>
<div class="product-mainimage">
<?php 
if (round($product->prices['basePriceWithTax'],$currency->_priceConfig['salesPrice'][1]) != $product->prices['salesPrice']) {
$sum = ($product->prices['basePriceWithTax'] - $product->prices['salesPrice'])/$product->prices['basePriceWithTax'] * 100 ;
echo '<div class="skidka">-' .round($sum,0) . '%</div> ';
}?>
<div class="imagecontainer">
<?php // Product Image
if ($product->images) {
?>
<div class="prodimage">
<a href="<?php echo JRoute::_ ( 'index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $product->virtuemart_product_id . '&virtuemart_category_id=' . $product->virtuemart_category_id ); ?>">
<img src="<?php echo $product->images[0]->file_url_thumb; ?>" />
</a>
</div>
<?php
}
?>
</div>
<div class="center productinfo">
<?php echo '<a href="'.JRoute::_ ( 'index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $product->virtuemart_product_id . '&virtuemart_category_id=' . $product->virtuemart_category_id).'">'; 
echo $currency->createPriceDiv ('salesPrice', 'COM_VIRTUEMART_PRODUCT_SALESPRICE', $product->prices, TRUE, FALSE);
 ?>
</a>
</div>
</div>
</div>
</li>
<?php	}

} ?>
</ul>
</div>