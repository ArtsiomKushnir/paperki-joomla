<?php
/**
* @version 1.3.0
* @package RSform!Pro 1.3.0
* @copyright (C) 2007-2010 www.rsjoomla.com
* @license GPL, http://www.gnu.org/copyleft/gpl.html
*/

// no direct access
defined('_JEXEC') or die('Restricted access');
?>


<?php 
if($formId == 3 && JRequest::getVar('tema')){
?>
<script type="text/javascript">
jQuery(document).ready(function() {
jQuery('div.rsform #tema').attr('value', 'Заявка: <?php echo JRequest::getVar("tema");?>');

	});
  </script>
<?php
}
?>
<div class="rsform<?php echo $moduleclass_sfx; ?>">
	<?php echo RSFormProHelper::displayForm($formId, true); ?>
</div>