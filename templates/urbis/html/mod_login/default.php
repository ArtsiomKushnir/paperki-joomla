<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_login
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
JHtml::_('behavior.keepalive');
?>
<?php if ($type == 'logout') : ?>
<form action="<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>" method="post" id="loginout-form">

<div class="logoutbut">
<small>Ваш Логин (УНП): </small> <b><?php echo $user->get('username');?></b><br/>
</div>	
<div class="kabinet">
<a href="index.php?option=com_virtuemart&view=user&layout=edit">Ваш личный кабинет</a>
</div>
<a href="#" id="enterbut" class="logout">Выход</a>
<div id="login-form">
Вы уверены, что хотите выйти? <br/><br/>
<input type="submit" name="Submit" class="button" value="Выйти" title="<?php echo JText::_('JLOGOUT'); ?>" />
</div>




		<input type="hidden" name="option" value="com_users" />
		<input type="hidden" name="task" value="user.logout" />
		<input type="hidden" name="return" value="<?php echo $return; ?>" />
		<?php echo JHtml::_('form.token'); ?>
	
</form>
<?php else : ?>
		<?php
		$usersConfig = JComponentHelper::getParams('com_users');
		if ($usersConfig->get('allowUserRegistration')) : ?>
		<div class="loginbut">
<a href="#" id="enterbut">Войти</a>
</div>
<div class="floatleft">
 или 
</div>
		<div class="register">
<a href="<?php echo JRoute::_( 'index.php?option=com_virtuemart&view=user' ); ?>">
Зарегистрироваться
</a>
		</div>
		<?php endif; ?>
<form action="<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>" method="post" id="login-form" >

	<fieldset class="userdata">
	<div id="form-login-username">
		<input id="modlgn-username" type="text" name="username" class="inputbox"  size="18" value="Логин (УНП)"  onblur="if (this.value=='') this.value='Логин (УНП)';" onfocus="if (this.value=='Логин (УНП)') this.value='';"/>
	</div>
	<div id="form-login-password">
		<input id="modlgn-passwd" type="text" name="password" class="inputbox" size="18" value="Пароль"  onblur="if (this.value=='') {this.type = 'text'; this.value='Пароль';}" onfocus="this.type = 'password'; if (this.value=='Пароль') this.value='';"/>
	</div>
		<ul>
		<li>
			<a href="<?php echo JRoute::_('index.php?option=com_users&view=reset'); ?>">
			<?php echo JText::_('MOD_LOGIN_FORGOT_YOUR_PASSWORD'); ?></a>
		</li>

	</ul>
	<input type="submit" name="Submit" class="button" value="Войти" />
	<input type="hidden" name="option" value="com_users" />
	<input type="hidden" name="task" value="user.login" />
	<input type="hidden" name="return" value="<?php echo $return; ?>" />
	<?php echo JHtml::_('form.token'); ?>
	</fieldset>

</form>
<?php endif; ?>
