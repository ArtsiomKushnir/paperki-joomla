<?php
/**
*
* Description
*
* @package	VirtueMart
* @subpackage Manufacturer
* @author Kohl Patrick, Eugen Stranz
* @link http://www.virtuemart.net
* @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* @version $Id: default.php 2701 2011-02-11 15:16:49Z impleri $
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
    $link = JRoute::_('index.php?option=com_virtuemart&view=manufacturer&virtuemart_manufacturer_id=' .  $this->manufacturer->virtuemart_manufacturer_id);

?>
<h1><?php echo $this->manufacturer->mf_name; ?></h1>

<div class="contentpaneopen">

	

	<div class="spacer">

	
		<div class="manufacturer-image">
		<?php 

$skp = '000000';
$len = strlen($this->manufacturer->virtuemart_manufacturer_id);
if ($len == '1') {
$skp = '00000'.$this->manufacturer->virtuemart_manufacturer_id;
}
else if ($len == '2') {$skp = '0000'.$this->manufacturer->virtuemart_manufacturer_id;}
else if ($len == '3') {$skp = '000'.$this->manufacturer->virtuemart_manufacturer_id;}
else if ($len == '4') {$skp = '00'.$this->manufacturer->virtuemart_manufacturer_id;}
else if ($len == '5') {$skp = '0'.$this->manufacturer->virtuemart_manufacturer_id;}
else if ($len == '6') {$skp = $this->manufacturer->virtuemart_manufacturer_id;}

$mf_image = '';
$pas = 'images/brands/'.$skp.'.jpg';
$pas2 = 'images/brands/'.$skp.'.png';
$pas3 = 'images/brands/'.$skp.'.gif';
$pas4 = 'images/brands/'.$skp.'.jpeg';

if (file_exists($pas)) {
$mf_image = $pas;
}
else if (file_exists($pas2)) {$mf_image = $pas2;}
else if (file_exists($pas3)) {$mf_image = $pas3;}
else if (file_exists($pas4)) {$mf_image = $pas4;}

if($mf_image){


 ?>
<img src="<?php echo $mf_image;?>" alt="<?php echo $this->mf_name; ?>" title="<?php echo $this->mf_name; ?>"/>
<?php } ?>
		</div>
	

	<?php // Manufacturer Email
	if(!empty($this->manufacturer->mf_email)) { ?>
		<div class="manufacturer-email">
		<?php // TO DO Make The Email Visible Within The Lightbox
		echo JHtml::_('email.cloak', $this->manufacturer->mf_email,true,JText::_('COM_VIRTUEMART_EMAIL'),false) ?>
		</div>
	<?php } ?>

	<?php // Manufacturer URL
	if(!empty($this->manufacturer->mf_url)) { ?>
		<div class="manufacturer-url">
			<a target="_blank" href="<?php echo $this->manufacturer->mf_url ?>"><?php echo JText::_('COM_VIRTUEMART_MANUFACTURER_PAGE') ?></a>
		</div>
	<?php } ?>

	<?php // Manufacturer Description
	if(!empty($this->manufacturer->mf_desc)) { ?>
		<div class="manufacturer-description">
			<?php echo $this->manufacturer->mf_desc ?>
		</div>
	<?php } ?>
</br>
	<?php // Manufacturer Product Link
	$manufacturerProductsURL = JRoute::_('index.php?option=com_virtuemart&view=category&virtuemart_manufacturer_id=' . $this->manufacturer->virtuemart_manufacturer_id);

	if(!empty($this->manufacturer->virtuemart_manufacturer_id)) { ?>
</br>
		<div class="manufacturer-product-link">
			<a target="_top" href="<?php echo $manufacturerProductsURL; ?>"><?php echo JText::sprintf('COM_VIRTUEMART_PRODUCT_FROM_MF',$this->manufacturer->mf_name); ?></a>
		</div>
	<?php } ?>

	<div class="clear"></div>
	</div>
</div>