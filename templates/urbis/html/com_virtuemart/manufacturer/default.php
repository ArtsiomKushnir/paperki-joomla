<?php
/**
*
* Description
*
* @package	VirtueMart
* @subpackage Manufacturer
* @author Kohl Patrick, Eugen Stranz
* @link http://www.virtuemart.net
* @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* @version $Id: default.php 2701 2011-02-11 15:16:49Z impleri $
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

// Category and Columns Counter
$iColumn = 1;
$iManufacturer = 1;

// Calculating Categories Per Row
$manufacturerPerRow = 6;
if ($manufacturerPerRow != 1) {
	$manufacturerCellWidth = ' width'.floor ( 100 / $manufacturerPerRow );
} else {
	$manufacturerCellWidth = '';
}

// Separator
$verticalSeparator = " vertical-separator";
$horizontalSeparator = '<div class="horizontal-separator"></div>';

// Lets output the categories, if there are some
if (!empty($this->manufacturers)) { ?>

<h1>Все бренды</h1>
<div class="manufacturer-view-default">

	<?php // Start the Output
	foreach ( $this->manufacturers as $manufacturer ) {

$skp = '000000';
$len = strlen($manufacturer->virtuemart_manufacturer_id);
if ($len == '1') {
$skp = '00000'.$manufacturer->virtuemart_manufacturer_id;
}
else if ($len == '2') {$skp = '0000'.$manufacturer->virtuemart_manufacturer_id;}
else if ($len == '3') {$skp = '000'.$manufacturer->virtuemart_manufacturer_id;}
else if ($len == '4') {$skp = '00'.$manufacturer->virtuemart_manufacturer_id;}
else if ($len == '5') {$skp = '0'.$manufacturer->virtuemart_manufacturer_id;}
else if ($len == '6') {$skp = $manufacturer->virtuemart_manufacturer_id;}

$mf_image = '';
$pas = 'images/brands/'.$skp.'.jpg';
$pas2 = 'images/brands/'.$skp.'.png';
$pas3 = 'images/brands/'.$skp.'.gif';
$pas4 = 'images/brands/'.$skp.'.jpeg';

if (file_exists($pas)) {
$mf_image = $pas;
}
else if (file_exists($pas2)) {$mf_image = $pas2;}
else if (file_exists($pas3)) {$mf_image = $pas3;}
else if (file_exists($pas4)) {$mf_image = $pas4;}

		// this is an indicator wether a row needs to be opened or not
		if ($iColumn == 1) { ?>
		<div class="row">
		<?php }

		// Show the vertical seperator
		if ($iManufacturer == $manufacturerPerRow or $iManufacturer % $manufacturerPerRow == 0) {
			$showVerticalSeparator = ' ';
		} else {
			$showVerticalSeparator = $verticalSeparator;
		}

		// Manufacturer Elements
		$manufacturerURL = JRoute::_('index.php?option=com_virtuemart&view=manufacturer&virtuemart_manufacturer_id=' . $manufacturer->virtuemart_manufacturer_id);
		$manufacturerIncludedProductsURL = JRoute::_('index.php?option=com_virtuemart&view=category&virtuemart_manufacturer_id=' . $manufacturer->virtuemart_manufacturer_id);
		$manufacturerImage = $manufacturer->images[0]->displayMediaThumb("",false);

		// Show Category ?>
		<div class="manufacturer floatleft<?php echo $showVerticalSeparator ?>">
			<div class="spacer">


<?php if($mf_image){?>
<a title="<?php echo $manufacturer->mf_name; ?>" href="<?php echo $manufacturerURL; ?>"><img src="<?php echo $mf_image;?>" alt="<?php echo $manufacturer->mf_name; ?>" title="<?php echo $manufacturer->mf_name; ?>"/></a>
<?php } else {
 ?>
<h2>
<a title="<?php echo $manufacturer->mf_name; ?>" href="<?php echo $manufacturerURL; ?>"><?php echo $manufacturer->mf_name; ?></a>
</h2>
<?php } ?>
							</div>
		</div>
		<?php
		$iManufacturer ++;

		// Do we need to close the current row now?
		if ($iColumn == $manufacturerPerRow) {
			echo '<div class="clear"></div></div>';
			$iColumn = 1;
		} else {
			$iColumn ++;
		}
	}

	// Do we need a final closing row tag?
	if ($iColumn != 1) { ?>
		<div class="clear"></div>
	</div>
	<?php } ?>

</div>
<?php
}
?>
<br/>