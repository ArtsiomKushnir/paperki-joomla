<?php
defined('_JEXEC') or die('');

/**
*
* Template for the shopping cart
*
* @package	VirtueMart
* @subpackage Cart
* @author Max Milbers
*
* @link http://www.virtuemart.net
* @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
*/

echo "<h3>".JText::_('COM_VIRTUEMART_CART_ORDERDONE_THANK_YOU')."</h3>";

require_once(JPATH_VM_ADMINISTRATOR.DS.'models'.DS.'orders.php');
$order = VirtueMartModelOrders::getOrder($this->cart->virtuemart_order_id);

	$session =& JFactory::getSession();
    $couponorgift = $session->get('couponorgift', 1);
    $baseforskidka = $order['details']['BT']->order_total;
    $orderNumber = $order['details']['BT']->order_number;
    if ($couponorgift == 1 && $baseforskidka >= 100)
    {
    	$procskid = 1; 
    	if($baseforskidka > 200)
    	{
    		$procskid = 2;
    	}
    	if($baseforskidka > 400)
    	{
    		$procskid = 3;
    	}
    	if($baseforskidka > 650)
    	{
    		$procskid = 4;
    	}
    	if($baseforskidka > 950)
    	{
    		$procskid = 5;
    	}
    	if($baseforskidka > 1300)
    	{
    		$procskid = 6;
    	}
    	if($baseforskidka > 1650)
    	{
    		$procskid = 7;    		
    	}
    	if($baseforskidka >= 2000)
    	{
    		$procskid = 8; // размер скидки %
    	}

    	$couponsumm = round($baseforskidka*$procskid/100, 2); // размер скидки в рублевом выражении

    	///////////////////////////////////////////////////////////////////////////////////////////////////
    	$db =& JFactory::getDBO();
    	//------------------------------------------------------------------------------------------------
    	$query = $db->getQuery(true);    
    	
		$columns = 'coupon_discount';
		$values = $couponsumm* -1;

		$query
    		->update($db->quoteName('#__virtuemart_orders'))
    		->set($db->quoteName($columns).' = '.$db->quote($values))
    		->where($db->quoteName('order_number').' = '.$db->quote($orderNumber));

    	$db->setQuery($query);
		$db->execute();
		//////////////////////////////////////////////////////////////////////////////////////////////////
		$query = $db->getQuery(true);    
    	
		$columns = 'order_salesPrice';
		$values = $baseforskidka;

		$query
    		->update($db->quoteName('#__virtuemart_orders'))
    		->set($db->quoteName($columns).' = '.$db->quote($values))
    		->where($db->quoteName('order_number').' = '.$db->quote($orderNumber));

    	$db->setQuery($query);
		$db->execute();
		//////////////////////////////////////////////////////////////////////////////////////////////////
		$query = $db->getQuery(true);    
    	
		$columns = 'order_total';
		$values = $baseforskidka - $couponsumm;

		$query
    		->update($db->quoteName('#__virtuemart_orders'))
    		->set($db->quoteName($columns).' = '.$db->quote($values))
    		->where($db->quoteName('order_number').' = '.$db->quote($orderNumber));

    	$db->setQuery($query);
		$db->execute();
		///////////////////////////////////////////////////////////////////////////////////////////////////

    }
    if ($couponorgift == 2) 
	{

    }
echo $this->html;

if (!class_exists('VirtueMartCart')) {require(JPATH_VM_SITE . DS . 'helpers' . DS . 'cart.php');}
$carter = VirtueMartCart::getCart();

//$session->set('couponorgift', 0); // обнуление переключателя скидки или подарка
$session->set('selectedgiftprod', 0); // обнуление подарка
$dourbiscupon = $carter->setCouponCode('noller'); // обнуление купона на скидку

echo '<br/><br/>Наши специалисты свяжутся с вами в ближайшее время!<br/> Если у вас возникли вопросы при оформлении заказа, свяжитесь с нами по телефонам: +375 (162) 42-63-12, 42-84-01';