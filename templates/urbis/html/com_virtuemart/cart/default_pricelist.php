<?php defined ('_JEXEC') or die('Restricted access');
/**
 *
 * Layout for the shopping cart
 *
 * @package    VirtueMart
 * @subpackage Cart
 * @author Max Milbers
 * @author Patrick Kohl
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 *
 */

// Check to ensure this file is included in Joomla!

// jimport( 'joomla.application.component.view');
// $viewEscape = new JView();
// $viewEscape->setEscape('htmlspecialchars');

$session =& JFactory::getSession();
if($session->get('refreshpage') == 'yeah') {
	$mainframe = JFactory::getApplication();
	$session =& JFactory::getSession();
	$session->set('refreshpage', 'nono');
	$mainframe->redirect(JRoute::_('index.php?option=com_virtuemart&view=cart'));
};

$couponorgift = $session->get('couponorgift', 1);
// -------------- Скидка PERFORMER -------------------------------------------
	$checkBaseSkidkaToPerformer = $session->get('BaseSkidkaToPerformer', 1);
	$is9491 = false;
	$summa9491 = 0;
	$summaNo9491 = 0;
// ---------------------------------------------------------------------------

$catModel = VmModel::getModel('Category');
vmJsApi::js( 'fancybox/jquery.fancybox-1.3.4.pack');
vmJsApi::css('jquery.fancybox-1.3.4');
$document = JFactory::getDocument ();
$imageJS = '
jQuery(document).ready(function() {
	jQuery("a[rel=vm-additional-images]").fancybox({
		"titlePosition" 	: "inside",
		"transitionIn"	:	"elastic",
		"transitionOut"	:	"elastic"
	});
});
';
$document->addScriptDeclaration ($imageJS);
?>

<fieldset>
	<table class="cart-summary width100 product-table">
		<tr>
			<th width="30px">Фото</th>
			<th align="center" width="50px">Код</th>
			<th align="left" ><?php echo JText::_('COM_VIRTUEMART_CART_NAME') ?></th>
			<th align="center" width="120px">Цена с НДС</th>
			<th align="center" width="130px">Количество</th>
			<?php 
				if ( VmConfig::get('show_tax')) 
				{ 
			?>
			<th align="right" width="100px">
				<?php  echo "<span  class='priceColor2'>".JText::_('COM_VIRTUEMART_CART_SUBTOTAL_TAX_AMOUNT') ?>
			</th>

			<?php 
				} ?>
			<th align="right" width="120px"><?php echo JText::_('COM_VIRTUEMART_CART_TOTAL') ?> &nbsp;</th>
		</tr>

<?php
	$sumskidka = 0.0;
	$baseSkidkaToPerformer = 0.0;
	foreach ($this->cart->products as $pkey => $prow)
	{
		$product_sku = str_replace("m","", $prow->product_sku);
		//image finder---------------------------------------------
		$icode = '00000000';
		$len = strlen($product_sku);
		if ($len == '1') {$icode = '0000000'.$product_sku;}
		else if ($len == '2') {$icode = '000000'.$product_sku;}
		else if ($len == '3') {$icode = '00000'.$product_sku;}
		else if ($len == '4') {$icode = '0000'.$product_sku;}
		else if ($len == '5') {$icode = '000'.$product_sku;}
		else if ($len == '6') {$icode = '00'.$product_sku;}
		else if ($len == '7') {$icode = '0'.$product_sku;}
		else if ($len == '8') {$icode =  $product_sku;}
		$full_image = '';
		$pas = 'images/catalog/'.$icode.'.jpg';
		$pas2 = 'images/catalog/'.$icode.'.png';
		$pas3 = 'images/catalog/'.$icode.'.gif';
		$pas4 = 'images/catalog/'.$icode.'.jpeg';
		if (file_exists($pas)) {$full_image = $pas;}
		else if (file_exists($pas2)) {$full_image = $pas2;}
		else if (file_exists($pas3)) {$full_image = $pas3;}
		else if (file_exists($pas4)) {$full_image = $pas4;}
		else{$full_image = 'images/catalog/noimage.jpg';}
		$full_image2 = '';
		$pas = 'images/catalog/'.$icode.'-1.jpg';
		$pas2 = 'images/catalog/'.$icode.'-1.png';
		$pas3 = 'images/catalog/'.$icode.'-1.gif';
		$pas4 = 'images/catalog/'.$icode.'-1.jpeg';
		if (file_exists($pas)) {$full_image2 = $pas;}
		else if (file_exists($pas2)) {$full_image2 = $pas2;}
		else if (file_exists($pas3)) {$full_image2 = $pas3;}
		else if (file_exists($pas4)) {$full_image2 = $pas4;}
		//end image finder-------------------------------------------
		$newcat = $catModel->getCategory(max($prow->categories));
		// -------------- Скидка PERFORMER -------------------------------------------
		// получаем родительскую категорию -------------------------
			$db = JFactory::getDBO();
			$sql = "SELECT category_parent_id FROM #__virtuemart_category_categories WHERE id = '".$newcat->virtuemart_category_id."'";
			$db->setQuery($sql);
			$parentcategory = $db->loadResult(); // родительская категория
		// ---------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------------
		echo '<tr valign="top" class="sectiontableentry1">';
		// изображение товара ----------------------------------------------------------------------------------------------
			echo '<td class="product-tableimage"  width="30px">';
				echo '<a href="'.$this->baseurl.'/images/catalog/'.$icode.'.jpg" onclick="return hs.expand(this,{captionText: this.getAttribute('."'".'caption'."'".'), slideshowGroup:'."'".'group'.$prow->virtuemart_category_id."'".'})" class="highslide" title="'.$newcat->category_name.' '.$prow->product_name.'"  caption="'.$newcat->category_name.' '.$prow->product_name.'" target="_blank"><img src="templates/urbis/images/grey.gif" data-original="'.$this->baseurl.'/'.$full_image.'"alt="'.$prow->product_name.'" class="lazy" /></a>';
			echo '</td>';
		// код товара --------------------------------------------------------------------------------------------------------
			echo '<td align="center">'.substr($icode, -5, 5).'</td>';
			echo '<td align="left">';
				echo '<h3>';
		// --- Наименование товара -------------------------------------------------------------------------------------------
				echo JHTML::link ( JRoute::_ ( 'index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id='.$prow->virtuemart_product_id.'&virtuemart_category_id='.$newcat->virtuemart_category_id ), $newcat->category_name, array ('title' => $newcat->category_name.' '.$prow->product_name));
				echo '<br/>'.$prow->product_name." ".$prow->customfields;
				echo '</h3>';
			echo '</td>';
		// --- Цена товара ---------------------------------------------------------------------------------------------------
		// проверка на наличие скидок или диапазона цен у товара -------------------------------------------------------------

				$sql = "SELECT MAX(product_price) FROM #__virtuemart_product_prices WHERE virtuemart_product_id = '".$pkey."'";
				$db->setQuery($sql);
				$prices = $db->loadResult();
				$skidkaclass= '';
				if($prices > $this->cart->pricesUnformatted[$pkey]['salesPrice'])
				{
					$skidkaclass = ' class="yellow"';
				}
				if ($this->cart->pricesUnformatted[$pkey]['basePrice'] != $this->cart->pricesUnformatted[$pkey]['salesPrice'])
				{
					$skidkaclass = ' class="yellow"';
				}
			echo '<td align="center"'.$skidkaclass.'">';
		// цена товара -------------------------------------------------------------------------------------------------------


				// -------------- Скидка PERFORMER ----------------------------------------------------------------------------
				// скидка на бумагу Xerox Performer A4 ---------------------------------------------------------------------
				
					if (($product_sku == '9491')) {
						$is9491 = true;
						if (($couponorgift == 3) && ($checkBaseSkidkaToPerformer >= 70)) {
							$this->cart->pricesUnformatted[$pkey]['salesPrice'] = 6; // меняет отображаемую цену
							$this->cart->pricesUnformatted[$pkey]['priceWithoutTax'] = 6; // меняет цену
							$this->cart->pricesUnformatted[$pkey]['salesPriceTemp'] = 6;
							$this->cart->pricesUnformatted[$pkey]['priceBeforeTax'] = 6;
							$this->cart->pricesUnformatted[$pkey]['discountedPriceWithoutTax'] = 6;
							$this->cart->pricesUnformatted[$pkey]['basePriceVariant'] = 6;
							//$this->cart->pricesUnformatted[$pkey]['basePrice'] = 6;	
							$this->cart->pricesUnformatted[$pkey]['costPrice'] = 6;
							$this->cart->pricesUnformatted[$pkey]['subtotal_with_tax'] = 6;
							$this->cart->pricesUnformatted[$pkey]['subtotal'] = 6;


							$this->cart->products[$pkey]->product_price = 6;
							$this->cart->products[$pkey]->prices[0]['product_price'] = 6;


							$session->set('skidkaPerformerStatus', true);
							$this->cart->couponCode = 'skidkaXEROXPerformerA4';
						} else {
							$session->set('skidkaPerformerStatus', false);
							$this->cart->couponCode = 'noller';
						}
					}
				
				// ----------------------------------------------------------------------------------------------------------


				if ($this->cart->pricesUnformatted[$pkey]['basePrice'] != $this->cart->pricesUnformatted[$pkey]['salesPrice'])
				{
					// ----- поле цены если есть скидка на товар  -----------
					echo '<span style="color: #D8161F; font-size: 13px; text-decoration:line-through;" >' . $this->currencyDisplay->createPriceDiv('basePrice', '', $this->cart->pricesUnformatted[$pkey], FALSE) . "</span> ";
				}
				if($prices > $this->cart->pricesUnformatted[$pkey]['basePrice'] && $session->get('skidkaPerformerStatus', 0) === false)
				{
					// ----- поле цены если есть диапазоны цен -----------
					echo '<span style="color: #D8161F; font-size: 13px; text-decoration:line-through;" ><div class="PricebasePrice" style="display : block;"><span class="PricebasePrice">'.number_format($prices, 2, ',', '').' р.</span></div></span>';
				}
				// ------ цена в корзине -------------

				echo $this->currencyDisplay->createPriceDiv ('salesPrice', '', $this->cart->pricesUnformatted[$pkey], FALSE);

				if($prices > $this->cart->pricesUnformatted[$pkey]['basePrice'])
				{	
					// информер оптовой скидки----------------------------------------------------------
					if ($session->get('skidkaPerformerStatus', 0) === true) {
						
					} else {
						echo '<span class="help" style="float: right; margin-top: -17px;"">';
						$sqlp = "SELECT product_price, price_quantity_start, price_quantity_end FROM #__virtuemart_product_prices WHERE virtuemart_product_id = '".$pkey."' ORDER BY product_price DESC";
						$db->setQuery($sqlp);
						$prices = $db->loadObjectList();
						if(count($prices) > 1)
						{
							echo '<div class="mprices"><div class="title"><strong>Цена зависит от количества!</strong></div><table cellpadding="0" cellspacing="0" width="100%">';
							foreach ($prices as $dprice)
							{
								echo '<tr><td class="help"><span>';
								if($dprice->price_quantity_start == 0 && $dprice->price_quantity_end > 0) echo 'от 1 ед.: </span></td><td>'.number_format($dprice->product_price, 2, ',', '').' р. <span class="denominprice">('. number_format(($dprice->product_price*10000), 0,' ',' ').' р.)</span></td></tr>';
								if($dprice->price_quantity_start > 0 && $dprice->price_quantity_end > 0) echo 'от '.$dprice->price_quantity_start.' ед.: </span></td><td>'.number_format($dprice->product_price, 2, ',', '').' р. <span class="denominprice">('. number_format(($dprice->product_price*10000), 0,' ',' ').' р.)</span></td></tr>';
								if($dprice->price_quantity_start > 0 && $dprice->price_quantity_end == 0) echo 'от '.$dprice->price_quantity_start.' ед.: </span></td><td>'.number_format($dprice->product_price, 2, ',', '').' р. <span class="denominprice">('. number_format(($dprice->product_price*10000), 0,' ',' ').' р.)</span></td></tr>';
							}
							echo '</table></div>';
						}
						echo '</span>';
					}
				}
				// -------------- вывод цена в корзине до деноминации -----------------
				//echo '</br><span class="denominprice">'. number_format(($this->cart->pricesUnformatted[$pkey]['salesPrice']*10000), 0,' ',' ').' р.</span>';
				
			echo '</td>';
			echo '<td align="right">';
				if ($prow->step_order_level) $step=$prow->step_order_level;
				else $step=1;
				if($step==0) $step=1;
				$alert=JText::sprintf ('Только целые числа!', $step);
			
				echo '<script type="text/javascript">function check'.$step.'(obj) {remainder=obj.value % '.$step.';quantity=obj.value;if (remainder != 0) {alert('."'".$alert.'!'."'".');obj.value = quantity-remainder;return false;}return true;}</script>';
				echo '<form action="'.JRoute::_ ('index.php').'" method="post" class="inline">';
				echo '<input type="hidden" name="option" value="com_virtuemart"/>';
				echo '<input type="hidden" name="quantitylast" value="'.$prow->quantity.'" />';

				echo '<!--<input type="text" title="'.JText::_('COM_VIRTUEMART_CART_UPDATE').'" class="inputbox" size="3" maxlength="4" name="quantity" value="'.$prow->quantity.'" /> -->';
                echo '<input type="text" onblur="check'.$step.'(this);" onclick="check'.$step.'(this);" onchange="check'.$step.'(this);" onsubmit="check('.$step.'this);" title="'.JText::_('COM_VIRTUEMART_CART_UPDATE').'" class="inputbox quantity-input js-recalculate" size="3" maxlength="4" name="quantity" value="'.$prow->quantity.'" />';
				echo '<input type="hidden" name="view" value="cart"/>';
				echo '<input type="hidden" name="task" value="update"/>';
				echo '<input type="hidden" name="cart_virtuemart_product_id" value="'.$prow->cart_item_id.'"/>';
				echo '<input type="submit" class="vmicon vm2-add_quantity_cart" name="update" title="'.JText::_ ('COM_VIRTUEMART_CART_UPDATE').'" align="middle" value=" "/>';
				echo '</form>';
				echo '<a class="vmicon vm2-remove_from_cart" title="'.JText::_ ('COM_VIRTUEMART_CART_DELETE').'" align="middle" href="'.JRoute::_ ('index.php?option=com_virtuemart&view=cart&task=delete&cart_virtuemart_product_id=' . $prow->cart_item_id).'"></a>';
			echo '</td>';
			// сумма товара ----------------------------------------------------------------------------------------------
			echo '<td colspan="1" align="right">';
				echo '<h3 class="superheader">';
						if (VmConfig::get ('checkout_show_origprice', 1) && !empty($this->cart->pricesUnformatted[$pkey]['basePriceWithTax']) && $this->cart->pricesUnformatted[$pkey]['basePriceWithTax'] != $this->cart->pricesUnformatted[$pkey]['salesPrice']) 
						{
							echo '<span class="line-through">'. $this->currencyDisplay->createPriceDiv ('basePriceWithTax', '', $this->cart->pricesUnformatted[$pkey], TRUE, FALSE, $prow->quantity).'</span><br />';
						}

						echo $this->currencyDisplay->createPriceDiv ('salesPrice', '', $this->cart->pricesUnformatted[$pkey], FALSE, FALSE, $prow->quantity);
						
						// накапливаем сумму для подсчета общей скидки
						$sumskidka += $this->cart->pricesUnformatted[$pkey]['subtotal'];
						// -------------- Скидка PERFORMER ----------------------------------------------------------------------------
						// если товар не из категории 5952 (БУМАГА ОФИСНАЯ) накапливаем сумму для подсчета скидки на Xerox Performer
						if ($parentcategory != '5952') {
							$baseSkidkaToPerformer += $this->cart->pricesUnformatted[$pkey]['subtotal'];
						}

						if ($product_sku == '9491') {
							$summa9491 += $this->cart->pricesUnformatted[$pkey]['salesPrice'] * $prow->quantity;
						} else {$summaNo9491 += $this->cart->pricesUnformatted[$pkey]['salesPrice'] * $prow->quantity;}
						// ------------------------------------------------------------------------------------------------------------
				echo '</h3>';
			echo '</td>';
		echo '</tr>';
	}
	// ----- ИТОГО ---------------------------------------------------------------
		echo '<tr class="sectiontableentry3">';
			echo '<td colspan="5" align="right"><strong>Итого :</strong></td>';
			echo '<td align="right" >';
			// -------------- Скидка PERFORMER ----------------------------------------------------------------------------
			if ($couponorgift == 3 && $is9491 && $baseSkidkaToPerformer >= 70) {
				$this->cart->pricesUnformatted['salesPrice'] = $summaNo9491 + $summa9491;
				echo $this->currencyDisplay->createPriceDiv ('salesPrice', '', $this->cart->pricesUnformatted, FALSE);
				$baseforskidka = $sumskidka;
			} else {
			// ------------------------------------------------------------------------------------------------------------
				echo $this->currencyDisplay->createPriceDiv ('salesPrice', '', $this->cart->pricesUnformatted, FALSE);
				$baseforskidka = $sumskidka;
			}
			echo '</td>';
		echo '</tr>';
?>
	</table>

<?php //------------------------------------------- ОПЛАТА ------------------------------------------------------------------------------------------- -?>

<?php
	if ($this->cart->pricesUnformatted['salesPrice'] > 0)
	{
		echo '<div class="category moduletableclear" style="padding: 10px 15px 3px 15px; margin: 10px 3px 0 3px;">';
			echo '<div class="floatleft width10"><img src="templates/urbis/images/payment.png" style="float: left; margin-right: 25px;" /></div>';
			echo '<div class="floatright width90">Способ оплаты: <strong>'.$this->cart->cartData['paymentName'].'</strong>';
			












			
			if (!empty($this->layoutName) && $this->layoutName == 'default')
			{
				echo JHTML::_('link', JRoute::_('index.php?view=cart&task=editpayment', $this->useXHTML, $this->useSSL), $this->select_payment_text, 'class=""');
			}
			echo '</div>';
			echo '<div class="clear"><br/></div>';
		echo '</div>';
	}
// ----------------------------------------- ДОСТАВКА ---------------------------------------------------------------------------------------------
	
	if ($this->cart->pricesUnformatted['salesPrice'] > 0)
	{
		echo '<div class="category moduletable dostavka" style="padding: 7px 15px; margin: 0 3px; overflow: auto;">';
			echo '<div class="floatleft width10"><img src="templates/urbis/images/shipment.png" style="float: left; margin-right: 25px;" /></div>';
			echo '<div class="floatright width90"><p><a class="highslide" href="index.php?option=com_content&view=article&id=284&tmpl=component" onclick="return hs.htmlExpand( this, { objectType: '."'".'iframe'."'".', outlineType: '."'".'rounded-white'."'".', width: 700 } )">';
			if(strlen($this->cart->cartData['shipmentName']) > 5)
			{
				echo $this->cart->cartData['shipmentName'];
			}
			else 
			{
				echo '<span class="vmshipment_description">Для вас доступно несколько вариантов доставки </span>';
			}
			echo '<span class="help"> </span></a></p>';
			if(strlen($this->cart->cartData['shipmentName']) > 5)
			{
				$changedostavlink = 'Изменить способ доставки';
			}
			else $changedostavlink = 'Выбрать способ доставки';
			echo JHTML::_('link', JRoute::_('index.php?view=cart&task=edit_shipment', $this->useXHTML, $this->useSSL), $changedostavlink , 'class=""');
			echo '</div>';
			echo '<div class="clear"><br/></div>';
		echo '</div>';
	}
?>
<?php //- ---------------------------------------СКИДКИ ------------------------------------------------------------------------------------------ ?>
 		<div class="category moduletable" style="padding: 10px 15px; margin: 0 3px;">
 			<img src="templates/urbis/images/gift.png" style="float: left; margin-right: 21px;" />
 				<h3>Скидки и бонусы</h3>
 					<?php

 						echo '<span class="vmshipment_description">Для вашего заказа доступны следующие бонусы:</span>';

 						echo '<div id="ui-tabs">';
 							echo '<ul id="tabs">';
 								// вкладка скидка на весь заказ от сммы в корзине
 								echo '<li ';
 								if($couponorgift == 1)
 								{
 									echo 'class="current"';
 								}
 								echo '><a href="'.JRoute::_ ( 'index.php?option=com_virtuemart&view=cart&couponorgift=1').'">Скидка на весь заказ</a></li>';

 								/*

 								echo '<li ';
 								// вкладка Акция на Xerox Performer A4 по 6 р. с НДС
 								if($couponorgift == 3)
 								{
 									echo 'class="current"';
 								}
 								echo '><a href="'.JRoute::_ ( 'index.php?option=com_virtuemart&view=cart&couponorgift=3').'">Скидка на Xerox Performer</a></li>';
 								// подарки к заказу.
 								/*echo '<li ';
 								if($couponorgift == 2)
 								{
 									echo 'class="current"';
 								}
 								echo '><a href="'.JRoute::_ ( 'index.php?option=com_virtuemart&view=cart&couponorgift=2').'">Получить подарок</a></li>'; */							
 							echo '</ul>';

 							if ($couponorgift == 1)
 							{
 								if ($baseforskidka < 100) { ?>
 									<div style="display: block;  border: 1px solid #D6E8F5; padding: 20px; min-height: 35px;">
		 									<a class="highslide" onclick="return hs.htmlExpand( this, { objectType: 'iframe', outlineType: 'rounded-white', width: 700 } )" href="index.php?option=com_content&view=article&id=287&tmpl=component">
				 								<span class="vmpayment_name">Скидка на заказ недоступна</span>
				 									<span class="vmpayment_description">
				 										<?php if($baseforskidka < 100)
				 										{
				 											$procskid = 1;
				 											$kolskid = number_format((100 - $baseforskidka), 2, ',', '');
				 										} ?>
				 				(для получения скидки <?php echo $procskid; ?>%, дополните корзину товаром на сумму <?php echo $kolskid;?> р.)
				 									</span>
				 								<span class="help"></span>
				 							</a>
		 							</div>
 								<?php } else { ?>
 							<div style="display: block;  border: 1px solid #D6E8F5; padding: 20px; min-height: 35px;">
 								<div style="display: inline-block; float: left; width: 80%; text-align: left;">
 									<a class="highslide" onclick="return hs.htmlExpand( this, { objectType: 'iframe', outlineType: 'rounded-white', width: 700 } )" href="index.php?option=com_content&view=article&id=287&tmpl=component">
 									<?php $procskid = 1;
 										if($baseforskidka >= 100)
 										{
 											$procskid = 1;
 											$kolskid = number_format((200 - $baseforskidka), 2, ',', '')." р.";
 											$this->cart->couponCode = 'skidka1';
 										}
 										if($baseforskidka > 200)
 										{
 											$procskid = 2;
 											$kolskid = number_format((400 - $baseforskidka), 2, ',', '')." р.";
 											$this->cart->couponCode = 'skidka2';
 										}
 										if($baseforskidka > 400)
 										{
 											$procskid = 3;
 											$kolskid = number_format((650 - $baseforskidka), 2, ',', '')." р.";
 											$this->cart->couponCode = 'skidka3';
 										}
 										if($baseforskidka > 650)
 										{
 											$procskid = 4;
 											$kolskid = number_format((950 - $baseforskidka), 2, ',', '')." р.";
 											$this->cart->couponCode = 'skidka4';
 										}
 										if($baseforskidka > 950)
 										{
 											$procskid = 5;
 											$kolskid = number_format((1300 - $baseforskidka), 2, ',', '')." р.";
 											$this->cart->couponCode = 'skidka5';
 										}
 										if($baseforskidka > 1300)
 										{
 											$procskid = 6;
 											$kolskid = number_format((1650 - $baseforskidka), 2, ',', '')." р.";
 											$this->cart->couponCode = 'skidka6';
 										}
 										if($baseforskidka > 1650)
 										{
 											$procskid = 7;
 											$kolskid = number_format((2000 - $baseforskidka), 2, ',', '')." р.";
 											$this->cart->couponCode = 'skidka7';
 										}
 										if($baseforskidka >= 2000)
 										{
 											$procskid = 8;
 											$this->cart->couponCode = 'skidka8';
 										} ?>

 										<span class="vmpayment_name">Скидка от суммы заказа: <?php echo $procskid.'%'; ?></span>
 										<span class="vmpayment_description">
 										<?php if($procskid == 8)
 											{
 												echo '(максимальный уровень скидки)';
 											}
 											else 
 											{
 												echo '(для получения скидки '.($procskid+1) .'%, дополните корзину товаром на сумму '.$kolskid.')';
 											} ?>
 										</span>
 										<span class="help"></span>
 									</a>
 								</div>
 								<div style="text-align: right; width: 20%; display: inline-block; float: right; margin-top: 7px;">
 									<span style="font: 19px Arial; color: #222222; text-align: right;"> <?php
 										$couponsumm = $baseforskidka*$procskid/100;							
 										echo '- '.number_format($couponsumm, 2, ',', '')." р.";
 										?>
 									</span>
 								</div>
 							</div><?php
 						}
 							} 

 							/*

 							else if ($couponorgift == 3) { ?>
 								<div style="display: block;  border: 1px solid #D6E8F5; padding: 20px;"> 									
 									<?php 
 										if ($baseSkidkaToPerformer >= 70.0) { 
 												$session->set('BaseSkidkaToPerformer', $baseSkidkaToPerformer);
 												if (($session->get('skidkaPerformerStatus', 0) === false) && ($is9491))
 												{
 													header('Location: /katalog/cart.html', 303);
 												}

 											?>
 											<span class="vmpayment_name">
 												<a class="highslide" onclick="return hs.htmlExpand( this, { objectType: 'iframe', outlineType: 'rounded-white', width: 700 } )" href="index.php?option=com_content&view=article&id=322&tmpl=component">
	 											Бумага офисная Xerox Performer A4 теперь по цене <strong>6 р. с НДС</strong>!
	 											</a>
 											</span>
 										<?php } else { 
 												$session->set('BaseSkidkaToPerformer', $baseSkidkaToPerformer);
 												if (($session->get('skidkaPerformerStatus', 0) === true) && ($is9491))
 												{
 													header('Location: /katalog/cart.html', 303);
 												}
 											?>
 											<span class="vmpayment_description">Чтобы получить Бумагу офисную Xerox Performer A4 по цене <strong>6 р. с НДС</strong> за пачку, <br/>Вам необходимо добавить в корзину канцтоваров на сумму: <strong>
 											<?php
 												echo 70.0 - $baseSkidkaToPerformer;
 											?> р. 
 											</strong> с НДС
 											</span>
 											<a class="highslide" onclick="return hs.htmlExpand( this, { objectType: 'iframe', outlineType: 'rounded-white', width: 700 } )" href="index.php?option=com_content&view=article&id=322&tmpl=component">
 											<span class="help"></span>
 											</a>
 										<?php }
 									?>
 									
 								</div>
 							<?php }
 							/*
 							else
 							{
 								echo '<div id="tab-1" class="tabs dyn-tabs" style="display: block;">';
 									include_once("templates/urbis/html/selectedpod.php");
 								echo '</div>';
 							}
 							echo '</div>';
 							*/

 						echo '</div>';
 						// ---------ЗАПИСЫВАЕМ СКИДКУ В ПАРАМЕТРЫ КОРЗИНЫ------------------------------------------------------
 						$this->cart->pricesUnformatted['billTotal'] = round($this->cart->pricesUnformatted['billTotal'] - $couponsumm, 2);
 						$this->cart->pricesUnformatted['salesPriceCoupon'] = $couponsumm*(-1);
 						// ---------------------------------------------------------------------------------------------------------------------
 						$this->cart->setCartIntoSession();
 						
 						if ($baseforskidka > 10)
 						{
 							if ( 1==2)
 							{
 								echo '<tr class="sectiontableentry1" valign="top">';
 									echo '<td colspan="';
 									if($this->cart->pricesUnformatted['salesPricePayment'] <> 0) echo '5" align="right">';
 									else echo '6" align="right">';
 										echo '<a class="highslide" href="index.php?option=com_content&view=article&id=287&tmpl=component" onclick="return hs.htmlExpand( this, { objectType: '."'".'iframe'."'".', outlineType: '."'".'rounded-white'."'".', width: 700 } )">';
 										echo $this->cart->cartData['paymentName'];
 										echo '</a>';
 									echo '</td>';
 									if($this->cart->pricesUnformatted['salesPricePayment'] <> 0)
 									{
 										echo '<td align="right">';
 											echo '<b>'.$this->currencyDisplay->createPriceDiv ('salesPricePayment', '', $this->cart->pricesUnformatted['salesPricePayment'], FALSE).'</b>';
 										echo '</td>';
 									}
 								echo '</tr>';
 							}
 							echo '<div class="moduletable" style="padding: 10px 15px; margin: 0 3px;">';
 								echo '<div class="floatrightsumm">';



 								if ($couponorgift == 3 && $is9491 && $baseSkidkaToPerformer >= 70) {
 									echo $this->currencyDisplay->createPriceDiv ('salesPrice', '', $this->cart->pricesUnformatted['salesPrice'], FALSE);
 								} else {
 									echo $this->currencyDisplay->createPriceDiv ('billTotal', '', $this->cart->pricesUnformatted['billTotal'], FALSE);
 								}
 								echo '</div>';

 								
 								echo '<div class="floatrightsumm" style="width: 220px;">';
 									echo '<h3>Всего сумма к оплате :</h3>';
 								echo '</div>';
 								echo '<div class="clear"></div>';
 							echo '</div>';
 						}
 					?>
</fieldset>
	<?php
		if ($sumskidka > 100 ) include_once ("templates/urbis/html/slide-pod.php");

		$debug = JRequest::getInt('debug', 0);
		if ($debug == 1) {
			echo '<pre>';
			print_r($this->cart);
			echo '</pre>';
		}
 	?>