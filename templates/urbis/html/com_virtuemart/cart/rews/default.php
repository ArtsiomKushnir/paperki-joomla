<?php
/**
*
* Layout for the shopping cart
*
* @package	VirtueMart
* @subpackage Cart
* @author Max Milbers
*
* @link http://www.virtuemart.net
* @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* @version $Id: cart.php 2551 2010-09-30 18:52:40Z milbo $
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

JHtml::_('behavior.formvalidation');
$document = JFactory::getDocument();

 vmdebug('cart prices',$this->cart->prices);
 vmdebug('cart pricesUnformatted',$this->cart->pricesUnformatted);
//  vmdebug('cart pricesUnformatted',$this->cart->cartData );
?>
<script>
jQuery('#system-message').hide();
</script>
<div class="vestheading">
<h1>Ваша корзина</h1>
</div>
<br>

<div class="cart-view">
	<div>
	
	<?php if (VmConfig::get('oncheckout_show_steps', 1) && $this->checkout_task==='confirm'){
		vmdebug('checkout_task',$this->checkout_task);
		echo '<div class="checkoutStep" id="checkoutStep4">'.JText::_('COM_VIRTUEMART_USER_FORM_CART_STEP4').'</div>';
	} ?>
	
<div class="clear"></div>
</div>



<?php 

 if(!empty($this->cart->products))
{

	echo $this->loadTemplate('pricelist');

	if ($this->checkout_task) $taskRoute = '&task='.$this->checkout_task;
	else $taskRoute ='';
	?>

	<form method="post" id="checkoutForm" name="checkoutForm" action="<?php echo JRoute::_( 'index.php?option=com_virtuemart&view=cart'.$taskRoute,$this->useXHTML,$this->useSSL ); ?>">

		<?php if($this->checkout_task =="confirm"){
// Leave A Comment Field 
$pn = $this->cart->cartData['paymentName'];
if(preg_match("/Банковский перевод/i", $pn) && 1==2){
echo '<br>
<b>Пожалуйста, введите ваши паспортные данные в поле "Оставить комментарий"</b>
<br>
Эта информация необходима нам для идентификации вашего платежа и мы гарантируем её полную конфиденциальность! ';
$this->cart->customer_comment = "Паспорт (серия_номер):\nКем выдан (наименование органа):\nДата выдачи (дд.мм.гг):\n";
};
?>
		<div class="customer-comment marginbottom15">
			<span class="comment"><?php echo JText::_('COM_VIRTUEMART_COMMENT'); ?></span><br />
			<textarea class="customer-comment" name="customer_comment" cols="50" rows="4"><?php echo $this->cart->customer_comment; ?></textarea>
		</div>
		<?php }
// Leave A Comment Field END 
?>

 <input type="hidden" name="tosAccepted" value="1"><input style="display: none;" class="terms-of-service" id="tosAccepted" type="checkbox" name="tosAccepted" value="1"  CHECKED/>			    </label>
		 

		<?php // Continue and Checkout Button ?>
		<div class="checkout-button-top">

		<?php // Continue Shopping Button
		if ($this->continue_link_html != '') {
			echo $this->continue_link_html;
		} ?>
	
			<?php // Terms Of Service Checkbox
		
		

			echo $this->checkout_link_html;
			$text = JText::_('COM_VIRTUEMART_ORDER_CONFIRM_MNU');

}else {echo "Ваша корзина пуста...";

echo '<br/><br/><a class="continue_link" href="/">Продолжить покупки в магазине</a>';
}
			?>
		</div>
		<?php //vmdebug('my cart',$this->cart);// Continue and Checkout Button END ?>

		<input type='hidden' name='task' value='<?php echo $this->checkout_task; ?>'/>
		<input type='hidden' name='option' value='com_virtuemart'/>
		<input type='hidden' name='view' value='cart'/>
	</form>

</div>
