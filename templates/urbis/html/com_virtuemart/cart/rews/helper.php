<?php

defined('_JEXEC') or die('Restricted access');
require_once JPATH_SITE.DS.'components'.DS.'com_virtuemart'.DS.'helpers'.DS.'cart.php';

class CartHelper {
	function __construct() {
		if (!class_exists( 'VmConfig' )) require(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_virtuemart'.DS.'helpers'.DS.'config.php');
		$this->cart = VirtueMartCart::getCart(false);
	}
	
	function assignValues() {
		//$new=(JFactory::getUser()->get('id')==0);
		$new=false;
		
		$this->cart->prepareAddressDataInCart("BT",$new);
		$this->BTaddress=$this->cart->BTaddress;
		$this->cart->prepareAddressDataInCart("ST",$new);
		$this->STaddress=$this->cart->STaddress;
		$this->lSelectShipment();
		$this->lSelectPayment();
	}
	
	public function lSelectShipment() {
		$found_shipment_method=false;
		$shipment_not_found_text = JText::_('COM_VIRTUEMART_CART_NO_SHIPPING_METHOD_PUBLIC');
		$this->shipment_not_found_text=$shipment_not_found_text;

		$shipments_shipment_rates=array();
		if (!$this->checkShipmentMethodsConfigured()) {
			$this->shipments_shipment_rates=$shipments_shipment_rates;
			$this->found_shipment_method=$found_shipment_method;
			return;
		}
		$selectedShipment = (empty($this->cart->virtuemart_shipmentmethod_id) ? 0 : $this->cart->virtuemart_shipmentmethod_id);

		$shipments_shipment_rates = array();
		if (!class_exists('vmPSPlugin')) require(JPATH_VM_PLUGINS . DS . 'vmpsplugin.php');
		JPluginHelper::importPlugin('vmshipment');
		$dispatcher = JDispatcher::getInstance();
		// Call this to get new VirtueMart Prices - i dont kno why is in the cart old prices before this :O
		$this->cart->getCartPrices();
		$returnValues = $dispatcher->trigger('plgVmDisplayListFEShipment', array( $this->cart, $selectedShipment, &$shipments_shipment_rates));
		// if no shipment rate defined
		$found_shipment_method = false;
		foreach ($returnValues as $returnValue) {
			if($returnValue){
				$found_shipment_method = true;
				break;
			}
		}
		$shipment_not_found_text = JText::_('COM_VIRTUEMART_CART_NO_SHIPPING_METHOD_PUBLIC');
		
		$shipments=array();
		foreach($shipments_shipment_rates as $items) {
			if(is_array($items)) {
				foreach($items as $item) {
					$shipments[]=$item;
				}
			} else {
				$shipments[]=$items;
			}
		}
		
		$this->shipment_not_found_text=$shipment_not_found_text;
		$this->shipments_shipment_rates=$shipments;
		$this->found_shipment_method=$found_shipment_method;
		return;
	}
	
	public function lSelectPayment() {
		$payment_not_found_text='';
		$payments_payment_rates=array();
		if (!$this->checkPaymentMethodsConfigured()) {
			$this->paymentplugins_payments=$payments_payment_rates;
			$this->found_payment_method=$found_payment_method;
		}

		$selectedPayment = empty($this->cart->virtuemart_paymentmethod_id) ? 0 : $this->cart->virtuemart_paymentmethod_id;

		$paymentplugins_payments = array();
		if(!class_exists('vmPSPlugin')) require(JPATH_VM_PLUGINS.DS.'vmpsplugin.php');
		JPluginHelper::importPlugin('vmpayment');
		$dispatcher = JDispatcher::getInstance();
		// Call this to get new VirtueMart Prices - i dont kno why is in the cart old prices before this :O
		$this->cart->getCartPrices();
		$returnValues = $dispatcher->trigger('plgVmDisplayListFEPayment', array($this->cart, $selectedPayment, &$paymentplugins_payments));
		// if no payment defined
		$found_payment_method = false;
		foreach ($returnValues as $returnValue) {
			if($returnValue){
				$found_payment_method = true;
				break;
			}
		}

		if (!$found_payment_method) {
			$link=''; // todo
			$payment_not_found_text = JText::sprintf('COM_VIRTUEMART_CART_NO_PAYMENT_METHOD_PUBLIC', '<a href="'.$link.'">'.$link.'</a>');
		}
		
		$payments=array();
		foreach($paymentplugins_payments as $items) {
			if(is_array($items)) {
				foreach($items as $item) {
					$payments[]=$item;
				}
			} else {
				$payments[]=$items;
			}
		}
		$this->payment_not_found_text=$payment_not_found_text;
		$this->paymentplugins_payments=$payments;
		$this->found_payment_method=$found_payment_method;
	}
	
	private function checkPaymentMethodsConfigured() {

		//For the selection of the payment method we need the total amount to pay.
		$paymentModel = VmModel::getModel('Paymentmethod');
		$payments = $paymentModel->getPayments(true, false);
		if (empty($payments)) {

			$text = '';
			if (!class_exists('Permissions'))
			require(JPATH_VM_ADMINISTRATOR . DS . 'helpers' . DS . 'permissions.php');
			if (Permissions::getInstance()->check("admin,storeadmin")) {
				$uri = JFactory::getURI();
				$link = $uri->root() . 'administrator/index.php?option=com_virtuemart&view=paymentmethod';
				$text = JText::sprintf('COM_VIRTUEMART_NO_PAYMENT_METHODS_CONFIGURED_LINK', '<a href="' . $link . '">' . $link . '</a>');
			}

			vmInfo('COM_VIRTUEMART_NO_PAYMENT_METHODS_CONFIGURED', $text);

			$tmp = 0;
			$this->found_payment_method=$tmp;

			return false;
		}
		return true;
	}
	
	private function checkShipmentMethodsConfigured() {

		//For the selection of the shipment method we need the total amount to pay.
		$shipmentModel = VmModel::getModel('Shipmentmethod');
		$shipments = $shipmentModel->getShipments();
		if (empty($shipments)) {

			$text = '';
			if (!class_exists('Permissions'))
			require(JPATH_VM_ADMINISTRATOR . DS . 'helpers' . DS . 'permissions.php');
			if (Permissions::getInstance()->check("admin,storeadmin")) {
				$uri = JFactory::getURI();
				$link = $uri->root() . 'administrator/index.php?option=com_virtuemart&view=shipmentmethod';
				$text = JText::sprintf('COM_VIRTUEMART_NO_SHIPPING_METHODS_CONFIGURED_LINK', '<a href="' . $link . '">' . $link . '</a>');
			}

			vmInfo('COM_VIRTUEMART_NO_SHIPPING_METHODS_CONFIGURED', $text);

			$tmp = 0;
			$this->found_shipment_method=$tmp;

			return false;
		}
		return true;
	}
	
	function setPayment() {
		if(!class_exists('vmPSPlugin')) require(JPATH_VM_PLUGINS.DS.'vmpsplugin.php');
		JPluginHelper::importPlugin('vmpayment');
		$this->cart->setPaymentMethod(JRequest::getInt('virtuemart_paymentmethod_id'));
		$dispatcher=JDispatcher::getInstance();
		$ret=$dispatcher->trigger('plgVmOnSelectCheckPayment',array($this->cart));
		if(JRequest::getInt('check_err',0)==1) {
			if($ret[0]===false) {
				$msgs=JFactory::getApplication()->getMessageQueue();
				$messages=array();
				foreach($msgs as $msg) {
					$messages[]=str_replace("<br/>","\n",$msg["message"]);
				}
				
				return array('error'=>1,'message'=>$messages);
			} else {
				return array('error'=>0);
			}
		}
		
		$price=$this->cart->getCartPrices();
		require_once JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'helpers'.DS.'currencydisplay.php';
		$cdisp=CurrencyDisplay::getInstance();
		$price["salesPricePayment"]=!empty($price["salesPricePayment"])?$cdisp->priceDisplay($price["salesPricePayment"]):"";
		$price["billTotal"]=!empty($price["billTotal"])?$cdisp->priceDisplay($price["billTotal"]):"";
		$price["paymentTax"]=!empty($price["billTotal"])?$cdisp->priceDisplay($price["paymentTax"]):"";
		$price["billTaxAmount"]=!empty($price["billTaxAmount"])?$cdisp->priceDisplay($price["billTaxAmount"]):"";
		return $price;	
	}
	
	function setShipment() {
		$this->cart->setShipment(JRequest::getInt('id'));
		$price=$this->cart->getCartPrices();
		require_once JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'helpers'.DS.'currencydisplay.php';
		$cdisp=CurrencyDisplay::getInstance();
		$price["shipmentTax"]=!empty($price["shipmentTax"])?$cdisp->priceDisplay($price["shipmentTax"]):"";
		$price["billTaxAmount"]=!empty($price["billTaxAmount"])?$cdisp->priceDisplay($price["billTaxAmount"]):"";
		$price["salesPriceShipment"]=!empty($price["salesPriceShipment"])?$cdisp->priceDisplay($price["salesPriceShipment"]):"";
		$price["billTotal"]=!empty($price["billTotal"])?$cdisp->priceDisplay($price["billTotal"]):"";
		return $price;
	}
	
	function setAddress() {
		if(JRequest::getInt('STsameAsBT')=='1') {
			$this->cart->STsameAsBT=1;
			$this->cart->ST=0;
			$this->cart->setCartIntoSession();
		} else {
			$this->cart->STsameAsBT=0;
		}
		if($this->cart->STsameAsBT==1 && JRequest::getString('address_type')=='ST') {
		} else {
			$this->cart->saveAddressInCart(JRequest::get('post'),JRequest::getString('address_type'));
		}
	}
	
	function register() {
		$user=VmModel::getModel('user');
		$ret=$user->store(JRequest::get('post'));
		$activate=JComponentHelper::getParams('useractivation');
		
		if($ret["success"] && $activate==false) {
			JFactory::getApplication()->login(array('username'=>$ret["user"]->username,'password'=>$ret["user"]->password_clear));
		}
		
		return $ret;
	}
	
	function updateProduct() {
		$id=JRequest::getString('id');
		$this->cart->updateProductCart(JRequest::getString('id'));
		$price=$this->cart->getCartPrices();
		require_once JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'helpers'.DS.'currencydisplay.php';
		$cdisp=CurrencyDisplay::getInstance();
		
		$nprice["subtotal_tax_amount"]=!empty($price[$id]["subtotal_tax_amount"])?$cdisp->priceDisplay($price[$id]["subtotal_tax_amount"]):"";
		$nprice["subtotal_discount"]=!empty($price[$id]["subtotal_discount"])?$cdisp->priceDisplay($price[$id]["subtotal_discount"]):"";
		$nprice["subtotal_with_tax"]=!empty($price[$id]["subtotal_with_tax"])?$cdisp->priceDisplay($price[$id]["subtotal_with_tax"]):"";
		$nprice["taxAmount"]=!empty($price["taxAmount"])?$cdisp->priceDisplay($price["taxAmount"]):"";
		$nprice["discountAmount"]=!empty($price["discountAmount"])?$cdisp->priceDisplay($price["discountAmount"]):"";
		$nprice["salesPrice"]=!empty($price["salesPrice"])?$cdisp->priceDisplay($price["salesPrice"]):"";
		$nprice["billTaxAmount"]=!empty($price["billTaxAmount"])?$cdisp->priceDisplay($price["billTaxAmount"]):"";
		$nprice["billDiscountAmount"]=!empty($price["billDiscountAmount"])?$cdisp->priceDisplay($price["billDiscountAmount"]):"";	
		$nprice["billTotal"]=!empty($price["billTotal"])?$cdisp->priceDisplay($price["billTotal"]):"";
		
		$nprice["salesPricePayment"]=!empty($price["salesPricePayment"])?$cdisp->priceDisplay($price["salesPricePayment"]):"";
		$nprice["paymentTax"]=!empty($price["paymentTax"])?$cdisp->priceDisplay($price["paymentTax"]):"";
		$nprice["shipmentTax"]=!empty($price["shipmentTax"])?$cdisp->priceDisplay($price["shipmentTax"]):"";
		$nprice["salesPriceShipment"]=!empty($price["salesPriceShipment"])?$cdisp->priceDisplay($price["salesPriceShipment"]):"";
		
		
		return $nprice;
	}
	
	function removeProduct() {
		$this->cart->removeProductCart(JRequest::getString('id'));
		$price=$this->cart->getCartPrices();
		require_once JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'helpers'.DS.'currencydisplay.php';
		$cdisp=CurrencyDisplay::getInstance();
		$nprice["taxAmount"]=!empty($price["taxAmount"])?$cdisp->priceDisplay($price["taxAmount"]):"";
		$nprice["discountAmount"]=!empty($price["discountAmount"])?$cdisp->priceDisplay($price["discountAmount"]):"";
		$nprice["salesPrice"]=!empty($price["salesPrice"])?$cdisp->priceDisplay($price["salesPrice"]):"";
		$nprice["billTaxAmount"]=!empty($price["billTaxAmount"])?$cdisp->priceDisplay($price["billTaxAmount"]):"";
		$nprice["billDiscountAmount"]=!empty($price["billDiscountAmount"])?$cdisp->priceDisplay($price["billDiscountAmount"]):"";	
		$nprice["billTotal"]=!empty($price["billTotal"])?$cdisp->priceDisplay($price["billTotal"]):"";
		
		$nprice["salesPricePayment"]=!empty($price["salesPricePayment"])?$cdisp->priceDisplay($price["salesPricePayment"]):"";
		$nprice["paymentTax"]=!empty($price["paymentTax"])?$cdisp->priceDisplay($price["paymentTax"]):"";
		$nprice["shipmentTax"]=!empty($price["shipmentTax"])?$cdisp->priceDisplay($price["shipmentTax"]):"";
		$nprice["salesPriceShipment"]=!empty($price["salesPriceShipment"])?$cdisp->priceDisplay($price["salesPriceShipment"]):"";
		
		return $nprice;
	}
	
	function setCoupon() {
		$msg=$this->cart->setCouponCode(JRequest::getString('coupon'));
		$price=$this->cart->getCartPrices();
		require_once JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'helpers'.DS.'currencydisplay.php';
		$cdisp=CurrencyDisplay::getInstance();
		$nprice=array();
		
		$nprice["couponTax"]=!empty($price["couponTax"])?$cdisp->priceDisplay($price["couponTax"]):"";
		$nprice["salesPriceCoupon"]=!empty($price["salesPriceCoupon"])?$cdisp->priceDisplay($price["salesPriceCoupon"]):"";
		$nprice["billTaxAmount"]=!empty($price["billTaxAmount"])?$cdisp->priceDisplay($price["billTaxAmount"]):"";
		$nprice["billDiscountAmount"]=!empty($price["billDiscountAmount"])?$cdisp->priceDisplay($price["billDiscountAmount"]):"";	
		$nprice["billTotal"]=!empty($price["billTotal"])?$cdisp->priceDisplay($price["billTotal"]):"";
		if(strlen($msg)) {
			$lang=JFactory::getLanguage();
			$lang->load('com_virtuemart');
			return array('error'=>1,'message'=>JText::_($msg),'cart'=>$nprice);
		}
		return $nprice;
	}
	
	function updatePrices() {
		$price=$this->cart->getCartPrices();
		
		require_once JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'helpers'.DS.'currencydisplay.php';
		$cdisp=CurrencyDisplay::getInstance();
		$nprice=array();
		
		foreach($price as $id=>$field) {
			if(is_array($field)) {
				$nprice["products"][$id]["taxAmount"]=!empty($price[$id]["taxAmount"])?$cdisp->priceDisplay($price[$id]["taxAmount"]):"";
				$nprice["products"][$id]["salesPrice"]=!empty($price[$id]["salesPrice"])?$cdisp->priceDisplay($price[$id]["salesPrice"]):"";
			}
		}
		
		$nprice["taxAmount"]=!empty($price["taxAmount"])?$cdisp->priceDisplay($price["taxAmount"]):"";
		$nprice["discountAmount"]=!empty($price["discountAmount"])?$cdisp->priceDisplay($price["discountAmount"]):"";
		$nprice["salesPrice"]=!empty($price["salesPrice"])?$cdisp->priceDisplay($price["salesPrice"]):"";
		$nprice["billTaxAmount"]=!empty($price["billTaxAmount"])?$cdisp->priceDisplay($price["billTaxAmount"]):"";
		$nprice["billDiscountAmount"]=!empty($price["billDiscountAmount"])?$cdisp->priceDisplay($price["billDiscountAmount"]):"";	
		$nprice["billTotal"]=!empty($price["billTotal"])?$cdisp->priceDisplay($price["billTotal"]):"";
		
		$nprice["salesPricePayment"]=!empty($price["salesPricePayment"])?$cdisp->priceDisplay($price["salesPricePayment"]):"";
		$nprice["paymentTax"]=!empty($price["paymentTax"])?$cdisp->priceDisplay($price["paymentTax"]):"";
		$nprice["shipmentTax"]=!empty($price["shipmentTax"])?$cdisp->priceDisplay($price["shipmentTax"]):"";
		$nprice["salesPriceShipment"]=!empty($price["salesPriceShipment"])?$cdisp->priceDisplay($price["salesPriceShipment"]):"";
		
		return $nprice;
	}
}
?>
