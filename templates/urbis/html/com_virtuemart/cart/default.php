﻿<?php
$session =& JFactory::getSession();
$selectedgift = 0;
$selectedgift = JRequest::getInt('selectedgiftprod', 0);
if($selectedgift > 0)
	{
		$session->set('selectedgiftprod', $selectedgift);
		exit(print($selectedgift));
	}

if($this->cart->pricesUnformatted['salesPrice'] < 100)
{
	$session->set('selectedgiftprod', 0);
}
/**
 *
 * Layout for the shopping cart
 *
 * @package    VirtueMart
 * @subpackage Cart
 * @author Max Milbers
 *
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: cart.php 2551 2010-09-30 18:52:40Z milbo $
 */

// Check to ensure this file is included in Joomla!
defined ('_JEXEC') or die('Restricted access');

JHtml::_ ('behavior.formvalidation');
$document = JFactory::getDocument ();
$document->addScriptDeclaration ($box);
$document->addScriptDeclaration ("

//<![CDATA[
	jQuery(document).ready(function($) {
	if ( $('#STsameAsBTjs').is(':checked') ) {
				$('#output-shipto-display').hide();
			} else {
				$('#output-shipto-display').show();
			}
		$('#STsameAsBTjs').click(function(event) {
			if($(this).is(':checked')){
				$('#STsameAsBT').val('1') ;
				$('#output-shipto-display').hide();
			} else {
				$('#STsameAsBT').val('0') ;
				$('#output-shipto-display').show();
			}
		});
	});

//]]>

");
$document->addStyleDeclaration ('#facebox .content {display: block !important; height: 480px !important; overflow: auto; width: 560px !important; }');

?>

<div class="cart-view">
	<div>
		<div class="width50 floatleft">
			<h1><?php echo JText::_ ('COM_VIRTUEMART_CART_TITLE'); ?></h1>
		</div>


		<?php
if($this->cart->pricesUnformatted['salesPrice'] > 0 ){
 if (VmConfig::get ('oncheckout_show_steps', 1) && $this->checkout_task === 'confirm') {
		vmdebug ('checkout_task', $this->checkout_task);
		echo '<div class="checkoutStep" id="checkoutStep4">' . JText::_ ('COM_VIRTUEMART_USER_FORM_CART_STEP4') . '</div>';
	}

 ?>
		<div class="width50 floatleft right">
			<?php // Continue Shopping Button
			if (!empty($this->continue_link_html))
			{
				echo $this->continue_link_html;
			} ?>
		</div>
		<div class="clear"></div>
	</div>

	<?php
	echo $this->loadTemplate ('pricelist');
echo '<br/>';
if($this->cart->pricesUnformatted['salesPrice'] > 10 ){
//echo shopFunctionsF::getLoginForm ($this->cart, FALSE);

	// This displays the form to change the current shopper
	$adminID = JFactory::getSession()->get('vmAdminID');
	if ((JFactory::getUser()->authorise('core.admin', 'com_virtuemart') || JFactory::getUser($adminID)->authorise('core.admin', 'com_virtuemart')) && (VmConfig::get ('oncheckout_change_shopper', 0))) {

		echo $this->loadTemplate ('shopperform');
	}
	?>

	<?php
//print_r($this);


		if ($this->checkout_task) {
			$taskRoute = '&task=' . $this->checkout_task;
		}
		else {
			$taskRoute = '';
		}
	?>
		<form method="post" id="checkoutForm" name="checkoutForm" action="<?php echo JRoute::_ ('index.php?option=com_virtuemart&view=cart' . $taskRoute, $this->useXHTML, $this->useSSL); ?>">
	
<?php 	//if ($this->checkout_task === 'confirm') { ?>

		<?php // Leave A Comment Field ?>

			<div class="customer-comment marginbottom15">
				<span class="comment">Комментарий к заказу</span><br/>
				<textarea class="customer-comment" name="customer_comment" cols="60" rows="2"><?php echo $this->cart->customer_comment; ?></textarea>
			</div>

		<?php // Leave A Comment Field END ?>

<?php// 	} ?>

		<?php // Continue and Checkout Button ?>

		<div class="checkout-button-top">
			<?php echo $this->checkout_link_html; ?>
		</div>

		<?php // Continue and Checkout Button END ?>

		<input type="hidden" value="1" name="tosAccepted"></input>
		<input type='hidden' name='order_language' value='<?php echo $this->order_language; ?>'/>
		<input type='hidden' id='STsameAsBT' name='STsameAsBT' value='<?php echo $this->cart->STsameAsBT; ?>'/>
		<input type='hidden' name='task' value='<?php echo $this->checkout_task; ?>'/>
		<input type='hidden' name='option' value='com_virtuemart'/>
		<input type='hidden' name='view' value='cart'/>
	</form>
<?php 

} else echo '<h3 class="superheader">Оформление заказа на указанную сумму невозможно!</h3> <div class="floatright">Минимальная сумма заказа на сайте - <b>10 белорусских рублей</b>.<br/>При меньшей сумме просьба обращаться в <a href = "http://paperki.by/nashi-magaziny.html" title="Розничные магазины Канцелярские штучки в Бресте" >розничные магазины.</a></div>';

}

else {
$document = JFactory::getDocument();
$document->addScript(JURI::root(true). "/templates/urbis/js/jquery.cookie.js");
$document->addScriptDeclaration("
jQuery.removeCookie('skolny_tovar', { path: '/' });
");
echo '<div class="clear"></div>Ваша корзина пуста! <br/><br/></div> ';
if (!empty($this->continue_link_html))
	{
		echo $this->continue_link_html;
	}
}
?>
</div>
<br/>
<pre><?php //print_r($this->cart); ?></pre><br/>
<pre><?php //print_r($this->cart->getCartPrices()); ?></pre><br/>
<pre><?php //print_r($this->cart->couponCode); ?></pre><br/>