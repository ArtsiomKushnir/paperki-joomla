<?php


// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
?>


<table width="100%" border="0" cellspacing="0" class="html-email">

<tr>
<td width="180px">
<img src="<?php echo  JURI::root().'templates/urbis/images/logo-small.jpg';?>" style="margin: 10px 0 10px 5px;"/>
<a class="default" title="<?php echo $this->vendor->vendor_store_name ?>" href="<?php echo JURI::root().'index.php?option=com_virtuemart&view=orders&layout=details&order_number='.$this->orderDetails['details']['BT']->order_number.'&order_pass='.$this->orderDetails['details']['BT']->order_pass; ?>">
<?php echo JText::_('COM_VIRTUEMART_MAIL_SHOPPER_YOUR_ORDER_LINK'); ?></a>


</td>
    <td style="padding: 5px 10px;">
<b>
<?php 
//exit(print_r($this->userfields['fields']));
//$user_id = JFactory::getUser($this->orderDetails['details']['BT']->virtuemart_user_id)->name;
if(!empty($this->orderDetails['details']['BT']->first_name)){
$user_id = $this->orderDetails['details']['BT']->first_name;
}else{
$user_id = JFactory::getUser($this->orderDetails['details']['BT']->virtuemart_user_id)->name;
}
echo $user_id;
?>,<br/> благодарим за заказ в интернет-магазине <a href="<?php echo JURI::root();?>"><?php echo $this->vendor->vendor_store_name; ?></a>!
</b> 
</br>
</br>
<h2>Ваш заказ номер <strong><?php echo $this->orderDetails['details']['BT']->order_number ?></strong> от <?php echo date('d.m.Y', strtotime($this->orderDetails['details']['BT']->created_on)); echo ' <b>'.JText::_($this->orderDetails['details']['BT']->order_status_name);?></b>! </h2>
<small>Данное письмо подтвеждает бронирование для Вас товара на складе и содержит счет к оплате.</small>
 </td>
  </tr>
</table>


<table width="100%" border="0" cellspacing="0" class="html-email">
        <tbody>
            <tr>
                <th colspan="2" align="center">
<h2>Счет-фактура <b>№ <?php echo $this->orderDetails['details']['BT']->order_number ?></b>  
от <?php echo date('d.m.Y', strtotime($this->orderDetails['details']['BT']->created_on)); ?> 
г.</h2>
                </th>
</tr>

<tr>
<td style="text-align: right; width: 180px;" valign="top">Продавец:</td>
<td style="padding: 5px 10px;">
<?php 
echo '<p><small><b>Общество с ограниченной ответственностью «Паперки»</b><br/>
224023, г. Брест, ул.Советской Конституции, 1<br/>
УНП: 200578550<br/>
Р/с № 3012683590014 в филиале ОАО "БПС-Банк" по Брестской области, <br/>г.Брест, ул.Мицкевича, 10, 
МФО 150501309 <br/> 
</small></p>';
?>
</td>
</tr>


<tr>
<td style="text-align: right;" valign="top">Покупатель:</td>
<td style="padding: 5px 10px;">
<p><small>
<b><?php echo $user_id;?></b><br/>
++!++<?php echo $this->orderDetails['details']['BT']->middle_name?$this->orderDetails['details']['BT']->middle_name.'<br/>':'';
echo $this->orderDetails['details']['BT']->last_name?'Контактное лицо: '.$this->orderDetails['details']['BT']->last_name.'<br/>':'';

if(JFactory::getUser($this->orderDetails['details']['BT']->virtuemart_user_id)->username) {?>
УНП: <?php echo JFactory::getUser($this->orderDetails['details']['BT']->virtuemart_user_id)->username;?><br/>
Р/с № <?php echo $this->orderDetails['details']['BT']->address_1;?> в  <?php echo $this->orderDetails['details']['BT']->zip;?>, 
МФО (код банка): <?php echo $this->orderDetails['details']['BT']->company;?> <br/> 
<?php } 

foreach ($this->userfields['fields'] as $field) {	
if ($field['name'] == 'phone_1') { ?>
Тел.: <span class="values vm2<?php echo '-' . $field['name'] ?>" ><?php echo $this->escape($field['value']) ?></span>
<br class="clear" />
<?php }	
}
foreach ($this->userfields['fields'] as $field) {
if ($field['name'] == 'email') { ?>
e-mail: <span class="values vm2<?php echo '-' . $field['name'] ?>" ><?php echo $this->escape($field['value']) ?></span>
<br class="clear" />
<?php }	 
	    }
	    ?>
</small></p>
		</td>
            </tr>
        </tbody>
    </table>
