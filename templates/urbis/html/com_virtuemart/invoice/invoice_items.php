<?php
/**
*
* Order items view
*
* @package	VirtueMart
* @subpackage Orders
* @author Max Milbers, Valerie Isaksen
* @link http://www.virtuemart.net
* @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* @version $Id: details_items.php 5432 2012-02-14 02:20:35Z Milbo $
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
$catModel = VmModel::getModel('Category');
$prodModel = VmModel::getModel('Product');
 if ( VmConfig::get('show_tax')) {
    $colspan=7;
 } else {
    $colspan=8;
 }

$db =& JFactory::getDBO();
$mainframe = Jfactory::getApplication();
$curid = $mainframe->getUserStateFromRequest( "virtuemart_currency_id", 'virtuemart_currency_id',JRequest::getInt('virtuemart_currency_id', $vendor_currency['vendor_currency']) );
//print_r($this);
if(!$curid) $curid = $this->orderDetails['details']['BT']->user_currency_id;
$curid = 194;
$query = 'SELECT currency_name FROM #__virtuemart_currencies WHERE virtuemart_currency_id = '.$curid;
$db->setQuery($query);
$curname = $db->loadResult();
?>
<table class="html-email" width="100%"  cellpadding="0">
	<tr align="left" class="sectiontableheader">
		<td align="left" width="4%" ><strong>Фото</strong></td>
		<td align="left" width="4%" ><strong>Код</strong></td>
		<td align="left" colspan="1" width="54%" ><strong><?php echo JText::_('COM_VIRTUEMART_PRODUCT_NAME_TITLE') ?></strong></td>
		<td align="center" width="14%" ><strong><?php echo JText::_('COM_VIRTUEMART_ORDER_PRINT_PRICE') ?></strong></td>
		<td align="center" width="6%"><strong><?php echo JText::_('COM_VIRTUEMART_ORDER_PRINT_QTY') ?></strong></td>
		<td align="right" width="18%"><strong>Всего с НДС</strong></td>
	</tr>

<?php

	foreach($this->orderDetails['items'] as $item) {
		$qtt = $item->product_quantity ;
		$product_link = JURI::root().'index.php?option=com_virtuemart&view=productdetails&virtuemart_category_id=' . $item->virtuemart_category_id .
			'&virtuemart_product_id=' . $item->virtuemart_product_id;

		?>
		<tr valign="top">
<td width="32px" style="text-align: center;">
<?php $product_sku = str_replace("m","", $item->order_item_sku);
////image finder
$icode = '00000000';
$len = strlen($product_sku);
if ($len == '1') {
$icode = '0000000'.$product_sku;
}else if ($len == '2') {$icode = '000000'.$product_sku;}
else if ($len == '3') {$icode = '00000'.$product_sku;}
else if ($len == '4') {$icode = '0000'.$product_sku;}
else if ($len == '5') {$icode = '000'.$product_sku;}
else if ($len == '6') {$icode = '00'.$product_sku;}
else if ($len == '7') {$icode = '0'.$product_sku;}
else if ($len == '8') {$icode =  $product_sku;}
$full_image = '';
$pas = JURI::root().'images/catalog/'.$icode.'.jpg';
$pas2 = JURI::root().'images/catalog/'.$icode.'.png';
$pas3 = JURI::root().'images/catalog/'.$icode.'.gif';
$pas4 = JURI::root().'images/catalog/'.$icode.'.jpeg';

if (file_exists($pas)) {
$full_image = $pas;
}
else if (file_exists($pas2)) {$full_image = $pas2;}
else if (file_exists($pas3)) {$full_image = $pas3;}
else if (file_exists($pas4)) {$full_image = $pas4;}
else{$full_image =  $pas;}


?>

<img src="<?php echo $full_image; ?>" width="30" height="30" alt="<?php echo $item->order_item_name; ?>" />
</td>
<td style="text-align: center;">
<?php echo $item->order_item_sku;?>
</td>
<td align="left" colspan="1" >
<?php

$pros = $prodModel->getProduct($item->virtuemart_product_id);
$newcat = $catModel->getCategory(max($pros->categories));
echo JHTML::link ( JRoute::_ ( JURI::root().'index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $item->virtuemart_product_id . '&virtuemart_category_id=' . $newcat->virtuemart_category_id ), $newcat->category_name, array ('title' =>$item->order_item_name) ); 
echo '<br/><small>'.$item->order_item_name.'</small>'; 

?></a>
				<?php
// 				vmdebug('$item',$item);
					if (!empty($item->product_attribute)) {
							if(!class_exists('VirtueMartModelCustomfields'))require(JPATH_VM_ADMINISTRATOR.DS.'models'.DS.'customfields.php');
							$product_attribute = VirtueMartModelCustomfields::CustomsFieldOrderDisplay($item,'FE');
						echo $product_attribute;
					}
				?>
			</td>

 <td style="text-align: center;">
<?php 
echo str_replace('$', ' ', ($this->currency->priceDisplay($item->product_final_price, $curid))); ?>
		    </td>
			<td align="center" >
				<?php echo $qtt; ?>
			</td>

			<td align="right"  class="priceCol">
				<?php
echo str_replace('$', ' ', ($this->currency->priceDisplay(  $item->product_subtotal_with_tax ,$curid))); //No quantity or you must use product_final_price
 ?>
			</td>
		</tr>
<?php
	}
?>
 <tr class="sectiontableentry1">
			<td colspan="5" align="right"><?php echo JText::_('COM_VIRTUEMART_ORDER_PRINT_PRODUCT_PRICES_TOTAL'); ?></td>
			<td align="right"><?php echo str_replace('$', ' ', ($this->currency->priceDisplay($this->orderDetails['details']['BT']->order_salesPrice, $curid))) ?></td>
		  </tr>
<?php
 	
if ($this->orderDetails['details']['BT']->coupon_code !== 'noller') 
{
    //$coupon_code=$this->orderDetails['details']['BT']->coupon_code?' ('.$this->orderDetails['details']['BT']->coupon_code.')':'';
    $baseforskidka = $this->orderDetails['details']['BT']->order_total;
    //$orderNumber = $this->orderDetails['details']['BT']->order_number;
    	if($baseforskidka >= 100)
    	{
    		$procskid = 1;
    	}
    	if($baseforskidka > 200)
    	{
    		$procskid = 2;
    	}
    	if($baseforskidka > 400)
    	{
    		$procskid = 3;
    	}
    	if($baseforskidka > 650)
    	{
    		$procskid = 4;
    	}
    	if($baseforskidka > 950)
    	{
    		$procskid = 5;
    	}
    	if($baseforskidka > 1300)
    	{
    		$procskid = 6;
    	}
    	if($baseforskidka > 1650)
    	{
    		$procskid = 7;    		
    	}
    	if($baseforskidka >= 2000)
    	{
    		$procskid = 8; // размер скидки %
    	}
    	$couponsumm = round($baseforskidka*$procskid/100, 2); // размер скидки в рублевом выражении
	?>
	<tr>
		<td colspan="5" align="right">Скидка от суммы заказа: </td>
		<td align="right">
			<?php
				echo '- '.$this->currency->priceDisplay($couponsumm, $this->currency);
			?>
		</td>
	</tr>
	<tr>
		<td align="right" class="pricePad" colspan="5"><strong><?php echo JText::_('COM_VIRTUEMART_ORDER_PRINT_TOTAL') ?></strong></td>
		<td align="right">
			<strong>
			<?php 
				echo $this->currency->priceDisplay($this->orderDetails['details']['BT']->order_total - $couponsumm, $curid);
			?>
			</strong>
		</td>
	</tr>

<?php  } else { ?>

	<tr>
		<td align="right" class="pricePad" colspan="5"><strong><?php echo JText::_('COM_VIRTUEMART_ORDER_PRINT_TOTAL') ?></strong></td>
		<td align="right">
			<strong>
			<?php 
				echo $this->currency->priceDisplay($this->orderDetails['details']['BT']->order_total, $curid);
			?>
			</strong>
		</td>
	</tr>
	<?php } ?>
	<tr>
		<td align="right" class="pricePad" colspan="2">Способ получения заказа: </td>
		<td align="left" class="pricePad" colspan="4"><small> <?php 

		$query = "SELECT * FROM #__virtuemart_shipmentmethods_ru_ru WHERE virtuemart_shipmentmethod_id =".$this->orderDetails['details']['BT']->virtuemart_shipmentmethod_id;
		$db->setQuery($query);
		$shipmentdesc = $db->loadObject();
		echo '<b>'.$shipmentdesc->shipment_name.'</b><br/>'.$shipmentdesc->shipment_desc; ?></small></td>
	</tr>
</table>