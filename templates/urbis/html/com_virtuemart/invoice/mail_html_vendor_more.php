<?php
defined('_JEXEC') or die('');

/**
*
* Template for the order email
*
* @package	VirtueMart
* @subpackage Order
* @author Max Milbers
*
* @link http://www.virtuemart.net
* @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
*/
?>

<?php 
$session =& JFactory::getSession();
$selectedgift = $session->get('selectedgiftprod', 0);

if($selectedgift > 0){
$productModel = VmModel::getModel('Product');
$product = $productModel->getProduct ($selectedgift, true, true, false, false);
$categoryModel = VmModel::getModel('category');
foreach ($product->categories as $cats)
{$category = $categoryModel->getCategory($cats);
if($category->haschildren || $category->virtuemart_category_id == '1349' ){
}else $newcat = $category;
}

$product_sku = str_replace("m","", $product->product_sku);
////image finder
$icode = '00000000';
$len = strlen($product_sku);
if ($len == '1') {
$icode = '0000000'.$product_sku;
}else if ($len == '2') {$icode = '000000'.$product_sku;}
else if ($len == '3') {$icode = '00000'.$product_sku;}
else if ($len == '4') {$icode = '0000'.$product_sku;}
else if ($len == '5') {$icode = '000'.$product_sku;}
else if ($len == '6') {$icode = '00'.$product_sku;}
else if ($len == '7') {$icode = '0'.$product_sku;}
else if ($len == '8') {$icode =  $product_sku;}
$full_image = '';
$pas = 'images/catalog/'.$icode.'.jpg';
$pas2 = 'images/catalog/'.$icode.'.png';
$pas3 = 'images/catalog/'.$icode.'.gif';
$pas4 = 'images/catalog/'.$icode.'.jpeg';

if (file_exists($pas)) {
$full_image = $pas;
}
else if (file_exists($pas2)) {$full_image = $pas2;}
else if (file_exists($pas3)) {$full_image = $pas3;}
else if (file_exists($pas4)) {$full_image = $pas4;}
else{$full_image = 'images/catalog/noimage.jpg';}

$full_image2 = '';
$pas = 'images/catalog/'.$icode.'-1.jpg';
$pas2 = 'images/catalog/'.$icode.'-1.png';
$pas3 = 'images/catalog/'.$icode.'-1.gif';
$pas4 = 'images/catalog/'.$icode.'-1.jpeg';

if (file_exists($pas)) {
$full_image2 = $pas;
}
else if (file_exists($pas2)) {$full_image2 = $pas2;}
else if (file_exists($pas3)) {$full_image2 = $pas3;}
else if (file_exists($pas4)) {$full_image2 = $pas4;}
//////end image finder
?>

<table class="html-email" width="100%" border="0" cellspacing="0">
<tbody>
<tr>
<td colspan="2">
<p><b>Покупатель отказался от скидки и выбрал подарок! </b></p>
</br>
<img src="<?php echo JURI::root().$full_image; ?>" width="90" style="display: block; float: left; margin: -5px 20px 0 5px;" id="selectedimage" />

<span id="selectedgifttext">
<?php 
echo '<b>'.$product->product_name.'</b>'; 
echo '<br/><b>ID: '.$selectedgift.'</b>'; 
echo '<br/><br/>Код товара: <strong>'.$product_sku.'</strong>'; 

?>
<br/><br/>
</span>
</td>
</tr>
</table>


<?php 
} ?>

<table class="html-email" width="100%" border="0" cellspacing="0">
<tbody>
<tr>
<td style="text-align: left;" valign="top">
 <?php   if (!empty($this->orderDetails['details']['BT']->customer_note)) {
	echo '<br/><b>Комментарий покупателя:</b><br/>'.$this->orderDetails['details']['BT']->customer_note;
    } else echo 'Информация о покупателе:';
?>
</td>
<td style="padding: 5px 10px;">
<p><small>
<b><?php 
if(!empty($this->orderDetails['details']['BT']->first_name)){
$user_id = $this->orderDetails['details']['BT']->first_name;
}else{
$user_id = JFactory::getUser($this->orderDetails['details']['BT']->virtuemart_user_id)->name;
}
echo $user_id;?></b><br/>
++!++ 
<?php echo $this->orderDetails['details']['BT']->middle_name?$this->orderDetails['details']['BT']->middle_name.'<br/>':'';
echo $this->orderDetails['details']['BT']->last_name?'Контактное лицо: '.$this->orderDetails['details']['BT']->last_name.'<br/>':'';

if(JFactory::getUser($this->orderDetails['details']['BT']->virtuemart_user_id)->username) {?>
УНП: <?php echo JFactory::getUser($this->orderDetails['details']['BT']->virtuemart_user_id)->username;?><br/>
Р/с № <?php echo $this->orderDetails['details']['BT']->address_1;?> в  <?php echo $this->orderDetails['details']['BT']->zip;?>, 
МФО (код банка): <?php echo $this->orderDetails['details']['BT']->company;?> <br/> 
<?php } 

foreach ($this->userfields['fields'] as $field) {	
if ($field['name'] == 'phone_1') { ?>
Тел.: <span class="values vm2<?php echo '-' . $field['name'] ?>" ><?php echo $this->escape($field['value']) ?></span>
<br class="clear" />
<?php }	
}
foreach ($this->userfields['fields'] as $field) {
if ($field['name'] == 'email') { ?>
e-mail: <span class="values vm2<?php echo '-' . $field['name'] ?>" ><?php echo $this->escape($field['value']) ?></span>
<br class="clear" />
<?php }	 
	    }
	    ?>
</small></p>
		</td>
            </tr>
</tbody>
</table>
<?php 

//echo JText::_('COM_VIRTUEMART_VENDOR_MORE'); ?>