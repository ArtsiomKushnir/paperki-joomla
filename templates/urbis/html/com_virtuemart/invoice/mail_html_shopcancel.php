<?php


// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
?>


<table width="100%" border="0" cellpadding="0" cellspacing="0" class="html-email">

<tr>
    <td colspan="3">
<b>Уважаемый(ая) 
<?php foreach ($this->userfields['fields'] as $field) {
if ($field['name'] == 'last_name') { ?>
<span class="values vm2<?php echo '-' . $field['name'] ?>" ><?php echo $this->escape($field['value']) ?></span>
<?php }

if ($field['name'] == 'first_name') { 
?>
<span class="values vm2<?php echo '-' . $field['name'] ?>" ><?php echo $this->escape($field['value']) ?></span>
<?php }

if ($field['name'] == 'middle_name') { ?>
<span class="values vm2<?php echo '-' . $field['name'] ?>" ><?php echo $this->escape($field['value']) ?></span>
<?php }
}
?>, благодарим за заказ в интернет-магазине <?php echo $this->vendor->vendor_store_name; ?>!
</b> 
</br>
</br>
</p>
<h3>Ваш заказ отменен!</h3></td>

 </td>
  </tr>

<?php $nb=count($this->orderDetails['history']);
  if($this->orderDetails['history'][$nb-1]->customer_notified && !(empty($this->orderDetails['history'][$nb-1]->comments))) { ?>
  <tr>
    <td colspan="3">
		<b>Комментарий отдела продаж:</b><br/><?php echo  $this->orderDetails['history'][$nb-1]->comments; ?>
	</td>
  </tr>
  <?php } ?>


<table class="html-email" cellspacing="0" cellpadding="0" border="0" width="100%">  <tr  >
	<th width="100%">
	    Надеемся, что Вы и дальше будете оставаться нашим клиентом!
	</th>
	
    </tr>
</table>