<?php


// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
?>


<table width="100%" border="0" cellspacing="0" class="html-email">

<tr>
<td width="180px">
<img src="<?php echo  JURI::root().'templates/urbis/images/logo-small.jpg';?>" style="margin: 10px 0 10px 5px;"/>
<a class="default" title="<?php echo $this->vendor->vendor_store_name ?>" href="<?php echo JURI::root().'index.php?option=com_virtuemart&view=orders&layout=details&order_number='.$this->orderDetails['details']['BT']->order_number.'&order_pass='.$this->orderDetails['details']['BT']->order_pass; ?>">
<?php echo JText::_('COM_VIRTUEMART_MAIL_SHOPPER_YOUR_ORDER_LINK'); ?></a>


</td>
    <td style="padding: 5px 10px;">
<b>
<?php 
//exit(print_r($this->userfields['fields']));
$user_id = JFactory::getUser($this->orderDetails['details']['BT']->virtuemart_user_id)->name;
echo $user_id;
?>,<br/> благодарим за заказ в интернет-магазине <a href="<?php echo JURI::root();?>"><?php echo $this->vendor->vendor_store_name; ?></a>!
</b> 
</br>
</br>
<h2>Ваш заказ номер <strong><?php echo $this->orderDetails['details']['BT']->order_number ?></strong> от <?php echo date('d.m.Y', strtotime($this->orderDetails['details']['BT']->created_on)); echo ' <b>'.JText::_($this->orderDetails['details']['BT']->order_status_name);?></b>! </h2>
<small>Надеемся что вы и дальше будете оставаться нашим клиентом.</small>
 </td>
  </tr>
</table>