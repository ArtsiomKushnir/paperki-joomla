<?php
/**
 *
 * Layout for the shopper mail, when he confirmed an ordner
 *
 * The addresses are reachable with $this->BTaddress, take a look for an exampel at shopper_adresses.php
 *
 * With $this->cartData->paymentName or shipmentName, you get the name of the used paymentmethod/shippmentmethod
 *
 * In the array order you have details and items ($this->orderDetails['details']), the items gather the products, but that is done directly from the cart data
 *
 * $this->orderDetails['details'] contains the raw address data (use the formatted ones, like BTaddress). Interesting informatin here is,
 * order_number ($this->orderDetails['details']['BT']->order_number), order_pass, coupon_code, order_status, order_status_name,
 * user_currency_rate, created_on, customer_note, ip_address
 *
 * @package	VirtueMart
 * @subpackage Cart
 * @author Max Milbers, Valerie Isaksen
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
?>

<html>
    <head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<style type="text/css">
            body, td, p, th {  font: 14px Arial; color: #111111;}
	    table.html-email {margin:10px auto;background:#fff;}
a {
    color: #015DCB;
    outline: medium none;
    text-decoration: underline;
}
 h2 {
    color: #015DCB;
    font: 14px Arial;
    margin-bottom: 10px;
    text-align: left;
    text-decoration: none;
}
	    span.grey {color:#666;}
	    span.date {color:#666; }
small{color: #555555; font: bold 14px;}
	    a.default:link, a.default:hover, a.default:visited {color:#333333;line-height:22px;background: #f2f2f2;margin: 10px 0px 10px 5px;padding: 2px 4px 1px 4px;border: solid #CAC9C9 1px;border-radius: 4px;-webkit-border-radius: 4px;-moz-border-radius: 4px;text-shadow: 1px 1px 1px #f2f2f2;font-size: 10px;background-position: 0px 0px;display: inline-block;text-decoration: none;}
	    a.default:hover {color:#888;background: #f8f8f8;}
	    .cart-summary{ }
	    .html-email th,
.html-email
{ 
margin: 0px;
padding: 0;
 background: none repeat scroll 0 0 #E1F2FF;
    border: 1px solid #FFFFFF;
    border-radius: 4px;
    box-shadow: 0 1px 2px #AAAAAA;
    color: #777777;
}


.html-email{ 
 border-collapse: separate;
}

.html-email th h2{
text-align: center;
padding: 7px 0 0 0;
font: 16px Arial;
}
	    .sectiontableentry2,  .cart-summary th{ background: #E1F2FF;margin: 0px;padding: 10px;}
	    .sectiontableentry1, .html-email td, .cart-summary td {
   border: 1px solid #E1F2FF;
    padding: 1px 5px;
    vertical-align: middle;
background: #fff;margin: 0px;}
	    .line-through{text-decoration:line-through}
table tr td p{padding: 0; margin: 0;}
.vmshipment_description{display: none;}	
.sectiontableheader td{ 
background: none repeat scroll 0 0 #E1F2FF;
color: #222;
}
</style>

    </head>

    <body style="word-wrap: break-word;">
	<div style="background-color: #F7FAF7; border-radius: 6px; width: 800px">
	    <table style="margin: auto;" cellpadding="0" cellspacing="0" width="760" ><tr><td>
			<?php
$stat = $this->orderDetails['details']['BT']->order_status;

if($stat == 'C' || $stat == 'R' || $stat == 'S') {
echo $this->loadTemplate('shopshorter');
echo $this->loadTemplate('pricelist');
echo $this->loadTemplate('faktura');
} 
else if($stat == 'X') {
echo $this->loadTemplate('shopchancel');
}
else {	
echo $this->loadTemplate('shopshort');
echo $this->loadTemplate($this->recipient . '_more');
echo $this->loadTemplate('pricelist');
}
echo $this->loadTemplate('footer');

			?>
		    </td></tr>
	    </table>
	</div>
    </body>
</html>