<?php
defined('_JEXEC') or die('Restricted access');
$categoryModel = VmModel::getModel('category');
$iCol = 1;
$iCategory = 1;
?>

<div class="category-view">

<?php  foreach ($this->categories as $categoryp) {
if($categoryp->virtuemart_category_id != '1349' && $categoryp->virtuemart_category_id != '5685' ) {
$category = $categoryModel->getCategory($categoryp->virtuemart_category_id);
if($category->children){
 ?>
<div class="width48 floatleft" style="margin-right: 15px;">
<h2>  <?php echo $categoryp->category_name ?></h2>
<?php 
echo '<div class="catlist category"><ul>';
foreach ($category->children as $categor ) { 
$caturl = JRoute::_ ( 'index.php?option=com_virtuemart&view=category&virtuemart_category_id=' .$categor->virtuemart_category_id );
?>
<li>
<div><a href="<?php echo $caturl;?>"><?php echo $categor->category_name; ?></a></div>
</li>
<?php
}
echo '</ul></div></div>';

if ($iCol == 2) {
echo '<div class="clear"></div>';
$iCol = 1;
} else $iCol++;  
}
}
}
?>

<div class="clear"></div>
</div>