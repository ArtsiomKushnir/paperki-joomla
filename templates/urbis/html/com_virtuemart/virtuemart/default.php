<?php

defined('_JEXEC') or die('Restricted access');
$document = &JFactory::getDocument();
$app = JFactory::getApplication(); 
$menu = $app->getMenu()->getActive();
$document->title = $menu->params->get('page_title');
?>
<div class="vestheading">
<h1><?php echo $menu->params->get('page_title');?></h1>
</div>
<?php # Vendor Store Description
if (!empty($this->vendor->vendor_store_desc)) { ?>
	<?php 
echo $this->vendor->vendor_store_desc; 
?>
<?php } ?>
<?php
if ($this->categories and VmConfig::get('show_categories', 1)) echo $this->loadTemplate('categories');
?>