<?php

defined ('_JEXEC') or die('Restricted access');
?>
<div class="product-price" id="productPrice<?php echo $this->product->virtuemart_product_id ?>">
	<?php

	//vmdebug('view productdetails layout default show prices, prices',$this->product);
	if ($this->product->prices['salesPrice']<=0 and VmConfig::get ('askprice', 1) and isset($this->product->images[0]) and !$this->product->images[0]->file_is_downloadable) {
		?>
		<a class="ask-a-question bold" href="<?php echo $this->askquestion_url ?>"><?php echo JText::_ ('COM_VIRTUEMART_PRODUCT_ASKPRICE') ?></a>
		<?php
	} else {

echo '<div class="PricesalesPrice" style="display : block;">Цена: ';
	if (round($this->product->prices['basePrice'],$this->currency->_priceConfig['salesPrice'][1]) != $this->product->prices['salesPrice']) {
		echo '<span style="color: #D8161F; font-size: 13px; text-decoration:line-through;" >' . $this->currency->createPriceDiv ('basePrice', 'COM_VIRTUEMART_PRODUCT_SALESPRICE', $this->product->prices, TRUE, FALSE) . "</span> ";
	}
	echo '<span class="PricesalesPrice" style="font-size: 18px;">'.$this->currency->createPriceDiv ('salesPrice', 'COM_VIRTUEMART_PRODUCT_SALESPRICE', $this->product->prices, TRUE, FALSE) . '</span>';
	}


echo '</div>';


	?>
</div>
