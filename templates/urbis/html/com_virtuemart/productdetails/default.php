<?php
/**
 *
 * Show the product details page
 *
 * @package	VirtueMart
 * @subpackage
 * @author Max Milbers, Eugen Stranz
 * @author RolandD,
 * @todo handle child products
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: default.php 5444 2012-02-15 15:31:35Z Milbo $
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');


$url = JRoute::_('index.php?option=com_virtuemart&view=productdetails&task=askquestion&virtuemart_product_id=' . $this->product->virtuemart_product_id . '&virtuemart_category_id=' . $this->product->virtuemart_category_id . '&tmpl=component');
$document = &JFactory::getDocument();
$document->addScriptDeclaration("
	jQuery(document).ready(function($) {
	
		$('.additional-images a').mouseover(function() {
			var himg = this.href ;
			var extension=himg.substring(himg.lastIndexOf('.')+1);
			if (extension =='png' || extension =='jpg' || extension =='gif') {
				$('.main-image img').attr('src',himg );
$('.main-image a').attr('href',himg );
			}
			console.log(extension)
		});
	});
");

/* Let's see if we found the product */
if (empty($this->product)) {
    echo JText::_('COM_VIRTUEMART_PRODUCT_NOT_FOUND');
    echo '<br /><br />  ' . $this->continue_link_html;
    return;
}
	    $link = 'index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $this->product->virtuemart_product_id.'&virtuemart_category_id='.$this->product->virtuemart_category_id;

$categoryModel = VmModel::getModel('category');
$newcatcategoryId = JRequest::getVar('virtuemart_category_id');
$parentcat = $categoryModel->getCategory($newcatcategoryId);
?>
		
<div class="productdetails-view">

<?php $full_name_prod = $parentcat->category_name." ".$this->product->product_name; 
		$full_name_prod = html_entity_decode($full_name_prod); // преобразовываем HTML сущности в символы ?>

<h1><?php echo $full_name_prod; ?></h1>
<?php $document->setTitle($full_name_prod); ?>

    
    <?php

    // Product Edit Link
    echo $this->edit_link;
    // Product Edit Link END
    ?>

  
<div style="margin-left: 20px;">
	<div class="width44 floatleft">
<?php
echo $this->loadTemplate('images');
?>
	</div>

	<div class="width56 floatright">
	    <div class="spacer-buy-area">
        <div class="product-short-description">
	    <?php 
    if (!empty($this->product->customfieldsSorted['normal'])) {
	$this->position='normal';
	echo $this->loadTemplate('customfields');
    }

		// Manufacturer of the Product
		if (VmConfig::get('show_manufacturers', 1) && !empty($this->product->virtuemart_manufacturer_id) && $this->product->mf_name) {
		    echo '<p>'.$this->loadTemplate('manufacturer').'</p>';
	$manufacturerProductsURL = JRoute::_('index.php?option=com_virtuemart&view=category&virtuemart_manufacturer_id=' . $this->product->virtuemart_manufacturer_id);

	if($this->product->virtuemart_manufacturer_id > 0) { ?>
<p><a href="<?php echo $manufacturerProductsURL; ?>">Смотреть все товары производителя</a></p> 
		
	<?php } 

		}

/** @todo Test if content plugins modify the product description */
	    echo $this->product->product_s_desc; ?>
        </div>

<div class="greybg">
<?php

		if ($this->showRating) {
		    $maxrating = VmConfig::get('vm_maximum_rating_scale',5);

				if (empty($this->rating)) {  } else {
					$ratingwidth = ( $this->rating->rating * 100 ) / $maxrating;//I don't use round as percetntage with works perfect, as for me
					?>
					<span class="vote">
						<?php echo JText::_('COM_VIRTUEMART_RATING').' '.round($this->rating->rating, 2) . '/'. $maxrating; ?><br/>
						<span title=" <?php echo (JText::_("COM_VIRTUEMART_RATING_TITLE") . $this->rating->rating . '/' . $maxrating) ?>" class="vmicon ratingbox" style="display:inline-block;">
							<span class="stars-orange" style="width:<?php echo $ratingwidth;?>%">
							</span>
						</span>
					</span>
					<?php
				}
		}
		// Product Price
		if ($this->show_prices ) {
//ADDED BY URBIS TO SHOW A PRICE LIST
$db = JFactory::getDBO();
$sql = "SELECT product_price, price_quantity_start, price_quantity_end FROM #__virtuemart_product_prices WHERE 
virtuemart_product_id = '".$this->product->virtuemart_product_id."' ORDER BY product_price DESC";
$db->setQuery($sql);
$prices = $db->loadObjectList();
if(count($prices) > 1){
echo '<div class="mprices"><div class="title"><strong>Цена зависит от количества!</strong></div><table cellpadding="0" cellspacing="0" width="100%">';
foreach ($prices as $dprice){
echo '<tr><td class="help"><span>';
if($dprice->price_quantity_start == 0 && $dprice->price_quantity_end > 0) echo 'от 1 ед.: </span></td><td>'.number_format($dprice->product_price, 2, ' р. ', '').' к.</td></tr>';
if($dprice->price_quantity_start > 0 && $dprice->price_quantity_end > 0) echo 'от '.$dprice->price_quantity_start.' ед.: </span></td><td>'.number_format($dprice->product_price, 2, ' р. ', '').' к.</td></tr>';
if($dprice->price_quantity_start > 0 && $dprice->price_quantity_end == 0) echo 'от '.$dprice->price_quantity_start.' ед.: </span></td><td>'.number_format($dprice->product_price, 2, ' р. ', '').' к.</td></tr>';

}
echo '</table></div>';
}
//ADDED BY URBIS TO SHOW A PRICE LIST
		    echo $this->loadTemplate('showprices');
		}
		?>


		<?php
		// Add To Cart Button
// 			if (!empty($this->product->prices) and !empty($this->product->images[0]) and $this->product->images[0]->file_is_downloadable==0 ) {
		if (!VmConfig::get('use_as_catalog', 0) and !empty($this->product->prices)) {
		    echo $this->loadTemplate('addtocart');
		}  // Add To Cart Button END
		?>
</div>
		


		

	    </div>
	</div>
</br></br>
	<div class="clear"></div>

<?php 
	if (!empty($this->product->product_desc)) { ?>
        <div class="product-description">
	<?php /** @todo Test if content plugins modify the product description */ ?>
 <!--   	<span class="title"><?php echo JText::_('COM_VIRTUEMART_PRODUCT_DESC_TITLE') ?></span> -->
	<?php echo $this->product->product_desc; ?>
        </div>
    <?php
    }

?>
</br>
    </div>


<?php
    $this->loadTemplate('manufacturer');
 // Product custom_fields END
    // Product Packaging
    $product_packaging = '';
    if ($this->product->packaging || $this->product->box) { ?>
	  <div class="product-packaging">

	    <?php
	    if ($this->product->packaging) {
		$product_packaging .= JText::_('COM_VIRTUEMART_PRODUCT_PACKAGING1') . $this->product->packaging;
		if ($this->product->box)
		    $product_packaging .= '<br />';
	    }
	    if ($this->product->box)
		$product_packaging .= JText::_('COM_VIRTUEMART_PRODUCT_PACKAGING2') . $this->product->box;
	    echo str_replace("{unit}", $this->product->product_unit ? $this->product->product_unit : JText::_('COM_VIRTUEMART_PRODUCT_FORM_UNIT_DEFAULT'), $product_packaging);
	    ?>
        </div>
   <?php } // Product Packaging END
    ?>

    <?php

    if (!empty($this->product->customfieldsRelatedProducts)) {
	echo $this->loadTemplate('relatedproducts');
    } // Product customfieldsRelatedProducts END


    ?>
<div id="tabs-3"  style="margin-left: 20px;">
<?php
//echo $this->loadTemplate('reviews');
?>
</br></br
</div>
</div>
</div>