<?php
/**
 *
 * Show the product details page
 *
 * @package	VirtueMart
 * @subpackage
 * @author Max Milbers, Valerie Isaksen

 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: default_images.php 5406 2012-02-09 12:22:33Z alatak $
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
error_reporting(0);


$product_sku = str_replace("m","", $this->product->product_sku);
////image finder
$icode = '00000000';
$len = strlen($product_sku);
if ($len == '1') {
$icode = '0000000'.$product_sku;
}else if ($len == '2') {$icode = '000000'.$product_sku;}
else if ($len == '3') {$icode = '00000'.$product_sku;}
else if ($len == '4') {$icode = '0000'.$product_sku;}
else if ($len == '5') {$icode = '000'.$product_sku;}
else if ($len == '6') {$icode = '00'.$product_sku;}
else if ($len == '7') {$icode = '0'.$product_sku;}
else if ($len == '8') {$icode =  $product_sku;}
$full_image = '';
$pas = 'images/catalog/'.$icode.'.jpg';
$pas2 = 'images/catalog/'.$icode.'.png';
$pas3 = 'images/catalog/'.$icode.'.gif';
$pas4 = 'images/catalog/'.$icode.'.jpeg';

if (file_exists($pas)) {
$full_image = $pas;
}
else if (file_exists($pas2)) {$full_image = $pas2;}
else if (file_exists($pas3)) {$full_image = $pas3;}
else if (file_exists($pas4)) {$full_image = $pas4;}
else{$full_image = 'images/catalog/noimage.jpg';}

$full_image2 = '';
if (file_exists('images/catalog/'.$icode.'-1.jpg')) { $full_image2 = 'images/catalog/'.$icode.'-1.jpg';}
else if (file_exists('images/catalog/'.$icode.'-1.png')) {$full_image2 = 'images/catalog/'.$icode.'-1.png';}
else if (file_exists('images/catalog/'.$icode.'-1.gif')) {$full_image2 = 'images/catalog/'.$icode.'-1.gif';}
else if (file_exists('images/catalog/'.$icode.'-1.jpeg')) {$full_image2 = 'images/catalog/'.$icode.'-1.jpeg';}

$full_image3 = '';
if (file_exists('images/catalog/'.$icode.'-2.jpg')) { $full_image3 = 'images/catalog/'.$icode.'-2.jpg';}
else if (file_exists('images/catalog/'.$icode.'-2.png')) {$full_image3 = 'images/catalog/'.$icode.'-2.png';}
else if (file_exists('images/catalog/'.$icode.'-2.gif')) {$full_image3 = 'images/catalog/'.$icode.'-2.gif';}
else if (file_exists('images/catalog/'.$icode.'-2.jpeg')) {$full_image3 = 'images/catalog/'.$icode.'-2.jpeg';}

$full_image4 = '';
if (file_exists('images/catalog/'.$icode.'-3.jpg')) { $full_image4 = 'images/catalog/'.$icode.'-3.jpg';}
else if (file_exists('images/catalog/'.$icode.'-3.png')) {$full_image4 = 'images/catalog/'.$icode.'-3.png';}
else if (file_exists('images/catalog/'.$icode.'-3.gif')) {$full_image4 = 'images/catalog/'.$icode.'-3.gif';}
else if (file_exists('images/catalog/'.$icode.'-3.jpeg')) {$full_image4 = 'images/catalog/'.$icode.'-3.jpeg';}

//////end image finder

$file = $full_image;
$old = ImageCreateFromJpeg($file); 
$w = ImageSX ($old); 
$h = ImageSY ($old);
$height = (260/$w)*$h + 10;

// Product Main Image
    ?>
<div class="main-image">
<div class="mainimagecontainer" style="height: <?php echo $height;?>px !important;  overflow: hidden">

<?php 
$prodModel = VmModel::getModel('Product');
$this->product->stock = $prodModel::getStockIndicator($this->product); ?>
<div class="prodinstock mainimgstock">
<a class="highslide" href="index.php?option=com_content&view=article&id=289&tmpl=component" onclick="return hs.htmlExpand( this, { objectType: 'iframe', outlineType: 'rounded-white', width: 700 } )" >
<span class="vmicon vm2-<?php echo $this->product->stock->stock_level ?>" title="<?php echo $this->product->stock->stock_tip ?>"></span>
</a>
</div>

<div class="plitkasku2">
<?php echo 'Код: <b>'.substr($icode, -5, 5).'</b>';?>
</div>


<a href="<?php echo $this->baseurl ?>/images/catalog/<?php echo $icode; ?>.jpg" onclick="return hs.expand(this,{captionText: this.getAttribute('caption'), slideshowGroup: 'group<?php echo $this->product->id;?>'})" class="highslide"  title="<?php echo $category->category_name.' '.$this->product->product_name ?>"  caption="<?php echo $category->category_name.' '.$this->product->product_name ?>" target="_blank" >
<img src="templates/urbis/images/grey.gif" data-original="<?php echo $this->baseurl.'/'.$full_image; ?>"   alt="<?php echo $category->category_name.' '.$this->product->product_name ?>" class="lazy" />
</a></div>
</div>

    <div class="additional-images highslide-gallery">

<?php if($full_image2 || $full_image3 || $full_image4){ ?>
<div class="floatleft">
<a href="<?php echo $this->baseurl ?>/images/catalog/<?php echo $icode; ?>.jpg" onclick="return hs.expand(this,{captionText: this.getAttribute('caption'), slideshowGroup: 'group<?php echo $this->product->id;?>'})" class="highslide"  title="<?php echo $category->category_name.' '.$this->product->product_name ?>"  caption="<?php echo $category->category_name.' '.$this->product->product_name ?>" target="_blank" >
<img src="<?php echo $this->baseurl.'/'.$full_image; ?>" alt="<?php echo $category->category_name.' '.$this->product->product_name ?>" />
</a>
</div>
<?php } ?>


<?php if($full_image2){ ?>
<div class="floatleft">
<a href="<?php echo $this->baseurl.'/'.$full_image2; ?>" onclick="return hs.expand(this,{captionText: this.getAttribute('caption'), slideshowGroup: 'group<?php echo $this->product->id;?>'})" class="highslide"  title="<?php echo $category->category_name.' '.$this->product->product_name ?>"  caption="<?php echo $category->category_name.' '.$this->product->product_name ?>" target="_blank" >
<img src="<?php echo $this->baseurl.'/'.$full_image2; ?>" alt="<?php echo $category->category_name.' '.$this->product->product_name ?>" />
</a>
</div>
<?php } ?>

<?php if($full_image3){ ?>
<div class="floatleft">
<a href="<?php echo $this->baseurl.'/'.$full_image3; ?>" onclick="return hs.expand(this,{captionText: this.getAttribute('caption'), slideshowGroup: 'group<?php echo $this->product->id;?>'})" class="highslide"  title="<?php echo $category->category_name.' '.$this->product->product_name ?>"  caption="<?php echo $category->category_name.' '.$this->product->product_name ?>" target="_blank" >
<img src="<?php echo $this->baseurl.'/'.$full_image3; ?>" alt="<?php echo $category->category_name.' '.$this->product->product_name ?>" />
</a>
</div>
<?php } ?>

<?php if($full_image4){ ?>
<div class="floatleft">
<a href="<?php echo $this->baseurl.'/'.$full_image4; ?>" onclick="return hs.expand(this,{captionText: this.getAttribute('caption'), slideshowGroup: 'group<?php echo $this->product->id;?>'})" class="highslide"  title="<?php echo $category->category_name.' '.$this->product->product_name ?>"  caption="<?php echo $category->category_name.' '.$this->product->product_name ?>" target="_blank" >
<img src="<?php echo $this->baseurl.'/'.$full_image4; ?>" alt="<?php echo $category->category_name.' '.$this->product->product_name ?>" />
</a>
</div>
<?php } ?>


        <div class="clear"></div>
    </div>
