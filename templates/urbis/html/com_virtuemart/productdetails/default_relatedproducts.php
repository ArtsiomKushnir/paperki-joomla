<?php
/**
 *
 * Show the product details page
 *
 * @package	VirtueMart
 * @subpackage
 * @author Max Milbers, Valerie Isaksen

 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: default_relatedproducts.php 6431 2012-09-12 12:31:31Z alatak $
 */

// Check to ensure this file is included in Joomla!
defined ( '_JEXEC' ) or die ( 'Restricted access' );
?>
        <div class="product-related-products">
<div class="heading">
<h3>
<?php 
$catModel = VmModel::getModel('Category');
$parentcat = $catModel->getParentCategory($this->product->virtuemart_category_id);
if($parentcat->virtuemart_category_id == 17) echo 'Данный комплект мебели состоит из:';
else echo 'Смотрите также';
?>
</h3>
</div>



    <?php
    foreach ($this->product->customfieldsRelatedProducts as $field) {
$productModel = VmModel::getModel('Product');
$product = $productModel->getProduct($field->custom_value, true, true, true, false);
$productModel->addImages($product);
if(!empty($field->display)) {?>
<div class="relatedprods">
<div class="product-main center floatleft <?php echo $cellwidth;?>">
<h3>
<?php // Product Name
echo JHTML::link ( JRoute::_ ( 'index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $product->virtuemart_product_id . '&virtuemart_category_id=' . $product->virtuemart_category_id ), $product->product_name, array ('title' => $product->product_name ) ); ?>
</h3>
<div class="product-mainimage">
<div class="imagecontainer">
<?php // Product Image
if ($product->images) {
?>
<div class="prodimage">
<a href="<?php echo JRoute::_ ( 'index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $product->virtuemart_product_id . '&virtuemart_category_id=' . $product->virtuemart_category_id ); ?>">
<img src="<?php echo $product->images[0]->file_url_thumb; ?>" />
</a>
</div>
<?php
}
?>
</div>
<div class="center productinfo">
<?php echo '<a href="'.JRoute::_ ( 'index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $product->virtuemart_product_id . '&virtuemart_category_id=' . $product->virtuemart_category_id).'">'; 
echo $this->currency->createPriceDiv ('salesPrice', 'COM_VIRTUEMART_PRODUCT_SALESPRICE', $product->prices, TRUE, FALSE);
 ?>
</a>
</div>
</div>
</div>
</div>

<?php }	    
} ?>
<div class="clear"></div>

