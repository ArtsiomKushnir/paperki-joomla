<?php
/**
 *
 * Modify user form view, User info
 *
 * @package	VirtueMart
 * @subpackage User
 * @author Oscar van Eijk
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: edit_shopper.php 6037 2012-05-17 17:45:32Z Milbo $
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

?>
<form method="post" id="adminForm" name="userForm" action="<?php echo JRoute::_('index.php?view=user',$this->useXHTML,$this->useSSL) ?>" class="form-validate">

<?php if( $this->userDetails->virtuemart_user_id!=0)  {
//    echo $this->loadTemplate('vmshopper');
    } ?>
<?php 
if( $this->userDetails->virtuemart_user_id ==0)  {
echo '<h3 class="superheader floatleft">Пожалуйста, заполните регистрационную форму.</h3><div class="clr"></div>
Поля, отмеченные знаком *, являются обязательными для заполнения.<div class="clr"><br/></div>';
}
echo $this->loadTemplate('address_userfields'); 
if( $this->userDetails->virtuemart_user_id==0)  {?>
<tr><td class="key"><label class="primer" for="primer">Проверка на спам *</label></td>
<td>
<?php 
$numo = rand(2, 10);
$numt = rand(1, 9);
echo $numo.' + '.$numt.' = ';
?>
<input type="text" class="required" name="primer" id="primer" style="width: 70px;">
<input type="hidden" name="primero" value="<?php echo $numo*22735;?>">
<input type="hidden" name="primert" value="<?php echo $numt*124735;?>"><?php 
}
?>
</td></tr></tbody></table><br/>

<?php if(!$this->userDetails->user_is_vendor){ ?>
	<input class="button" type="submit" onclick="javascript:return myValidator(userForm, 'saveUser');" value="<?php echo $this->button_lbl ?>" />
	&nbsp;
	<input class="button" type="reset" onclick="window.location.href='<?php echo JRoute::_('index.php?option=com_virtuemart&view=user', FALSE); ?>'" value="<?php echo JText::_('COM_VIRTUEMART_CANCEL'); ?>" />
<div class="clr"><br/></div>



<?php } ?>
<?php if(!empty($this->virtuemart_userinfo_id)){
	echo '<input type="hidden" name="virtuemart_userinfo_id" value="'.(int)$this->virtuemart_userinfo_id.'" />';
}
?>
<input type="hidden" name="task" value="<?php echo $this->fTask; // I remember, we removed that, but why?   ?>" />
<input type="hidden" name="address_type" value="BT" />

<input type="hidden" name="option" value="com_virtuemart" />
<input type="hidden" name="controller" value="user" />
<?php echo JHTML::_( 'form.token' ); ?>
</form>