<?php

/**
 *
 * Modify user form view, User info
 *
 * @package	VirtueMart
 * @subpackage User
 * @author Oscar van Eijk, Eugen Stranz
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: edit_address_userfields.php 6349 2012-08-14 16:56:24Z Milbo $
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

// Status Of Delimiter
$closeDelimiter = false;
$openTable = true;
$hiddenFields = '';

// Output: Userfields
foreach($this->userFields['fields'] as $field) {

	if ($field['hidden'] == true ) {

		// We collect all hidden fields
		// and output them at the end
		$hiddenFields .= $field['formcode'] . "\n";

	} else {

if($field['name'] == 'username' || $field['name'] == 'password' || $field['name'] == 'password2' || $field['name'] == 'name' || $field['name'] == 'agreed'){
$field['required'] = true;
}


if($this->userDetails->virtuemart_user_id !=0 && $field['name'] == 'password'){
$field['required'] = false;
}


if($field['name'] == 'password2') $sty = ' style="display: none;"'; 
elseif($field['name'] == 'first_name') $sty = ' style="display: none;"'; 
else $sty = '';


		// If we have a new delimiter
		// we have to start a new table
		if($openTable) {
			$openTable = false;
			?>

			<table  class="adminForm user-details">

		<?php
		}

		// Output: Userfields

if($this->userDetails->virtuemart_user_id !=0 && $field['name'] == 'agreed'){
$field['required'] = false;
} else {
		?>
				<tr <?php echo $sty;?>>
					<td class="key" title="<?php echo $field['description'] ?>" >
						<label class="<?php echo $field['name'] ?>" for="<?php echo $field['name'] ?>_field">
							<?php echo $field['title'] . ($field['required'] ? ' *' : '') ?>
						</label>
					</td>
					<td>
						<?php echo $field['formcode'] ?>
					</td>
				</tr>
	<?php
	}
}

}


echo $hiddenFields
?>

<script type="text/javascript">
jQuery(document).ready(function() {
jQuery("#userForm #password2_field").val(jQuery("#userForm #password_field").val())
jQuery('#userForm #password_field').bind('keyup', function(event){repeat();});
jQuery("#adminForm #password2_field").val(jQuery("#admin #password_field").val())
jQuery('#adminForm #password_field').bind('keyup', function(event){repeatme();});

jQuery("#email_field").addClass('email');
jQuery('#name_field').bind('keyup', function(event){repeatname();});

});

function repeatname(){
jQuery("#first_name_field").val(jQuery("#name_field").val());
}


function repeat()
{
jQuery("#userForm #password2_field").val(jQuery("#userForm #password_field").val());
}

function repeatme()
{
jQuery("#adminForm #password2_field").val(jQuery("#adminForm #password_field").val())
}
</script> 