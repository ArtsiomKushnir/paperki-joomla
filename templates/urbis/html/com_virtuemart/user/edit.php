<?php
/**
*
* Modify user form view
*
* @package	VirtueMart
* @subpackage User
* @author Oscar van Eijk
* @link http://www.virtuemart.net
* @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* @version $Id: edit.php 6472 2012-09-19 08:46:21Z alatak $
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

//AdminMenuHelper::startAdminArea($this);
// vmdebug('User edit',$this);
// Implement Joomla's form validation
JHTML::_('behavior.formvalidation');
JHTML::stylesheet('vmpanels.css', JURI::root().'components/com_virtuemart/assets/css/'); // VM_THEMEURL
?>
<style type="text/css">
.invalid {
	border-color: #f00;
	background-color: #ffd;
	color: #000;
}
label.invalid {
	background-color: #fff;
	color: #f00;
}
</style>
<script language="javascript">
 function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
    }

function myValidator(f, t)
{
jQuery('#username_field').addClass("required");
<?php if($this->userDetails->virtuemart_user_id == 0) {?>
jQuery('#password_field').addClass("required");
jQuery('#agreed_field').addClass("required");
jQuery('#password2_field').addClass("required");
<?php } 
else { ?> jQuery('#agreed_field').removeClass("required"); <?php } ?>
jQuery('#name_field').addClass("required");
	f.task.value=t;
	if (document.formvalidator.isValid(f)) {
<?php if($this->userDetails->virtuemart_user_id==0) { ?>
if(jQuery('#' + f.id + ' #agreed_field').attr('checked')) {
<?php } ?>

if(isValidEmailAddress(jQuery('#' + f.id + ' #email_field').val())){
			f.submit();
			return true;
}
else {
jQuery('#' + f.id + ' #email_field').addClass("invalid");
alert('Неверный формат e-mail!');
 }
<?php if($this->userDetails->virtuemart_user_id==0) { ?>

		} 
else{
jQuery('#' + f.id + ' .agreed #formexample').css('color', '#ff0000');	
alert('Вы должны принять условия обслуживания! ');
}
<?php } ?>
	} else {
		var msg = '<?php echo addslashes( JText::_('COM_VIRTUEMART_USER_FORM_MISSING_REQUIRED_JS') ); ?>';
		alert (msg);
	}
	return false;
}
</script>
<h1><?php echo $this->page_title ?></h1>
<?php echo shopFunctionsF::getLoginForm(false); ?>
<br/>
<?php if($this->userDetails->virtuemart_user_id==0) {
echo '<h2>2. Впервые регистрируетесь на нашем сайте?</h2>';
}?>



<?php // Loading Templates in Tabs
if($this->userDetails->virtuemart_user_id!=0) {
    $tabarray = array();
    if($this->userDetails->user_is_vendor){
	    if(!empty($this->add_product_link)) {
		    echo $this->add_product_link;
	    }
	    $tabarray['vendor'] = 'COM_VIRTUEMART_VENDOR';
    }
    $tabarray['shopper'] = 'Ваши учетные данные';
    if (!empty($this->shipto)) {
	    $tabarray['shipto'] = 'COM_VIRTUEMART_USER_FORM_ADD_SHIPTO_LBL';
    }
    if (($_ordcnt = count($this->orderlist)) > 0) {
	    $tabarray['orderlist'] = 'COM_VIRTUEMART_YOUR_ORDERS';
    }
    $tabarray['indsetting'] = 'Ваши настройки';


    shopFunctionsF::buildTabs ( $this, $tabarray);

 } else {
    echo $this->loadTemplate ( 'shopper' );
 }

?>


