<?php
/**
 *
 * Enter address data for the cart, when anonymous users checkout
 *
 * @package    VirtueMart
 * @subpackage User
 * @author Oscar van Eijk, Max Milbers
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: edit_address.php 6406 2012-09-08 09:46:55Z Milbo $
 */
// Check to ensure this file is included in Joomla!
defined ('_JEXEC') or die('Restricted access');
// vmdebug('user edit address',$this->userFields['fields']);
// Implement Joomla's form validation
JHTML::_ ('behavior.formvalidation');
JHTML::stylesheet ('vmpanels.css', JURI::root () . 'components/com_virtuemart/assets/css/');

if ($this->fTask === 'savecartuser') {
	$rtask = 'registercartuser';
	$url = 0;
}
else {
	$rtask = 'registercheckoutuser';
	$url = JRoute::_ ('index.php?option=com_virtuemart&view=cart&task=checkout', $this->useXHTML, $this->useSSL);
}
?>
<h1>Заполните регистрационную форму</h1>
<?php
//echo shopFunctionsF::getLoginForm (TRUE, FALSE, $url);
?>
<script language="javascript">
jQuery(document).ready(function() {
  jQuery("#aboutcompany_field").attr('placeholder', 'Опишите планируемый объем закупки, особые пожелания к нашему сотрудничеству и т.д.');

<?php if($this->userDetails->virtuemart_user_id!=0) { ?>
//jQuery("tr.optroznica").hide();
<?php } ?>
if(jQuery('#optroznicaopt').attr('checked')){
  jQuery("tr.phone_1").fadeIn(300);
  jQuery("tr.company").fadeIn(450);
  jQuery("tr.rekvizit").fadeIn(600);
  jQuery("tr.aboutcompany").fadeIn(750);
}


jQuery(".optroznica input").click(function ()
{
if(jQuery('#optroznicaopt').attr('checked')){
  jQuery("tr.phone_1").fadeIn(300);
  jQuery("tr.company").fadeIn(450);
  jQuery("tr.rekvizit").fadeIn(600);
  jQuery("tr.aboutcompany").fadeIn(750);
} else {

  jQuery("tr.company").hide();
  jQuery("tr.rekvizit").hide();
  jQuery("tr.aboutcompany").hide();
}
});
});



	function myValidator(f, t) {
jQuery("#username_field").val(jQuery("#email_field").val());
jQuery("#name_field").val(jQuery("#first_name_field").val());
jQuery("#password2_field").val(jQuery("#password_field").val());
		f.task.value = t; //this is a method to set the task of the form on the fTask.
		if (document.formvalidator.isValid(f)) {
<?php if($this->userDetails->virtuemart_user_id==0) { ?>
if(jQuery('#agreed_field').attr('checked')) {
<?php } ?>
			f.submit();
			return true;
<?php if($this->userDetails->virtuemart_user_id==0) { ?>

		} 
else{
jQuery('.agreed #formexample').css('color', '#ff0000');	
			alert('Вы должны принять условия обслуживания! ');
}
<?php } ?>

		} else {
			var msg = '<?php echo addslashes (JText::_ ('COM_VIRTUEMART_USER_FORM_MISSING_REQUIRED_JS')); ?>';
			alert(msg + ' ');
		}
		return false;
	}

	function callValidatorForRegister(f) {

		var elem = jQuery('#username_field');
		elem.attr('class', "required");

		var elem = jQuery('#name_field');
		elem.attr('class', "required");

		var elem = jQuery('#password_field');
		elem.attr('class', "required");

		var elem = jQuery('#password2_field');
		elem.attr('class', "required");

		var elem = jQuery('#userForm');

		return myValidator(f, '<?php echo $rtask ?>');

	}
</script>


	<form method="post" id="userForm" name="userForm" class="form-validate">
		<!--<form method="post" id="userForm" name="userForm" action="<?php echo JRoute::_ ('index.php'); ?>" class="form-validate">-->
		

		<?php
		if (!class_exists ('VirtueMartCart')) {
			require(JPATH_VM_SITE . DS . 'helpers' . DS . 'cart.php');
		}

		if (count ($this->userFields['functions']) > 0) {
			echo '<script language="javascript">' . "\n";
			echo join ("\n", $this->userFields['functions']);
			echo '</script>' . "\n";
		}
		echo $this->loadTemplate ('userfields');

		?>


<div>
			<?php
			if (strpos ($this->fTask, 'cart') || strpos ($this->fTask, 'checkout')) {
				$rview = 'cart';
			}
			else {
				$rview = 'user';
			}
// echo 'rview = '.$rview;

			if (strpos ($this->fTask, 'checkout') || $this->address_type == 'ST') {
				$buttonclass = 'vm-button-correct';
			}
			else {
				$buttonclass = 'vm-button-correct';
			}
?>

				<a style="width: 290px;" class="<?php echo $buttonclass ?>" type="submit" onclick="javascript:return callValidatorForRegister(userForm);"
				        title="<?php echo JText::_ ('COM_VIRTUEMART_REGISTER_AND_CHECKOUT'); ?>"><?php echo JText::_ ('COM_VIRTUEMART_REGISTER_AND_CHECKOUT'); ?></a>
				<?php if (!VmConfig::get ('oncheckout_only_registered', 0)) { ?>
					<a class="<?php echo $buttonclass ?>" title="<?php echo JText::_ ('COM_VIRTUEMART_CHECKOUT_AS_GUEST'); ?>" type="submit"
					        onclick="javascript:return myValidator(userForm, '<?php echo $this->fTask; ?>');"><?php echo JText::_ ('COM_VIRTUEMART_CHECKOUT_AS_GUEST'); ?></a>
					<?php } ?>
				<a class="continue_link" type="reset"
				        onclick="window.location.href='<?php echo JRoute::_ ('index.php?option=com_virtuemart&view=' . $rview); ?>'"><?php echo JText::_ ('COM_VIRTUEMART_CANCEL'); ?></a>


	
		</div>



<input type="hidden" name="option" value="com_virtuemart"/>
<input type="hidden" name="view" value="user"/>
<input type="hidden" name="controller" value="user"/>
<input type="hidden" name="task" value="<?php echo $this->fTask; // I remember, we removed that, but why?   ?>"/>
<input type="hidden" name="layout" value="<?php echo $this->getLayout (); ?>"/>
<input type="hidden" name="address_type" value="<?php echo $this->address_type; ?>"/>
<?php if (!empty($this->virtuemart_userinfo_id)) {
	echo '<input type="hidden" name="shipto_virtuemart_userinfo_id" value="' . (int)$this->virtuemart_userinfo_id . '" />';
}
echo JHTML::_ ('form.token');
?>
</form>
