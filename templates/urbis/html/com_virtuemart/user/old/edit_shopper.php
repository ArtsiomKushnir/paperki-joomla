<?php
/**
 *
 * Modify user form view, User info
 *
 * @package	VirtueMart
 * @subpackage User
 * @author Oscar van Eijk
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: edit_shopper.php 6037 2012-05-17 17:45:32Z Milbo $
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

?>
<?php if(!$this->userDetails->user_is_vendor){ ?>

<?php } ?>
<?php if( $this->userDetails->virtuemart_user_id!=0)  {
 //   echo $this->loadTemplate('vmshopper');
    } ?>
<?php echo $this->loadTemplate('address_userfields'); ?>

<?php if( $this->userDetails->virtuemart_user_id==0)  {?>

<table class="adminform user-details" style="margin-top: -7px;">
<tbody>
<tr>
<td class="key">
<label class="agreed" for="agreed_field">
Решите простой пример: (проверка на спам)</label>
</td>
<td>
<?php 
$numo = rand(2, 10);
$numt = rand(1, 9);
echo $numo.' + '.$numt.' = ';
?>
<input type="text" class="required" name="primer" style="width: 70px;">
<input type="hidden" name="primero" value="<?php echo $numo*22735;?>">
<input type="hidden" name="primert" value="<?php echo $numt*124735;?>">
</td>
</tr>
</tbody>
</table>
<br/>
<?php
}
 if(!empty($this->virtuemart_userinfo_id)){
	echo '<input type="hidden" name="virtuemart_userinfo_id" value="'.(int)$this->virtuemart_userinfo_id.'" />';
}
?>
<input type="hidden" name="task" value="<?php echo $this->fTask; // I remember, we removed that, but why?   ?>" />
<input type="hidden" name="address_type" value="BT" />
<div>
<?php 			if ($this->userId == 0) {
?>
<a class="vm-button-correct"  onclick="javascript:return myValidator(userForm, 'saveUser');"  href="javascript:document.checkoutForm.submit();"><span>Зарегистрироваться</span></a>
<?php
			}
			else {
				?>
<a class="vm-button-correct"  onclick="javascript:return myValidator(userForm, 'saveUser');"  href="javascript:document.checkoutForm.submit();"><span>Сохранить</span></a>


<?php }	?>	&nbsp;
<a class="continue_link"  onclick="window.location.href='<?php echo JRoute::_('index.php?option=com_virtuemart&view=user'); ?>'" ><span>Отменить изменения</span></a>
</div>
