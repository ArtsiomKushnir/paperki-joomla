<?php

/**
 *
 * Modify user form view, User info
 *
 * @package	VirtueMart
 * @subpackage User
 * @author Oscar van Eijk, Eugen Stranz
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: edit_address_userfields.php 6349 2012-08-14 16:56:24Z Milbo $
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

// Status Of Delimiter
$closeDelimiter = false;
$openTable = true;
$hiddenFields = '';
$i==0;
// Output: Userfields
foreach($this->userFields['fields'] as $field) {

if ($field['hidden'] == true) {	$hiddenFields .= $field['formcode'] . "\n";} 
else {


if($field['name'] == 'name'){
$field['required'] = true;
?>
<tr><td class="key" title="<?php echo $field['description'] ?>" >
		<label class="<?php echo $field['name'] ?>" for="<?php echo $field['name'] ?>_field_simple">
		<?php echo $field['title'] . ($field['required'] ? ' *' : '') ?>
		</label>
</td>
<td>
<input id="name_field_simple" type="text" maxlength="100" value="" name="name" class="required">
</td></tr>
<?php
} 

elseif($field['name'] == 'first_name') {
?>
<tr style="display: none;"><td class="key" title="<?php echo $field['description'] ?>" >
<label class="<?php echo $field['name'] ?>" for="<?php echo $field['name'] ?>_field_simple">
<?php echo $field['title'] . ($field['required'] ? ' *' : '') ?>
</label></td>
<td><input id="first_name_field_simple" type="text" maxlength="100" value="" size="30" name="first_name"></td></tr>
<?php
} 

elseif($field['name'] == 'last_name'){
?>
<tr><td class="key" title="<?php echo $field['description'] ?>" >
<label class="<?php echo $field['name'] ?>" for="<?php echo $field['name'] ?>_field_simple">
<?php echo $field['title'] . ($field['required'] ? ' *' : '') ?>
</label></td>
<td><input id="last_name_field_simple" type="text" maxlength="100" value="" size="30" name="last_name"></td></tr>
<?php
} 

elseif($field['name'] == 'phone_1'){
?>
<tr><td class="key" title="<?php echo $field['description'] ?>" >
<label class="<?php echo $field['name'] ?>_simple" for="<?php echo $field['name'] ?>_field_simple">
<?php echo $field['title'] . ($field['required'] ? ' *' : '') ?>
</label></td>
<td>
<input id="phone_1_field_simple" class="required" type="text" maxlength="32" value="" size="30" name="phone_1">
</td></tr>
<?php
} 


elseif($field['name'] == 'email'){
$field['required'] = true;
?>
<tr><td class="key" title="<?php echo $field['description'] ?>" >
		<label class="<?php echo $field['name'] ?>" for="<?php echo $field['name'] ?>_field_simple">
		<?php echo $field['title'] . ($field['required'] ? ' *' : '') ?>
		</label>
</td>
<td>
<input id="email_field_simple" class="required" type="text" maxlength="100" value="" size="30" name="email" ></td></tr>
<?php
} 


else if($field['name'] == 'agreed'){
$field['required'] == true;
?>
<tr><td class="key" title="<?php echo $field['description'] ?>" >
		<label class="<?php echo $field['name'] ?>" for="<?php echo $field['name'] ?>_field">
		<?php echo $field['title'] . ($field['required'] ? ' *' : '') ?>
		</label>
</td>
<td class="agreedf2">
		<?php echo $field['formcode'] ?>
</td></tr>
<?php
}

else {
$field['hidden'] = true;
$field['required'] = false;
$hiddenFields .= $field['formcode'] . "\n";
}

}
}
echo '<div style="display: none;">'.$hiddenFields.'</div>';
?>

<script type="text/javascript">
jQuery(document).ready(function() {
jQuery('#name_field_simple').bind('keyup', function(event){repeatnamesimple();});
});

function repeatnamesimple(){
jQuery("#first_name_field_simple").val(jQuery("#name_field_simple").val());
}




</script> 