<?php
/**
 *
 * Enter address data for the cart, when anonymous users checkout
 *
 * @package    VirtueMart
 * @subpackage User
 * @author Oscar van Eijk, Max Milbers
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: edit_address.php 6406 2012-09-08 09:46:55Z Milbo $
 */
// Check to ensure this file is included in Joomla!
defined ('_JEXEC') or die('Restricted access');
// vmdebug('user edit address',$this->userFields['fields']);
// Implement Joomla's form validation
JHTML::_ ('behavior.formvalidation');
JHTML::stylesheet ('vmpanels.css', JURI::root () . 'components/com_virtuemart/assets/css/');

$document = JFactory::getDocument();
$document->addScript(JURI::root(true). "/templates/urbis/js/jquery.cookie.js");
$document->addScriptDeclaration("
jQuery(document).ready(function() {
var isskola = jQuery.cookie('skolny_tovar');
if(isskola = 'nabor_inside'){
jQuery('#userFormSimple label.name').parent('td').hide();
jQuery('#userFormSimple label.last_name').html('Ваши ФИО');
jQuery('#name_field_simple').val('Школьный набор');
jQuery('#name_field_simple').parent('td').hide();
jQuery('#first_name_field_simple').val('Школьный набор');
}
  });
");


$document->addScriptDeclaration("
jQuery(document).ready(function() {
jQuery('.toggle').children('div.toggler').hide();
jQuery('.toggle h4').click(function() {
if(jQuery(this).parent('div.toggle').hasClass('active')){
jQuery(this).parent('div.toggle').children('div.toggler').slideUp(400,'linear');
jQuery(this).parent('div.toggle').removeClass('active');
}
else {
jQuery('.toggle').children('div.toggler').slideUp(400,'linear');
jQuery('.toggle').removeClass('active');
jQuery(this).parent('div.toggle').children('div.toggler').slideDown(400,'linear');
jQuery(this).parent('div.toggle').addClass('active');
}
  });


  });

");

if ($this->fTask === 'savecartuser') {
	$rtask = 'registercartuser';
	$url = 0;
}
else {
	$rtask = 'registercheckoutuser';
	$url = JRoute::_ ('index.php?option=com_virtuemart&view=cart&task=checkout', $this->useXHTML, $this->useSSL);
}
?>
<h1>Оформление заказа: учетные данные</h1>
<?php
echo shopFunctionsF::getLoginForm (TRUE, FALSE, $url);
?>
<script language="javascript">
 function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
    }
	function myValidator(f, t) {
		f.task.value = t; //this is a method to set the task of the form on the fTask.
		if (document.formvalidator.isValid(f)) {

<?php if($this->userDetails->virtuemart_user_id==0) { ?>
if(jQuery('#' + f.id + ' #agreed_field').attr('checked')) {
<?php } ?>


if(isValidEmailAddress(jQuery('#' + f.id + ' input.email').val())){
f.submit();
return true;
}
else {
jQuery('#' + f.id + ' .email').addClass("invalid");
alert('Неверный формат e-mail!');
return false;
 }


<?php if($this->userDetails->virtuemart_user_id==0) { ?>

		} 
else{
jQuery('#' + f.id + ' .agreed #formexample').css('color', '#ff0000');	
alert('Вы должны принять условия обслуживания! ');
}
<?php } ?>



		} else {
			var msg = '<?php echo addslashes (JText::_ ('COM_VIRTUEMART_USER_FORM_MISSING_REQUIRED_JS')); ?>';
			alert(msg + ' ');
		}
		return false;
	}

	function callValidatorForRegister(f) {

		var elem = jQuery('#username_field');
		elem.attr('class', "required");

		var elem = jQuery('#name_field');
		elem.attr('class', "required");

		var elem = jQuery('#password_field');
		elem.attr('class', "required");

		var elem = jQuery('#password2_field');
		elem.attr('class', "required");

                var elem = jQuery('#agreed_field');
		elem.attr('class', "required");

		var elem = jQuery('#userForm');

		return myValidator(f, '<?php echo $rtask ?>');

	}
</script>
<br/>
	<h2><?php if($this->userId == 0){ 
		if ($this->address_type == 'BT') {
			echo '2. Впервые оформляете заказ на нашем сайте?';
		}
		else {
			echo '2. Впервые регистрируетесь на нашем сайте?';
		}
} else {
$app = JFactory::getApplication();
$app->redirect(JRoute::_('index.php?option=com_virtuemart&view=user&layout=edit', false));
}
		?>
	</h2>
<?php if (VmConfig::get ('oncheckout_show_register', 1) && $this->userId == 0 && !VmConfig::get ('oncheckout_only_registered', 0) && $this->address_type == 'BT') {
echo '<div class="itemBody">Предлагаем воспользоваться удобным для вас способом оформления заказа: <b>регистрация учетной записи</b> на сайте (только для юрлиц) или 
 упрощенное оформление заказа <b>без регистрации учетной записи</b> (для физлиц).<br/><br/></div>';
}
?>
<div class="klass toggle toggle1">
<h4>Создать учетную запись (только для юридических лиц)</h4>
<div class="toggler" style="display: none;">
<form method="post" id="userForm" name="userForm" class="form-validate">
<!--<form method="post" id="userForm" name="userForm" action="<?php echo JRoute::_ ('index.php'); ?>" class="form-validate">-->		
<?php
if (!class_exists ('VirtueMartCart')) {	require(JPATH_VM_SITE . DS . 'helpers' . DS . 'cart.php');}
if (count ($this->userFields['functions']) > 0) {
echo '<script language="javascript">' . "\n";
echo join ("\n", $this->userFields['functions']);
echo '</script>' . "\n";
}
echo '<fieldset class="width55" style="margin-right: 10px;">';
echo $this->loadTemplate ('userfields');
if( $this->userDetails->virtuemart_user_id==0)  {?>
<tr><td class="key"><label class="agreed" for="agreed_field">Проверка на спам</label></td>
<td>
<?php 
$numo = rand(2, 10);
$numt = rand(1, 9);
echo $numo.' + '.$numt.' = ';
?>
<input type="text" class="required" name="primer" style="width: 70px;">
<input type="hidden" name="primero" value="<?php echo $numo*22735;?>">
<input type="hidden" name="primert" value="<?php echo $numt*124735;?>">
</td></tr></tbody></table><br/>
<?php }
?>

<?php if (strpos ($this->fTask, 'cart') || strpos ($this->fTask, 'checkout')) {
$rview = 'cart';}
else {	$rview = 'user';}
$buttonclass = 'button vm-button-correct';



			if (VmConfig::get ('oncheckout_show_register', 1) && $this->userId == 0 && $this->address_type == 'BT' and $rview == 'cart') {
				?>

				<input class="addtocart-button" style="width: 210px;"  type="submit" onclick="javascript:return callValidatorForRegister(userForm);"
				        title="<?php echo JText::_ ('COM_VIRTUEMART_REGISTER_AND_CHECKOUT'); ?>" value="Зарегистрироваться и оформить" />
			
				<?php
			}
			else {
				?>

				<button class="<?php echo $buttonclass ?>" type="submit"
				        onclick="javascript:return myValidator(userForm, '<?php echo $this->fTask; ?>');"><?php echo JText::_ ('COM_VIRTUEMART_SAVE'); ?></button>
				<button class="default" type="reset"
				        onclick="window.location.href='<?php echo JRoute::_ ('index.php?option=com_virtuemart&view=' . $rview); ?>'"><?php echo JText::_ ('COM_VIRTUEMART_CANCEL'); ?></button>

				<?php } ?>
		
<div class="clr"><br/></div>
<?php
echo '</fieldset>';
?>
<input type="hidden" name="option" value="com_virtuemart"/>
<input type="hidden" name="view" value="user"/>
<input type="hidden" name="controller" value="user"/>
<input type="hidden" name="task" value="<?php echo $this->fTask; // I remember, we removed that, but why?   ?>"/>
<input type="hidden" name="layout" value="<?php echo $this->getLayout (); ?>"/>
<input type="hidden" name="address_type" value="<?php echo $this->address_type; ?>"/>
<?php if (!empty($this->virtuemart_userinfo_id)) {
	echo '<input type="hidden" name="shipto_virtuemart_userinfo_id" value="' . (int)$this->virtuemart_userinfo_id . '" />';
}
echo JHTML::_ ('form.token');
?>

</form>
</div>
</div>






<?php
///////регистрация по гостю/////////////////////////////////////////////////////////////////////////////////
if (VmConfig::get ('oncheckout_show_register', 1) && $this->userId == 0 && $this->address_type == 'BT' and $rview == 'cart' && !VmConfig::get ('oncheckout_only_registered', 0)) {
?>
<div class="klass toggle toggle2">
<h4>Оформить без регистрации учетной записи</h4>
<div class="toggler" style="display: none;">
<form method="post" id="userFormSimple" name="userFormSimple" class="form-validate">
<!--<form method="post" id="userFormSimple" name="userFormSimple" action="<?php echo JRoute::_ ('index.php'); ?>" class="form-validate">-->		
<?php
if (!class_exists ('VirtueMartCart')) {	require(JPATH_VM_SITE . DS . 'helpers' . DS . 'cart.php');}
if (count ($this->userFields['functions']) > 0) {
echo '<script language="javascript">' . "\n";
echo join ("\n", $this->userFields['functions']);
echo '</script>' . "\n";
}
echo '<fieldset class="width55">';
echo '<table  class="adminForm user-details">';
echo $this->loadTemplate ('userfields_short');
if( $this->userDetails->virtuemart_user_id==0)  {?>
<tr><td class="key"><label class="agreed_simle" for="primer_field_simle">Проверка на спам</label></td>
<td>
<?php 
$numo = rand(2, 10);
$numt = rand(1, 9);
echo $numo.' + '.$numt.' = ';
?>
<input type="text" id="primer_field_simle" class="required" name="primer" style="width: 70px;">
<input type="hidden" name="primero" value="<?php echo $numo*22735;?>">
<input type="hidden" name="primert" value="<?php echo $numt*124735;?>">
</td></tr></tbody></table><br/>
<?php }									
?>
<input class="addtocart-button" style="width: 210px;"   title="<?php echo JText::_ ('COM_VIRTUEMART_CHECKOUT_AS_GUEST'); ?>" type="submit"
 onclick="javascript:return myValidator(userFormSimple, '<?php echo $this->fTask; ?>');" value="<?php echo JText::_ ('COM_VIRTUEMART_CHECKOUT_AS_GUEST'); ?>" />
<div class="clr"><br/></div>
</fieldset>
<input type="hidden" name="option" value="com_virtuemart"/>
<input type="hidden" name="view" value="user"/>
<input type="hidden" name="controller" value="user"/>
<input type="hidden" name="task" value="<?php echo $this->fTask; // I remember, we removed that, but why?   ?>"/>
<input type="hidden" name="layout" value="<?php echo $this->getLayout (); ?>"/>
<input type="hidden" name="address_type" value="<?php echo $this->address_type; ?>"/>
<?php if (!empty($this->virtuemart_userinfo_id)) {
	echo '<input type="hidden" name="shipto_virtuemart_userinfo_id" value="' . (int)$this->virtuemart_userinfo_id . '" />';
}
echo JHTML::_ ('form.token');
?>

</form>
</div>
</div>
<?php } ?>
<div class="clr"><br/></div>