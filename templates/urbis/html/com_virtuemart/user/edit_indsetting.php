<?php
/**
*
* User details, Orderlist
*
* @package	VirtueMart
* @subpackage User
* @author Oscar van Eijk
* @link http://www.virtuemart.net
* @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* @version $Id: edit_orderlist.php 5351 2012-02-01 13:40:13Z alatak $
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access'); 


JPluginHelper::importPlugin('content');
$user = JFactory::getUser();
$db = JFactory::getDBO();
$sql = "SELECT * FROM #__acymailing_subscriber WHERE userid = '".$user->id."'";
$db->setQuery($sql);
$loadeduser = $db->loadObject();
echo JHtml::_('content.prepare', "{component url='index.php?option=com_acymailing&view=user&layout=modify&Itemid=304&subid=.".$loadeduser->subid."&key=".$loadeduser->key."' }", '', 'mod_custom.content');


?>


