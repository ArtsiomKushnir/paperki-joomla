<?php
/**
*
* Order detail view
*
* @package	VirtueMart
* @subpackage Orders
* @author Oscar van Eijk, Valerie Isaksen
* @link http://www.virtuemart.net
* @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* @version $Id: details.php 6246 2012-07-09 19:00:20Z Milbo $
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
JHTML::stylesheet('vmpanels.css', JURI::root().'components/com_virtuemart/assets/css/');
if($this->print){
	?>
<style type="text/css">
            body, td, p, th {  font: 14px Arial; color: #111111;}
	    table.html-email {margin:10px auto;background:#fff;}
a {
    color: #015DCB;
    outline: medium none;
    text-decoration: underline;
}
 h2 {
    color: #015DCB;
    font: 14px Arial;
    margin-bottom: 10px;
    text-align: left;
    text-decoration: none;
}
	    span.grey {color:#666;}
	    span.date {color:#666; }
small{color: #555555; font: bold 14px;}
	    a.default:link, a.default:hover, a.default:visited {color:#333333;line-height:22px;background: #f2f2f2;margin: 10px 0px 10px 5px;padding: 2px 4px 1px 4px;border: solid #CAC9C9 1px;border-radius: 4px;-webkit-border-radius: 4px;-moz-border-radius: 4px;text-shadow: 1px 1px 1px #f2f2f2;font-size: 10px;background-position: 0px 0px;display: inline-block;text-decoration: none;}
	    a.default:hover {color:#888;background: #f8f8f8;}
	    .cart-summary{ }
	    .html-email th,
.html-email
{ 
margin: 0px;
padding: 0;
 background: none repeat scroll 0 0 #E1F2FF;
    border: 1px solid #FFFFFF;
    border-radius: 4px;
    box-shadow: 0 1px 2px #AAAAAA;
    color: #777777;
}


.html-email{ 
 border-collapse: separate;
}

.html-email th h2{
text-align: center;
padding: 7px 0 0 0;
font: 16px Arial;
}
	    .sectiontableentry2,  .cart-summary th{ background: #E1F2FF;margin: 0px;padding: 10px;}
	    .sectiontableentry1, .html-email td, .cart-summary td {
   border: 1px solid #E1F2FF;
    padding: 1px 5px;
    vertical-align: middle;
background: #fff;margin: 0px;}
	    .line-through{text-decoration:line-through}
table tr td p{padding: 0; margin: 0;}
.vmshipment_description{display: none;}	
.sectiontableheader td{ 
background: none repeat scroll 0 0 #E1F2FF;
color: #222;
}


.product-table h3{
font: 11px Arial;
color: #888999;
text-align: left;
margin: 1px 0;
padding: 0;
}
.product-table h3 a{
font: 12px Arial;
text-decoration: none;
}
</style>
		<body onload="javascript:print();">
<img style="float:left; margin-right: 20px;" src="<?php  echo JURI::root() . $this-> vendor->images[0]->file_url ?>">
<h1><?php echo JText::_('COM_VIRTUEMART_ACC_ORDER_INFO'); ?></h1>
<small>ООО «ПАПЕРКИ» - интернет-магазин офисного обслуживания юридических лиц. <?php  echo '<a href="'.$this->vendor->vendor_url.'" target="_blank">'.$this->vendor->vendor_url.'</a></small>'; ?>
		<div style="clear: both;"> </div>

		

<div class='spaceStyle'>
		<?php
		echo $this->loadTemplate('order');
		?>
		</div>

		<div class='spaceStyle'>
		<?php
		echo $this->loadTemplate('items');
		?>
		</div>
		<?php	echo $this->vendor->vendor_legal_info; ?>
		</body>
		<?php
} else {

	?>
	<h1><?php echo JText::_('COM_VIRTUEMART_ACC_ORDER_INFO'); ?>

	<?php
$user_id = JFactory::getUser();

	/* Print view URL */
	$details_url = juri::root().'index.php?option=com_virtuemart&view=orders&layout=details&tmpl=component&virtuemart_order_id=' . $this->orderdetails['details']['BT']->virtuemart_order_id .'&order_pass=' . JRequest::getString('order_pass',false) .'&order_number='.JRequest::getString('order_number',false);
	$details_link = "<a href=\"javascript:void window.open('$details_url', 'win2', 'status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no');\"  >";
	//$details_link .= '<span class="hasTip print_32" title="' . JText::_('COM_VIRTUEMART_PRINT') . '">&nbsp;</span></a>';
	$button = (JVM_VERSION==1) ? '/images/M_images/printButton.png' : 'system/printButton.png';
	$details_link .= JHtml::_('image',$button, JText::_('COM_VIRTUEMART_PRINT'), NULL, true);
	$details_link  .=  '</a>';
	echo $details_link; ?>
</h1>
<?php if($this->order_list_link && $user_id>0){ ?>
	<div class='spaceStyle'>
	    <div  class="floatleft" style="margin-right: 15px;">
		<a class="button" href="<?php echo $this->order_list_link ?>">Список ваших заказов</a>
	    </div>
	    <div>
		<a class="button" href="index.php?option=com_virtuemart&view=user&layout=edit">Ваш личный кабинет</a>
	    </div>
	    <div class="clear"></div>
	</div>
<?php }?>
<div class='spaceStyle'>
	<?php
	echo $this->loadTemplate('order');
	?>
	</div>

	<div class='spaceStyle'>
	<?php

	$tabarray = array();

	$tabarray['items'] = 'COM_VIRTUEMART_ORDER_ITEM';
	$tabarray['history'] = 'История изменения заказа';

	shopFunctionsF::buildTabs ( $this, $tabarray); ?>
	 </div>
	    <br clear="all"/><br/>
	    <pre><?php// print_r($this); ?></pre><br/>
	<?php
}

?>