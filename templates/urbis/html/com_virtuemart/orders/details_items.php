<?php
/**
*
* Order items view
*
* @package	VirtueMart
* @subpackage Orders
* @author Oscar van Eijk, Valerie Isaksen
* @link http://www.virtuemart.net
* @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* @version $Id: details_items.php 5836 2012-04-09 13:13:21Z Milbo $
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

if($this->format == 'pdf'){
	$widthTable = '100';
	$widtTitle = '27';
} else {
	$widthTable = '100';
	$widtTitle = '49';
}
$catModel = VmModel::getModel('Category');
$prodModel = VmModel::getModel('Product');

vmJsApi::js( 'fancybox/jquery.fancybox-1.3.4.pack');
vmJsApi::css('jquery.fancybox-1.3.4');
$document = JFactory::getDocument ();
$imageJS = '
jQuery(document).ready(function() {
	jQuery("a[rel=vm-additional-images]").fancybox({
		"titlePosition" 	: "inside",
		"transitionIn"	:	"elastic",
		"transitionOut"	:	"elastic"
	});
});
';
$document->addScriptDeclaration ($imageJS);

?>

<table class="ordercart-summary html-email width100 product-table">
<tr>
<th width="30px">Фото</th>
<th align="center" width="40px">Код</th>
<th align="right" width="250px"><?php echo JText::_('COM_VIRTUEMART_PRODUCT_NAME_TITLE') ?></td>
<th align="right" width="100px"><?php echo JText::_('COM_VIRTUEMART_ORDER_PRINT_PRICE') ?></td>
<th align="right" width="100px"><?php echo JText::_('COM_VIRTUEMART_ORDER_PRINT_QTY') ?></td>
<th align="right" width="100px">Всего с НДС</td>
	</tr>
<?php

	foreach($this->orderdetails['items'] as $item) {
$qtt = $item->product_quantity ;
?>
<tr valign="top" class="sectiontableentry<?php echo $i ?>">
	
<td class="product-tableimage"  width="30px">

<?php $product_sku = str_replace("m","", $item->order_item_sku);
////image finder
$icode = '00000000';
$len = strlen($product_sku);
if ($len == '1') {
$icode = '0000000'.$product_sku;
}else if ($len == '2') {$icode = '000000'.$product_sku;}
else if ($len == '3') {$icode = '00000'.$product_sku;}
else if ($len == '4') {$icode = '0000'.$product_sku;}
else if ($len == '5') {$icode = '000'.$product_sku;}
else if ($len == '6') {$icode = '00'.$product_sku;}
else if ($len == '7') {$icode = '0'.$product_sku;}
else if ($len == '8') {$icode =  $product_sku;}
$full_image = '';
$pas = JURI::root().'images/catalog/'.$icode.'.jpg';
$pas2 = JURI::root().'images/catalog/'.$icode.'.png';
$pas3 = JURI::root().'images/catalog/'.$icode.'.gif';
$pas4 = JURI::root().'images/catalog/'.$icode.'.jpeg';

if (file_exists($pas)) {
$full_image = $pas;
}
else if (file_exists($pas2)) {$full_image = $pas2;}
else if (file_exists($pas3)) {$full_image = $pas3;}
else if (file_exists($pas4)) {$full_image = $pas4;}
else{$full_image =  $pas;}
$pros = $prodModel->getProduct($item->virtuemart_product_id);
$newcat = $catModel->getCategory(max($pros->categories));

?>

<a href="<?php echo $this->baseurl ?>/images/catalog/<?php echo $icode; ?>.jpg" onclick="return hs.expand(this,{captionText: this.getAttribute('caption'), slideshowGroup: 'group<?php echo $item->virtuemart_product_id;?>'})" class="highslide"  title="<?php echo $newcat->category_name.' '.$prow->product_name ?>"  caption="<?php echo $newcat->category_name.' '.$item->order_item_name ?>" target="_blank" >
<img src="<?php echo $full_image; ?>"   alt="<?php echo $item->order_item_name; ?>" style="width: 30px; height: auto;" />
</a>
</td>

<td align="center"><?php echo substr($icode, -4, 4);?></td>

<td align="left" >
<h3>
<?php echo JHTML::link ( JRoute::_ ( 'index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $item->virtuemart_product_id . '&virtuemart_category_id=' . $newcat->virtuemart_category_id ), $newcat->category_name, array ('title' => $item->order_item_name ) ); 
echo '<br/>'.$item->order_item_name;
	if (!empty($item->product_attribute)) {
							if(!class_exists('VirtueMartModelCustomfields'))require(JPATH_VM_ADMINISTRATOR.DS.'models'.DS.'customfields.php');
							$product_attribute = VirtueMartModelCustomfields::CustomsFieldOrderDisplay($item,'FE');
						echo $product_attribute;
					}
?>
</h3>
</td>

			<td align="right"   class="priceCol" >
				<?php
				$item->product_discountedPriceWithoutTax = (float) $item->product_discountedPriceWithoutTax;
				if (!empty($item->product_priceWithoutTax) && $item->product_discountedPriceWithoutTax != $item->product_priceWithoutTax) {
					echo '<span class="line-through">'.$this->currency->priceDisplay($item->product_item_price, $this->currency) .'</span><br />';
					echo '<span >'.$this->currency->priceDisplay($item->product_discountedPriceWithoutTax, $this->currency) .'</span><br />';
				} else {
					echo '<span >'.$this->currency->priceDisplay($item->product_item_price, $this->currency) .'</span><br />'; 
				}
				?>
			</td>
			<td align="right" >
				<?php echo $qtt; ?>
			</td>
			

			<td align="right"  class="priceCol">
				<?php
				$item->product_basePriceWithTax = (float) $item->product_basePriceWithTax;
				$class = '';
				if(!empty($item->product_basePriceWithTax) && $item->product_basePriceWithTax != $item->product_final_price ) {
					echo '<span class="line-through" >'.$this->currency->priceDisplay($item->product_basePriceWithTax,$this->currency,$qtt) .'</span><br />' ;
				}
				elseif (empty($item->product_basePriceWithTax) && $item->product_item_price != $item->product_final_price) {
					echo '<span class="line-through">' . $this->currency->priceDisplay($item->product_item_price,$this->currency,$qtt) . '</span><br />';
				}

				echo $this->currency->priceDisplay(  $item->product_subtotal_with_tax ,$this->currency); //No quantity or you must use product_final_price ?>
			</td>
		</tr>


</tr>
	<?php

} ?>

 <tr class="sectiontableentry1">
			<td colspan="5" align="right"><?php echo JText::_('COM_VIRTUEMART_ORDER_PRINT_PRODUCT_PRICES_TOTAL'); ?></td>
			<td align="right"><?php echo $this->currency->priceDisplay($this->orderdetails['details']['BT']->order_salesPrice,$this->currency) ?></td>
 </tr>


<?php if($this->orderdetails['details']['BT']->order_billDiscountAmount > 0) {?>
 <tr class="sectiontableentry1">
<td colspan="5" align="right">Итого скидка: </td>
<td align="right"><span  class='priceColor2'><?php echo $this->currency->priceDisplay($this->orderdetails['details']['BT']->order_billDiscountAmount, $this->currency); ?></span></td>
 </tr>
 <?php } ?>

<?php if ($this->orderdetails['details']['BT']->coupon_discount <> 0.00) {
    $coupon_code=$this->orderdetails['details']['BT']->coupon_code?' ('.$this->orderdetails['details']['BT']->coupon_code.')':'';
	?>
	<tr>
<td colspan="5" align="right">Скидка от суммы заказа: </td>
<td align="right"><?php echo $this->currency->priceDisplay($this->orderdetails['details']['BT']->coupon_discount,$this->currency); ?></td>
	</tr>
<?php  } ?>




	<tr class="sectiontableentry2">
		<td align="right" class="pricePad" colspan="5"><strong><?php echo JText::_('COM_VIRTUEMART_ORDER_PRINT_TOTAL') ?></strong></td>
		<td align="right"><strong><?php echo $this->currency->priceDisplay($this->orderdetails['details']['BT']->order_total, $this->currency); ?></strong></td>
	</tr>

</table>
