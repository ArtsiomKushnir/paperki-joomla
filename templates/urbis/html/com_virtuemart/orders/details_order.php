<?php
/**
 *
 * Order detail view
 *
 * @package	VirtueMart
 * @subpackage Orders
 * @author Oscar van Eijk, Valerie Isaksen
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: details_order.php 5341 2012-01-31 07:43:24Z alatak $
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

?>
<br/><div class="clear"> </div>
<table class="order-summary" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr>
	<td class="key"><?php echo JText::_('COM_VIRTUEMART_ORDER_PRINT_PO_NUMBER') ?>: </td>
	<td class="orders-key" align="left">
	    <?php echo $this->orderdetails['details']['BT']->order_number; ?>
	</td>
    </tr>
    <tr>
	<td  class="key"><?php echo JText::_('COM_VIRTUEMART_ORDER_PRINT_PO_DATE') ?>: </td>
	<td align="left"><?php echo vmJsApi::date($this->orderdetails['details']['BT']->created_on, 'LC4', true); ?></td>
    </tr>
    <tr>
	<td  class="key"><?php echo JText::_('COM_VIRTUEMART_ORDER_PRINT_PO_STATUS') ?>: </td>
	<td align="left"><?php echo $this->orderstatuses[$this->orderdetails['details']['BT']->order_status]; ?></td>
    </tr>

    <tr>
	<td  class="key">Способ доставки:</td>
	<td align="left"><?php echo $this->shipment_name;?></td>
    </tr>

    <tr>
	<td  class="key">Способ оплаты:</td>
	<td align="left"><?php echo $this->payment_name;?></td>
    </tr>

	 <tr>
    <td class="key">Ваш комментарий: </td>
    <td valign="top" align="left"><?php echo $this->orderdetails['details']['BT']->customer_note; ?></td>
	</tr>

	 <tr>
    <td class="key">Бонусы к заказу: </td>
    <td valign="top" align="left">
    <?php
        if ($this->orderdetails['details']['BT']->coupon_code == 'noller' && $this->orderdetails['details']['BT']->order_subtotal > 100)
        {
            echo '<span>Подарок: </span>';
            echo 'Спросите у менеджера про подарок';
        }
        else if ($this->orderdetails['details']['BT']->coupon_code == 'skidkaXEROXPerformerA4')
        {
            echo '<span>Супер цена на Xerox Performer A4</span>';
        }
        else if ($this->orderdetails['details']['BT']->coupon_code !== 'noller' && $this->orderdetails['details']['BT']->coupon_code != 'skidkaXEROXPerformerA4' && $this->orderdetails['details']['BT']->order_subtotal > 100)
        {
            echo '<span>Скидка: </span>';
            echo '('.$this->currency->priceDisplay($this->orderdetails['details']['BT']->coupon_discount, $this->currency).')';
        }
        else
        {
            echo '<span>бонусы недоступны</span>';
        }
    ?>
     	
    </td>
	</tr>

     <tr>
	<td class="key">Всего к оплате: </td>
	<td class="orders-key" align="left"><?php echo $this->currency->priceDisplay($this->orderdetails['details']['BT']->order_total, $this->currency); ?></td>
    </tr>
</table>

<?php
    $debug = JRequest::getInt('debug', 0);
    if ($debug == 1) {
        echo '<pre>';
        print_r($this);
        echo '</pre>';
    }
?>