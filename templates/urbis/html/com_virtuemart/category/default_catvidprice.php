<?php 
defined ('_JEXEC') or die('Restricted access');
$categoryModel = VmModel::getModel('category');
	if(class_exists('VirtueMartCart')){
	$cart = VirtueMartCart::getCart(false);
	}
	$prodsincart = array();
	if(!empty($cart->products)){
	foreach ($cart->products as $cartprods)
	{
	$prodsincart[$cartprods->virtuemart_product_id] = $cartprods->quantity; 
	}
	}




?>
<table class="width100 product-table tabheader">
<tr>
<td style="padding: 0; width: 33px; text-align: center;"><small>Фото</small></td>
<td class="product-tablekod"><small>Код</small></td>
<td style="width: 10px; padding: 0"> </td>
<td class="product-tablename">Наименование</td>
<td class="product-tableprice">Цена с НДС</td>
<td class="tabletocart">Заказ</td>
</tr>
</table>
<?php
foreach ($this->products as $product ) { 
$allcats = '';

/* 12.07.2015
$allcats = '';
if($product->categories[0] == $this->category->virtuemart_category_id){
$newcatcategoryId = $product->categories[1];
} else $newcatcategoryId = $product->categories[0];
if(!empty($this->keyword)){
$newcatcategoryId = (max($product->categories));
}
$newcat = $categoryModel->getCategory($newcatcategoryId);
12.07.2015 */

foreach ($product->categories as $cats)
{$category = $categoryModel->getCategory($cats);
if($category->haschildren){
}elseif($category->virtuemart_category_id != 5685) $newcat = $category;
}

$product_sku = str_replace("m","", $product->product_sku);
////image finder
$icode = '00000000';
$len = strlen($product_sku);
if ($len == '1') {
$icode = '0000000'.$product_sku;
}else if ($len == '2') {$icode = '000000'.$product_sku;}
else if ($len == '3') {$icode = '00000'.$product_sku;}
else if ($len == '4') {$icode = '0000'.$product_sku;}
else if ($len == '5') {$icode = '000'.$product_sku;}
else if ($len == '6') {$icode = '00'.$product_sku;}
else if ($len == '7') {$icode = '0'.$product_sku;}
else if ($len == '8') {$icode =  $product_sku;}
$full_image = '';
$pas = 'images/catalog/'.$icode.'.jpg';
$pas2 = 'images/catalog/'.$icode.'.png';
$pas3 = 'images/catalog/'.$icode.'.gif';
$pas4 = 'images/catalog/'.$icode.'.jpeg';

$full_image = '';
if (file_exists($pas)) {
$full_image = $pas;
}
else if (file_exists($pas2)) {$full_image = $pas2;}
else if (file_exists($pas3)) {$full_image = $pas3;}
else if (file_exists($pas4)) {$full_image = $pas4;}

$full_image2 = '';
if (file_exists('images/catalog/'.$icode.'-1.jpg')) { $full_image2 = 'images/catalog/'.$icode.'-1.jpg';}
else if (file_exists('images/catalog/'.$icode.'-1.png')) {$full_image2 = 'images/catalog/'.$icode.'-1.png';}
else if (file_exists('images/catalog/'.$icode.'-1.gif')) {$full_image2 = 'images/catalog/'.$icode.'-1.gif';}
else if (file_exists('images/catalog/'.$icode.'-1.jpeg')) {$full_image2 = 'images/catalog/'.$icode.'-1.jpeg';}

$full_image3 = '';
if (file_exists('images/catalog/'.$icode.'-2.jpg')) { $full_image3 = 'images/catalog/'.$icode.'-2.jpg';}
else if (file_exists('images/catalog/'.$icode.'-2.png')) {$full_image3 = 'images/catalog/'.$icode.'-2.png';}
else if (file_exists('images/catalog/'.$icode.'-2.gif')) {$full_image3 = 'images/catalog/'.$icode.'-2.gif';}
else if (file_exists('images/catalog/'.$icode.'-2.jpeg')) {$full_image3 = 'images/catalog/'.$icode.'-2.jpeg';}

$full_image4 = '';
if (file_exists('images/catalog/'.$icode.'-3.jpg')) { $full_image4 = 'images/catalog/'.$icode.'-3.jpg';}
else if (file_exists('images/catalog/'.$icode.'-3.png')) {$full_image4 = 'images/catalog/'.$icode.'-3.png';}
else if (file_exists('images/catalog/'.$icode.'-3.gif')) {$full_image4 = 'images/catalog/'.$icode.'-3.gif';}
else if (file_exists('images/catalog/'.$icode.'-3.jpeg')) {$full_image4 = 'images/catalog/'.$icode.'-3.jpeg';}
//////end image finder
?>

<div class="width100 clear">
<?php
///количество в корзине
if (array_key_exists($product->virtuemart_product_id, $prodsincart)) {
echo '<div class="prodsincart incart'.$product->virtuemart_product_id.'"><span class="pquant">'.$prodsincart[$product->virtuemart_product_id].'</span> в корзине ';
echo '<span class="remove" onClick="Virtuemart.removecart('.$product->virtuemart_product_id.'); return false;" title="Удалить товар из заказа"> </span></div>';
}
else {
echo '<div class="prodsincart prodsincarthidden incart'.$product->virtuemart_product_id.'"><span class="pquant">0</span> в корзине ';
echo '<span class="remove" onClick="Virtuemart.removecart('.$product->virtuemart_product_id.'); return false;" title="Удалить товар из заказа"> </span></div>';
}
///количество в корзине

?>
<table class="width100 product-table">
<tr>
<td class="product-tableimage">
<?php if($full_image){ ?>
<a href="<?php echo $this->baseurl ?>/images/catalog/<?php echo $icode; ?>.jpg" onclick="return hs.expand(this,{captionText: this.getAttribute('caption'), slideshowGroup: 'group<?php echo $product->id;?>'})" class="highslide"  title="<?php echo $category->category_name.' '.$product->product_name ?>"  caption="<?php echo $category->category_name.' '.$product->product_name ?>" target="_blank" >
<img src="templates/urbis/images/grey.gif" data-original="<?php echo $this->baseurl.'/'.$full_image; ?>"   alt="<?php echo $product->product_name ?>" class="lazy" />
</a>
<?php } else {
$full_image = 'images/catalog/noimage.jpg';
 ?>
<img src="templates/urbis/images/grey.gif" data-original="<?php echo $this->baseurl.'/'.$full_image; ?>"   alt="<?php echo $product->product_name ?>" class="lazy" />
<?php } ?>
</td>
<td class="product-tablekod">
<?php echo substr($icode, -5, 5);?>

<?php if($full_image2 || $full_image3 || $full_image4){ 
$document = &JFactory::getDocument();
$document->addScriptDeclaration("
hs.addSlideshow({
slideshowGroup: 'group".$product->id."',
thumbstrip: {
		position: 'top right',
		mode: 'vertical',
		relativeTo: 'expander',
		offsetX: 5,
		offsetY: -5
	}
});
");
}
?>

<?php if($full_image2){ ?>
<div style="display: none;">
<a href="<?php echo $this->baseurl.'/'.$full_image2; ?>" onclick="return hs.expand(this,{captionText: this.getAttribute('caption'), slideshowGroup: 'group<?php echo $product->id;?>'})" class="highslide"  title="<?php echo $category->category_name.' '.$product->product_name ?>"  caption="<?php echo $category->category_name.' '.$product->product_name ?>" target="_blank" >
<img src="<?php echo $this->baseurl.'/'.$full_image2; ?>" alt="<?php echo $product->product_name ?>" />
</a>

<?php if($full_image3){ ?>
<a href="<?php echo $this->baseurl.'/'.$full_image3; ?>" onclick="return hs.expand(this,{captionText: this.getAttribute('caption'), slideshowGroup: 'group<?php echo $product->id;?>'})" class="highslide"  title="<?php echo $category->category_name.' '.$product->product_name ?>"  caption="<?php echo $category->category_name.' '.$product->product_name ?>" target="_blank" >
<img src="<?php echo $this->baseurl.'/'.$full_image3; ?>" alt="<?php echo $product->product_name ?>" />
</a>
<?php } ?>
<?php if($full_image4){ ?>
<a href="<?php echo $this->baseurl.'/'.$full_image4; ?>" onclick="return hs.expand(this,{captionText: this.getAttribute('caption'), slideshowGroup: 'group<?php echo $product->id;?>'})" class="highslide"  title="<?php echo $category->category_name.' '.$product->product_name ?>"  caption="<?php echo $category->category_name.' '.$product->product_name ?>" target="_blank" >
<img src="<?php echo $this->baseurl.'/'.$full_image4; ?>" alt="<?php echo $product->product_name ?>" />
</a>
<?php } ?>
</div>
<?php } ?>
</td>
<td style="width: 10px; padding: 0;">

<a class="highslide stockindicator" href="index.php?option=com_content&view=article&id=289&tmpl=component" onclick="return hs.htmlExpand( this, { objectType: 'iframe', outlineType: 'rounded-white', width: 700 } )" style="<?php
$stock_level = $product->product_in_stock - $product->product_ordered;
$stocktext = 'background: #9CC446;" title="'.JText::_ ('COM_VIRTUEMART_STOCK_LEVEL_DISPLAY_NORMAL_TIP').'"';
if($stock_level <= 4){$stocktext = 'background: #F3D44C;" title="'.JText::_ ('COM_VIRTUEMART_STOCK_LEVEL_DISPLAY_LOW_TIP').'"';
} 
//if($stock_level <= 0 ) $stocktext = 'background: #D70300;" title="'.JText::_ ('COM_VIRTUEMART_STOCK_LEVEL_DISPLAY_OUT_TIP').'"';
echo $stocktext;
?>
> </a></td>
<td class="product-tablename">
<h3>
<?php // Product Name
echo JHTML::link ( JRoute::_ ( 'index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $product->virtuemart_product_id . '&virtuemart_category_id=' . $newcat->virtuemart_category_id ), $newcat->category_name, array ('title' => $product->product_name ) ); 
echo '<br/>'.$product->product_name;
?>
</h3>
</td>

<?php
if(!empty($product->prices['salesPrice'])) {
$db = JFactory::getDBO();
$sql = "SELECT MIN(product_price) FROM #__virtuemart_product_prices WHERE virtuemart_product_id = '".$product->virtuemart_product_id."'";
$db->setQuery($sql);
$prices = $db->loadResult();
$skidkaclass= '';
if($prices < $product->prices['salesPrice']){
$skidkaclass = ' yellow';
}
if (round($product->prices['basePrice'],$this->currency->_priceConfig['salesPrice'][1]) != $product->prices['salesPrice'] && !empty($product->prices['basePrice'])) {
$skidkaclass = ' yellow';
}
}
?>
<td class="product-tableprice<?php echo $skidkaclass;?>">
<?php 
$db = JFactory::getDBO();
$sql = "SELECT MIN(product_price) FROM #__virtuemart_product_prices WHERE virtuemart_product_id = '".$product->virtuemart_product_id."'";
$db->setQuery($sql);
$prices = $db->loadResult();
$bPrice = ($this->currency->createPriceDiv ('basePrice', ' ', $product->prices, TRUE, FALSE));
$sPrice = ($this->currency->createPriceDiv ('salesPrice', 'COM_VIRTUEMART_PRODUCT_SALESPRICE', $product->prices, TRUE, FALSE));
$sp = $product->prices['salesPrice'];

if (round($product->prices['basePrice'],$this->currency->_priceConfig['salesPrice'][1]) != $product->prices['salesPrice']) {
echo '<span style="color: #D92236; font-size: 11px; text-decoration:line-through;" >' . $bPrice . "</span></br> ";
}
if($prices < $product->prices['salesPrice']){
//$product->prices['salesPrice'] = $prices;
echo $sPrice;

// информер оптовой скидки----------------------------------------------------------
echo '<span class="help">';
$sqlp = "SELECT product_price, price_quantity_start, price_quantity_end FROM #__virtuemart_product_prices WHERE
virtuemart_product_id = '".$product->virtuemart_product_id."' ORDER BY product_price DESC";
$db->setQuery($sqlp);
$prices = $db->loadObjectList();
if(count($prices) > 1){
echo '<div class="mprices"><div class="title"><strong>Цена зависит от количества!</strong></div><table cellpadding="0" cellspacing="0" width="100%">';
foreach ($prices as $dprice){
echo '<tr><td class="help"><span>';
if($dprice->price_quantity_start == 0 && $dprice->price_quantity_end > 0) echo 'от 1 ед.: </span></td><td>'.number_format($dprice->product_price, 2, ', ', '').' р. </td></tr>';
if($dprice->price_quantity_start > 0 && $dprice->price_quantity_end > 0) echo 'от '.$dprice->price_quantity_start.' ед.: </span></td><td>'.number_format($dprice->product_price, 2, ', ', '').' р. </td></tr>';
if($dprice->price_quantity_start > 0 && $dprice->price_quantity_end == 0) echo 'от '.$dprice->price_quantity_start.' ед.: </span></td><td>'.number_format($dprice->product_price, 2, ', ', '').' р. </td></tr>';
}
echo '</table></div>';
}
echo '</span>';
// ---------------------------------------------------------------------------------

//echo '</br><span class="denominprice">'. number_format(($sp*10000), 0,'',' ').' р.</span>';
} 
else echo $sPrice;
 ?>
</td>
<td class="tabletocart">
<div class="addtocart-area">
<form method="post" class="product js-recalculate" action="<?php echo JRoute::_ ('index.php'); ?>">
<input type="hidden" value="1" name="quantity">
<div class="addtocart-bar">
<script type="text/javascript">
function check(obj) {
// use the modulus operator '%' to see if there is a remainder
remainder=obj.value % 1;
quantity=obj.value;
if (remainder != 0) {
alert('Только целые числа!!');
obj.value = quantity-remainder;
return false;
}
return true;
}
</script>
<span class="quantity-controls js-recalculate">
<input class="quantity-controls quantity-minus" type="button">
</span>
<span class="quantity-box">
<input class="quantity-input js-recalculate" type="text" value="1" onblur="check(this);" name="quantity[]">
</span>
<span class="quantity-controls js-recalculate">
<input class="quantity-controls quantity-plus" type="button">
</span>
<span class="addtocart-button">
<input class="addtocart-button" type="submit" title="Добавить в корзину" value="В корзину" rel="nofollow" name="addtocart">
</span>
<div class="clear"></div>
</div>
<input class="pname" type="hidden" value="<?php echo $category->category_name.' '.$product->product_name ?>">
<input type="hidden" value="com_virtuemart" name="option">
<input type="hidden" value="cart" name="view">
<noscript><input type="hidden" name="task" value="add"/></noscript>
<input type="hidden" value="<?php echo $product->virtuemart_product_id;?>" name="virtuemart_product_id[]">
</form>
</div>
</td>
</tr>
</table>
</div>
<?php } ?>
<div class="clear"></div>
<br>
<div class="orderby-displaynumber">
<div class="floatleft display-number">Показать по: <?php echo $this->vmPagination->getLimitBox(); ?></div>				
<div id="bottom-pagination"><?php echo $this->vmPagination->getPagesLinks(); ?></div>				
<div class="clear"></div>
</div>
