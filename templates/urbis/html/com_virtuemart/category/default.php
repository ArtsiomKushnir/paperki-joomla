<?php defined('_JEXEC') or die('Restricted access'); 
$categoryModel = VmModel::getModel('category');
	if(class_exists('VirtueMartCart')){
		$cart = VirtueMartCart::getCart(false);
	}
	$prodsincart = array();
	if(!empty($cart->products)){
		foreach ($cart->products as $cartprods)
		{
			$prodsincart[$cartprods->virtuemart_product_id] = $cartprods->quantity; 
		}
	}
?>

<div class="vestheading">
<h1><?php 
$caturl = JRoute::_ ( 'index.php?option=com_virtuemart&view=category&virtuemart_category_id=' . $this->category->virtuemart_category_id );
if($this->category->category_name) { echo $this->category->category_name; }
elseif (!empty($this->keyword)) {
echo 'Поиск: '.$this->keyword;
} else echo 'Каталог продукции';
?></h1>
</div>
<?php if(!empty($this->category->category_description)){ ?>
<div class="category_description"><?php echo $this->category->category_description ; ?></div>
<?php } ?>

<?php 
$db = JFactory::getDBO();

if($this->category->virtuemart_category_id == $this->category->parents[0]->virtuemart_category_id && $this->category->haschildren && empty($this->keyword)){
/////вид: подкатегории по прямой ссылке (от дурака)
echo $this->loadTemplate('showcats');
} 

elseif(!empty($this->keyword)){
///вид: результат поиска товаров
$session = JFactory::getSession();
$catvid = $session->get('catvid', 2);
if($catvid == 1) {
$session->set('catvid', 2);
}
echo $this->loadTemplate('showsearchproducts');
} 

else {
?>
<div class="filterblock">
<?php
$session = JFactory::getSession();
if(JRequest::getInt('catvid')){
$session->set('catvid', JRequest::getInt('catvid'));
}
$catvid = $session->get('catvid', 1);
?>
<div class="vid">Вид: 
<?php if($this->category->virtuemart_category_id != 5685) {?> &nbsp; <a <?php if($catvid == 1) echo ' class="act" '; ?> href="<?php echo JRoute::_ ( 'index.php?option=com_virtuemart&view=category&virtuemart_category_id=' . $this->category->virtuemart_category_id.'&catvid=1'.(JRequest::getInt('virtuemart_manufacturer_id')?'&virtuemart_manufacturer_id='.JRequest::getInt('virtuemart_manufacturer_id'):'') );?>">Обычный</a> &nbsp; &nbsp;| <?php } ?>
&nbsp; &nbsp;<a <?php if($catvid == 2 || ($catvid == 1 && $this->category->virtuemart_category_id == 5685)) echo ' class="act" '; ?> href="<?php echo JRoute::_ ( 'index.php?option=com_virtuemart&view=category&virtuemart_category_id=' . $this->category->virtuemart_category_id.'&catvid=2'.(JRequest::getInt('virtuemart_manufacturer_id')?'&virtuemart_manufacturer_id='.JRequest::getInt('virtuemart_manufacturer_id'):'') );?>">Плитка</a> &nbsp; &nbsp;| 
&nbsp; &nbsp;<a <?php if($catvid == 3) echo ' class="act" '; ?> href="<?php echo JRoute::_ ( 'index.php?option=com_virtuemart&view=category&virtuemart_category_id=' . $this->category->virtuemart_category_id.'&catvid=3'.(JRequest::getInt('virtuemart_manufacturer_id')?'&virtuemart_manufacturer_id='.JRequest::getInt('virtuemart_manufacturer_id'):'') );?>">Прайс-лист</a>
</div>
<div class="sortblock">
Сортировка: 
&nbsp; <?php echo $this->orderByList['orderby']; ?>
</div>
<div class="manblock">
Бренд: 
&nbsp; 
<div class="orderlistcontainer">
<div class="activeOrder" style="color: #015DCB;">
<?php 
$db = JFactory::getDBO();
$sqlm = "
SELECT DISTINCT(#__virtuemart_product_manufacturers.virtuemart_manufacturer_id), #__virtuemart_manufacturers_ru_ru.mf_name 
FROM #__virtuemart_product_manufacturers 
LEFT JOIN #__virtuemart_manufacturers_ru_ru ON #__virtuemart_manufacturers_ru_ru.virtuemart_manufacturer_id = #__virtuemart_product_manufacturers.virtuemart_manufacturer_id 
LEFT JOIN #__virtuemart_product_categories ON #__virtuemart_product_categories.virtuemart_product_id = #__virtuemart_product_manufacturers.virtuemart_product_id 
WHERE #__virtuemart_product_categories.virtuemart_category_id = '".$this->category->virtuemart_category_id."'";
$db->setQuery($sqlm);
$mans = $db->loadObjectList();
$actman = 'Выберите бренд'; 
foreach ($mans as $manufact)
{
if(JRequest::getInt('virtuemart_manufacturer_id') == $manufact->virtuemart_manufacturer_id) $actman = $manufact->mf_name; 
}
echo $actman;
?>
</div>
<div class="orderlist">
<div><a style="color: #777777;" title="Все бренды" href="<?php echo JRoute::_ ( 'index.php?option=com_virtuemart&view=category&virtuemart_category_id=' . $this->category->virtuemart_category_id );?>">Все бренды</a></div>
<?php
$manslist = array();
foreach ($mans as $manufact)
{
echo '<div><a title="'.$manufact->mf_name.'" href="'.JRoute::_ ( 'index.php?option=com_virtuemart&view=category&virtuemart_category_id=' . $this->category->virtuemart_category_id.'&virtuemart_manufacturer_id='.$manufact->virtuemart_manufacturer_id).'">'.$manufact->mf_name.'</a></div>';
}
 ?>
</div>
</div>
</div>
</div>

<?php
if ( VmConfig::get('showCategory',1) && $this->category->haschildren  && $catvid == 1) {
echo $this->loadTemplate('catvid');
}


elseif($catvid == 2) {
echo $this->loadTemplate('catvidplitka');
}

elseif($catvid == 3) {
echo $this->loadTemplate('catvidprice');
}

else {
echo $this->loadTemplate('catvidplitka');
}

?>
</div>
<?php
}
?>
<br>
