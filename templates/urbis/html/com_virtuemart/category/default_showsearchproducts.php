<?php
/**
*
* Description
*
* @package	VirtueMart
* @subpackage
* @author
* @link http://www.virtuemart.net
* @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* @version $Id: default.php 6461 2012-09-16 21:49:03Z Milbo $
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
JHTML::_( 'behavior.modal' );
?>

<div class="filterblock">
<?php
$session =& JFactory::getSession();
if(JRequest::getInt('catvid')){
$session->set('catvid', JRequest::getInt('catvid'));
}
$catvid = $session->get('catvid', 2);
if($catvid == 1){
$catvid == 2;
}
?>
<div class="vid">Вид: 
&nbsp; &nbsp;<a <?php if($catvid == 2) echo ' class="act" '; ?> href="<?php echo JRoute::_ ('index.php?option=com_virtuemart&view=category&search=true&limitstart=0&catvid=2&keyword='.$this->keyword );?>">Плитка</a> &nbsp; &nbsp;| 
&nbsp; &nbsp;<a <?php if($catvid == 3) echo ' class="act" '; ?> href="<?php echo JRoute::_ ('index.php?option=com_virtuemart&view=category&search=true&limitstart=0&catvid=3&keyword='.$this->keyword );?>">Прайс-лист</a>
</div>
<div class="sortblock">
Сортировка: 
&nbsp; <?php echo $this->orderByList['orderby']; ?>
</div>
</div>

<?php 
if($catvid == 2) {
echo $this->loadTemplate('catvidplitka');
}

elseif($catvid == 3) {
echo $this->loadTemplate('catvidprice');
}
