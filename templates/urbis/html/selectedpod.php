<?php
// no direct access
defined('_JEXEC') or die('Restricted access');

if (!class_exists('VirtueMartCart')) {require(JPATH_VM_SITE . DS . 'helpers' . DS . 'cart.php');}
$carter = VirtueMartCart::getCart();
$session =& JFactory::getSession();
$selectedgift = 0;
$selectedgift = $session->get('selectedgiftprod', 0);
if($carter->pricesUnformatted['salesPrice'] > 100){
$req = " #__virtuemart_product_categories.virtuemart_category_id = '1350'";
}
if($carter->pricesUnformatted['salesPrice'] > 200){
$req.= " OR #__virtuemart_product_categories.virtuemart_category_id = '1351'";
}
if($carter->pricesUnformatted['salesPrice'] > 400){
$req.= " OR #__virtuemart_product_categories.virtuemart_category_id = '1352'";
}
if($carter->pricesUnformatted['salesPrice'] > 650){
$req.= " OR #__virtuemart_product_categories.virtuemart_category_id = '1353'";
}
if($carter->pricesUnformatted['salesPrice'] > 950){
$req.= " OR #__virtuemart_product_categories.virtuemart_category_id = '1354'";
}
if($carter->pricesUnformatted['salesPrice'] > 1300){
$req.= " OR #__virtuemart_product_categories.virtuemart_category_id = '1355'";
}
if($carter->pricesUnformatted['salesPrice'] > 1650){
$req.= " OR #__virtuemart_product_categories.virtuemart_category_id = '1356'";
}
if($carter->pricesUnformatted['salesPrice'] > 2000){
$req.= " OR #__virtuemart_product_categories.virtuemart_category_id = '1357'";
}

$db =& JFactory::getDBO();
$query = "SELECT COUNT(#__virtuemart_products.virtuemart_product_id) FROM #__virtuemart_products LEFT JOIN #__virtuemart_product_categories ON #__virtuemart_products.virtuemart_product_id = #__virtuemart_product_categories.virtuemart_product_id WHERE ".$req;
$db->setQuery($query);
$colgifts = $db->loadResult();

if($selectedgift == 0){
?>
<span id="selectgifttext">Вы еще не выбрали себе подарок! </br></span>
</br>
<a id="selectedimagebig" class="highslide" target="_blank" style="display: none;" onclick="return hs.expand(this,{captionText: this.getAttribute('caption'), slideshowGroup: 'groupgift'})" >
<img src="images/catalog/noimage.jpg" width="60" style="float: left; margin: -5px 20px 0 5px;" id="selectedimage" />
</a>
<span id="selectedgifttext"></span>

<a class="button" id="chosegifttext" >Выбрать себе подарок <?php if($colgifts>0) echo '(доступно '.$colgifts.')';?></a> 
<span id="requestres">
<img src="templates/urbis/images/loading.gif" style="float: left; margin: -2px 20px 0 5px;" id="requestloading"/>
<span id="requestrestext">Сохранение...</span>
</span>
<div class="clear"> </br></div>
<input type="hidden" value="<?php if($colgifts>0) echo '(доступно '.$colgifts.')';?>" name="selectedgift" id="selectedgift" />
<?php }
else{ 

$productModel = VmModel::getModel('Product');
$product = $productModel->getProduct ($selectedgift, true, true, false, false);
$categoryModel = VmModel::getModel('category');
foreach ($product->categories as $cats)
{$category = $categoryModel->getCategory($cats);
if($category->haschildren || $category->virtuemart_category_id == '1349' ){
}else $newcat = $category;
}

if(($carter->pricesUnformatted['salesPrice'] > 100 && $newcat->virtuemart_category_id == '1350') 
|| ($carter->pricesUnformatted['salesPrice'] > 200 && $newcat->virtuemart_category_id == '1351')
|| ($carter->pricesUnformatted['salesPrice'] > 400 && $newcat->virtuemart_category_id == '1352')
|| ($carter->pricesUnformatted['salesPrice'] > 650 && $newcat->virtuemart_category_id == '1353')
|| ($carter->pricesUnformatted['salesPrice'] > 950 && $newcat->virtuemart_category_id == '1354')
|| ($carter->pricesUnformatted['salesPrice'] > 1300 && $newcat->virtuemart_category_id == '1355')
|| ($carter->pricesUnformatted['salesPrice'] > 1650 && $newcat->virtuemart_category_id == '1356')
|| ($carter->pricesUnformatted['salesPrice'] > 2000 && $newcat->virtuemart_category_id == '1357')) {


$product_sku = str_replace("m","", $product->product_sku);
////image finder
$icode = '00000000';
$len = strlen($product_sku);
if ($len == '1') {
$icode = '0000000'.$product_sku;
}else if ($len == '2') {$icode = '000000'.$product_sku;}
else if ($len == '3') {$icode = '00000'.$product_sku;}
else if ($len == '4') {$icode = '0000'.$product_sku;}
else if ($len == '5') {$icode = '000'.$product_sku;}
else if ($len == '6') {$icode = '00'.$product_sku;}
else if ($len == '7') {$icode = '0'.$product_sku;}
else if ($len == '8') {$icode =  $product_sku;}
$full_image = '';
$pas = 'images/catalog/'.$icode.'.jpg';
$pas2 = 'images/catalog/'.$icode.'.png';
$pas3 = 'images/catalog/'.$icode.'.gif';
$pas4 = 'images/catalog/'.$icode.'.jpeg';

if (file_exists($pas)) {
$full_image = $pas;
}
else if (file_exists($pas2)) {$full_image = $pas2;}
else if (file_exists($pas3)) {$full_image = $pas3;}
else if (file_exists($pas4)) {$full_image = $pas4;}
else{$full_image = 'images/catalog/noimage.jpg';}

$full_image2 = '';
$pas = 'images/catalog/'.$icode.'-1.jpg';
$pas2 = 'images/catalog/'.$icode.'-1.png';
$pas3 = 'images/catalog/'.$icode.'-1.gif';
$pas4 = 'images/catalog/'.$icode.'-1.jpeg';

if (file_exists($pas)) {
$full_image2 = $pas;
}
else if (file_exists($pas2)) {$full_image2 = $pas2;}
else if (file_exists($pas3)) {$full_image2 = $pas3;}
else if (file_exists($pas4)) {$full_image2 = $pas4;}
//////end image finder
?>


<span id="selectgifttext">Выбранный вами подарок: </br></span>
</br>
<a id="selectedimagebig" class="highslide" target="_blank" href="<?php echo $full_image; ?>" style="display: block;" onclick="return hs.expand(this,{captionText: this.getAttribute('caption'), slideshowGroup: 'groupgift'})" >
<img src="<?php echo $full_image; ?>" width="70" style="display: block; float: left; margin: -5px 20px 0 5px;" id="selectedimage" />
</a>
<span id="selectedgifttext">
<?php 
echo '<br/><strong>'.$product->product_name.'</strong><br/>'; 
?>
<br/><br/><br/>
</span>

<a class="button" id="chosegifttext" >Выбрать другой подарок<?php if($colgifts>0) echo ' (доступно '.$colgifts.')';?></a>
<span id="requestres">
<img src="templates/urbis/images/loading.gif" style="float: left; margin: -2px 20px 0 5px;" id="requestloading"/>
<span id="requestrestext">Сохранение...</span>
</span>
<div class="clear"></div>
<input type="hidden" value="<?php if($colgifts>0) echo '(доступно '.$colgifts.')';?>" name="selectedgift" id="selectedgift" />
<?php }



else{
$session->set('selectedgiftprod', 0);
?>
<span id="selectgifttext">Выберите себе подарок: </br></span>
</br>
<a id="selectedimagebig" class="highslide" target="_blank" style="display: none;" onclick="return hs.expand(this,{captionText: this.getAttribute('caption'), slideshowGroup: 'groupgift'})" >
<img src="images/catalog/noimage.jpg" width="60" style="float: left; margin: -5px 20px 0 5px;" id="selectedimage" />
</a>
<span id="selectedgifttext"><strong>Выбранный вами подарок не соответствует минимальной сумме заказа!</strong></br></br></span>

<a class="button" id="chosegifttext" >Выбрать другой подарок<?php if($colgifts>0) echo ' (доступно '.$colgifts.')';?></a>
<span id="requestres">
<img src="templates/urbis/images/loading.gif" style="float: left; margin: -2px 20px 0 5px;" id="requestloading"/>
<span id="requestrestext">Сохранение...</span>
</span>
<div class="clear"> </br></div>
<input type="hidden" value="<?php if($colgifts>0) echo '(доступно '.$colgifts.')';?>" name="selectedgift" id="selectedgift" />

<?php
}

}
 ?>