<?php

defined('_JEXEC') or die('Restricted access');
?>
<table class="contentpaneopen">
<tbody>
<tr>
<td class="contentheading" width="100%">
<h1>Оставить отзыв о нашей работе</h1>
</td>
</tr>
</tbody>
</table>
<br />
<div>
<a class="button" href="<?php echo JRoute::_('index.php?option=com_easybookreloaded&view=easybookreloaded'); ?>">
<strong>Вернуться к отзывам</strong>
</a>
</div>
<br />
<div id="easybook">

       
    <div class="easy_entrylink">
        <script type="text/javascript">
            function x()
            {
                return;
            }

            function insertprompt(insert, input, start, end, revisedMessage, currentMessage)
            {
                // Internet Explorer
                if (typeof document.selection != 'undefined')
                {
                    var range = document.selection.createRange();
                    range.text = insert;
                    var range = document.selection.createRange();
                    range.move('character', 0);
                    range.select();
                }
                // Gecko Software
                else if (typeof input.selectionStart != 'undefined')
                {
                    revisedMessage = currentMessage.substr(0, start) + insert + currentMessage.substr(end);
                    document.gbookForm.gbtext.value=revisedMessage;
                    document.gbookForm.gbtext.focus();
                    var pos;
                    pos = start + insert.length;
                    input.selectionStart = pos;
                    input.selectionEnd = pos;
                }
            }

            function insert(aTag, eTag)
            {
                var input = document.forms['gbookForm'].elements['gbtext'];
                input.focus();
                // Internet Explorer
                if(typeof document.selection != 'undefined')
                {
                    var range = document.selection.createRange();
                    var insText = range.text;
                    range.text = aTag + insText + eTag;
                    range = document.selection.createRange();
                    if (insText.length == 0)
                    {
                        range.move('character', -eTag.length);
                    }
                    else
                    {
                        range.moveStart('character', aTag.length + insText.length + eTag.length);
                    }
                    range.select();
                }
                // Gecko Software
                else if (typeof input.selectionStart != 'undefined')
                {
                    var start = input.selectionStart;
                    var end = input.selectionEnd;
                    var insText = input.value.substring(start, end);
                    input.value = input.value.substr(0, start) + aTag + insText + eTag + input.value.substr(end);
                    var pos;
                    if (insText.length == 0)
                    {
                        pos = start + aTag.length;
                    }
                    else
                    {
                        pos = start + aTag.length + insText.length + eTag.length;
                    }
                    input.selectionStart = pos;
                    input.selectionEnd = pos;
                }
                else
                {
                    var pos;
                    var re = new RegExp('^[0-9]{0,3}$');
                    while (!re.test(pos))
                    {
                        pos = prompt("Einfügen an Position (0.." + input.value.length + "):", "0");
                    }
                    if (pos > input.value.length)
                    {
                        pos = input.value.length;
                    }
                    var insText = prompt("Bitte geben Sie den zu formatierenden Text ein:");
                    input.value = input.value.substr(0, pos) + aTag + insText + eTag + input.value.substr(pos);
                }
            }

            function insertsmilie(thesmile)
            {
                var input = document.forms['gbookForm'].elements['gbtext'];
                input.focus();
                // Internet Explorer
                if(typeof document.selection != 'undefined')
                {
                    var range = document.selection.createRange();
                    var insText = range.text;
                    range.text = " "+thesmile+" ";
                    range = document.selection.createRange();
                    range.move('character', 0);
                    range.select();
                }
                // Gecko Software
                else if (typeof input.selectionStart != 'undefined')
                {
                    var start = input.selectionStart;
                    var end = input.selectionEnd;
                    var insText = input.value.substring(start, end);
                    input.value = input.value.substr(0, start) + " "+thesmile+" " + input.value.substr(end);
                    var pos;
                    pos = start + (thesmile.length + 2);
                    input.selectionStart = pos;
                    input.selectionEnd = pos;
                }
                else
                {
                    var pos;
                    var re = new RegExp('^[0-9]{0,3}$');
                    while (!re.test(pos))
                    {
                        pos = prompt("Einfügen an Position (0.." + input.value.length + "):", "0");
                    }
                    if (pos > input.value.length)
                    {
                        pos = input.value.length;
                    }
                    var insText = prompt("Bitte geben Sie den zu formatierenden Text ein:");
                    input.value = input.value.substr(0, pos) + aTag + insText + eTag + input.value.substr(pos);
                }
            }

            <?php if($this->params->get('support_bbcode', false)) : ?>

            function DoPrompt(action)
            {
                var input = document.forms['gbookForm'].elements['gbtext'];
                input.focus();

                var start = input.selectionStart;
                var end = input.selectionEnd;
                var revisedMessage;
                var currentMessage = document.gbookForm.gbtext.value;

                <?php if($this->params->get('support_link', false)) : ?>

                if (action == "url")
                {
                    var thisURL = prompt("<?php echo JTEXT::_('COM_EASYBOOKRELOADED_ENTER_THE_URL_HERE'); ?>", "http://");
                    var thisTitle = prompt("<?php echo JTEXT::_('COM_EASYBOOKRELOADED_ENTER_THE_WEB_PAGE_TITLE'); ?>", "<?php echo JTEXT::_('COM_EASYBOOKRELOADED_WEB_PAGE_TITLE'); ?>");
                    if (thisURL != undefined && thisTitle != undefined)
                    {
                        if  (thisURL != "" && thisTitle != "")
                        {
                            var urlBBCode = "[URL="+thisURL+"]"+thisTitle+"[/URL]";
                            insertprompt(urlBBCode, input, start, end, revisedMessage, currentMessage);
                        }
                    }
                    return;
                }

                <?php endif; ?>
                <?php if($this->params->get('support_mail', true)) : ?>

                if (action == "email")
                {
                    var thisEmail = prompt("<?php echo JTEXT::_('COM_EASYBOOKRELOADED_ENTER_THE_EMAIL_ADDRESS'); ?>", "");
                    if (thisEmail != undefined)
                    {
                        if  (thisEmail != "")
                        {
                            var emailBBCode = "[EMAIL]"+thisEmail+"[/EMAIL]";
                            insertprompt(emailBBCode, input, start, end, revisedMessage, currentMessage);
                        }
                    }
                    return;
                }

                <?php endif; ?>

                if (action == "code")
                {
                    var thisLanguage = prompt("<?php echo JTEXT::_('COM_EASYBOOKRELOADED_WHICH_LANGUAGE'); ?>", "");
                    if (thisLanguage != undefined)
                    {
                        if  (thisLanguage != "")
                        {
                            var codeBBCode = "[CODE="+thisLanguage+"]\n\n[/CODE]";
                            insertprompt(codeBBCode, input, start, end, revisedMessage, currentMessage);
                        }
                    }
                    return;
                }
                if (action == "youtube")
                {
                    var thisYoutube = prompt("<?php echo JTEXT::_('COM_EASYBOOKRELOADED_YOUTUBE_VIDEO_ID'); ?>", "");
                    if (thisYoutube != undefined)
                    {
                        if  (thisYoutube != "")
                        {
                            var codeBBCode = "[YOUTUBE]"+thisYoutube+"[/YOUTUBE]";
                            insertprompt(codeBBCode, input, start, end, revisedMessage, currentMessage);
                        }
                    }
                    return;
                }

                <?php if($this->params->get('support_pic', false)) : ?>

                if (action == "image")
                {
                    var thisImage = prompt("<?php echo JTEXT::_('COM_EASYBOOKRELOADED_ENTER_THE_URL_OF_THE_PICTURE_YOU_WANT_TO_SHOW'); ?>", "http://");
                    if (thisImage != undefined)
                    {
                        if  (thisImage != "")
                        {
                            var imageBBCode = "[IMG]"+thisImage+"[/IMG]";
                            insertprompt(imageBBCode, input, start, end, revisedMessage, currentMessage);
                        }
                    }
                    return;
                }
                if (action == "image_link")
                {
                    var thisImage = prompt("<?php echo JTEXT::_('COM_EASYBOOKRELOADED_ENTER_THE_URL_OF_THE_PICTURE_YOU_WANT_TO_SHOW'); ?>", "http://");
                    var thisURL = prompt("<?php echo JTEXT::_('COM_EASYBOOKRELOADED_ENTER_THE_URL_HERE'); ?>", "http://");
                    if (thisImage != undefined && thisURL != undefined)
                    {
                        if  (thisImage != "" && thisURL != "")
                        {
                            var imageBBCode = "[IMGLINK="+thisURL+"]"+thisImage+"[/IMGLINK]";
                            insertprompt(imageBBCode, input, start, end, revisedMessage, currentMessage);
                        }
                    }
                    return;
                }
                <?php endif; ?>
            }
            <?php endif; ?>
        </script>
        <form name='gbookForm' action='<?php JRoute::_('index.php'); ?>' target='_top' method='post'>
            <input type='hidden' name='option' value='com_easybookreloaded' />
            <input type='hidden' name='task' value='save' />
            <input type='hidden' name='controller' value='entry' />
            <?php echo JHTML::_('form.token'); ?>

            <?php if($this->user->guest == 0 AND !_EASYBOOK_CANEDIT) : ?>
                <input type='hidden' name='gbname' value='<?php echo $this->entry->gbname; ?>' />
                <input type='hidden' name='gbmail' value='<?php echo $this->entry->gbmail; ?>' />
            <?php endif; ?>
            <?php if($this->entry->id) : ?>
                <input type='hidden' name='id' value='<?php echo $this->entry->id; ?>' />
            <?php endif; ?>
            <table style="padding-left: 50px;" cellpadding='0' cellspacing='4' border='0' >
                <?php if($this->params->get('enable_log', true)) : ?>
                    <tr style="display: none;">
                        <td width='130'><?php echo JTEXT::_('COM_EASYBOOKRELOADED_IP_ADDRESS'); ?><span class='small'>*</span></td>
                        <td><input type='text' name='gbip'  class='inputbox' value='<?php echo $this->entry->ip; ?>' disabled='disabled' /></td>
                    </tr>
                <?php endif; ?>
                <tr>
                    <td width='130'><label for='gbname'><?php echo JTEXT::_('COM_EASYBOOKRELOADED_NAME'); ?></label><span class='small'>*</span></td>
                    <?php if($this->user->guest == 1) : ?>
                        <td><input type='text' name='gbname' id='gbname'  style="width: 400px; height: 25px;" value='<?php echo $this->entry->gbname; ?>' /></td>
                    <?php elseif($this->user->guest == 0 AND !_EASYBOOK_CANEDIT) : ?>
                        <td><?php echo $this->entry->gbname; ?></td>
                    <?php elseif(_EASYBOOK_CANEDIT) : ?>
                        <td><input type='text' name='gbname' id='gbname'  style="width: 400px; height: 25px;" value='<?php echo $this->entry->gbname; ?>' /></td>
                    <?php endif; ?>
                </tr>
                <?php if($this->params->get('show_mail', true) OR $this->params->get('require_mail', true)) : ?>
                    <tr>
                        <td width='130'><label for='gbmail'><?php echo JTEXT::_('COM_EASYBOOKRELOADED_EMAIL'); ?></label>
                            <?php if($this->params->get('require_mail', true)) : ?>
                                <?php echo "<span class='small'>*</span>"; ?>
                            <?php endif; ?>
                        </td>
                        <?php if($this->user->guest == 1) : ?>
                            <td>
                                <input type='text' name='gbmail' id='gbmail'  style="width: 400px; height: 25px;" value='<?php echo $this->entry->gbmail; ?>' />
                            </td>
                        <?php elseif($this->user->guest == 0 AND !_EASYBOOK_CANEDIT) : ?>
                            <td>
                                <?php echo $this->entry->gbmail; ?>
                            </td>
                        <?php elseif(_EASYBOOK_CANEDIT) : ?>
                            <td>
                                <input type='text' name='gbmail' id='gbmail' style="width: 400px; height: 25px;" value='<?php echo $this->entry->gbmail; ?>' />
                            </td>
                        <?php endif; ?>
                    </tr>
                    <?php if(!$this->entry->id) : ?>
                        <tr style="display: none;">
                            <td width='130'>
                                <label for='gbmailshow'><?php echo JTEXT::_('COM_EASYBOOKRELOADED_SHOW_EMAIL_IN_PUBLIC'); ?></label>
                            </td>
                            <td>
                                <input type='checkbox' name='gbmailshow' id='gbmailshow' class='inputbox' value='1' />
                            </td>
                        </tr>
                    <?php endif; ?>
                <?php endif; ?>
               
                <?php if($this->params->get('show_loca', true)) : ?>
                    <tr>
                        <td width='130'>
                            <label for='gbloca'><?php echo JTEXT::_('COM_EASYBOOKRELOADED_LOCATION'); ?></label>
                        </td>
                        <td>
                            <input type='text' name='gbloca' id='gbloca'  style="width: 400px; height: 25px;" value='<?php echo $this->entry->gbloca; ?>' />
                        </td>
                    </tr>
                <?php endif; ?>
                
                    <input type='hidden' name='gbvote' value='0' />
             
                <?php if($this->params->get('show_title', true)) : ?>
                    <tr>
                        <td width='130'>
                            <label for='gbtitle'><?php echo JTEXT::_('COM_EASYBOOKRELOADED_TITLE'); ?></label>
                            <?php if($this->params->get('require_title', true)) : ?>
                                <span class='small'>*</span>
                            <?php endif; ?>
                        </td>
                        <td>
                            <input type='text' name='gbtitle' id='gbtitle'  class='inputbox' value='<?php echo $this->entry->gbtitle; ?>' />
                        </td>
                    </tr>
                <?php endif; ?>

                <tr>
                    <td width='130' valign='top'>
                        <label for='gbtext'><?php echo JTEXT::_('COM_EASYBOOKRELOADED_GUESTBOOK_ENTRY'); ?></label><span class='small'>*</span>
                        <br /><br />
                        </td>
                    <td valign='top'>
                        <textarea name='gbtext' id='gbtext' style="width: 400px; height: 125px;"  rows='15' cols='50'><?php echo $this->entry->gbtext; ?></textarea>
                    </td>
                </tr>
                <?php if($this->params->get('enable_spam', true) AND ($this->params->get('enable_spam_reg') OR $this->user->guest)) : ?>
                    <tr>
                        <td width='130'>
                            <label for='<?php echo $this->session->get('spamcheck_field_name', null, 'easybookreloaded'); ?>'><?php echo JText::_('COM_EASYBOOKRELOADED_SPAM'); ?></label><span class='small'>*</span>
                        </td>
                        <td>
                            <?php echo $this->session->get('spamcheck1', null, 'easybookreloaded').' '.$this->session->get('operator', null, 'easybookreloaded').' '.$this->session->get('spamcheck2', null, 'easybookreloaded'); ?> = <input type="text" name="<?php echo $this->session->get('spamcheck_field_name', null, 'easybookreloaded'); ?>" id="<?php echo $this->session->get('spamcheck_field_name', null, 'easybookreloaded'); ?>" size="3" value="" style="width: 100px;"/>
                        </td>
                    </tr>
                <?php endif; ?>
                <?php if($this->params->get('spamcheck_question') AND ($this->params->get('spamcheck_question_question') AND $this->params->get('spamcheck_question_answer')) AND ($this->params->get('enable_spam_reg') OR $this->user->guest)) : ?>
                    <tr>
                        <td width='130'>
                            <label for='<?php echo $this->session->get('spamcheck_question_field_name', null, 'easybookreloaded'); ?>'><?php echo $this->params->get('spamcheck_question_question', true); ?></label><span class='small'>*</span>
                        </td>
                        <td>
                            <input type='text' name='<?php echo $this->session->get('spamcheck_question_field_name', null, 'easybookreloaded'); ?>' id='<?php echo $this->session->get('spamcheck_question_field_name', null, 'easybookreloaded'); ?>'  class='inputbox' value='' />
                        </td>
                    </tr>
                <?php endif; ?>
           
 <tr>
                        <td width='130'> </td>
                        <td align="left">
<br/>
                <span><input type='submit' name='send' value='Отправить' class='button' /></span>
          </td>
                    </tr>
 </table>
        </form>
        <p class="easy_small_notice">
            <span>* <?php echo JTEXT::_('COM_EASYBOOKRELOADED_REQUIRED_FIELD'); ?></span>
        </p>

    </div>
</div>