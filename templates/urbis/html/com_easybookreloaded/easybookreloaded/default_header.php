<?php

defined('_JEXEC') or die('Restricted access');
?>
<table class="contentpaneopen">
<tbody>
<tr>
<td class="contentheading" width="100%">
<h1>Отзывы наших клиентов</h1>
</td>
</tr>
</tbody>
</table>
<div id="easybook">


    <div class="easy_entrylink">
        <?php

        if($this->params->get('show_introtext') == 1)
        {
            ?>
            <div class='easy_intro'>
             <p>В данном разделе Вы можете оставить отзыв о нашей работе.</br>
<b>Внимание, Ваш отзыв будет размещен только после проверки администратором.</b></p></br>
            </div>
        <?php
        }
        elseif($this->params->get('show_introtext') == 2)
        {
            ?>
            <div class='easy_intro'>
                <?php echo nl2br($this->params->get('introtext')); ?>
            </div>
<?php }

        if(_EASYBOOK_CANADD AND !$this->params->get('offline'))
        {
?>
<div>
<a class="product-details" href="<?php echo JRoute::_('index.php?option=com_easybookreloaded&controller=entry&task=add'); ?>">
<b>Оставить отзыв</b>
</a>
</div>
<?php
        }

 ?>
        <br />