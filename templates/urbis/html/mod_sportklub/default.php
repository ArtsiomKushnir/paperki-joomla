<?php
/**
 * @version		$Id: item.php 1251 2011-10-19 17:50:13Z joomlaworks $
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.gr
 * @copyright	Copyright (c) 2006 - 2011 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

$db = &JFactory::getDBO();
$query = "SELECT #__k2_attachments.title as filetit,  #__k2_attachments.filename AS filename, #__k2_items.*  FROM #__k2_attachments LEFT JOIN #__k2_items ON #__k2_items.id = #__k2_attachments.itemID";
$db->SetQuery($query);
$downfiles = $db->loadObjectList();
if(!empty($downfiles)) {
?>
<div class="alldfiles">
<div class="title">
<?php echo $module->title;?>
</div>
<?php
foreach ($downfiles as $attachment)
{
$fsize= round((filesize('media/k2/attachments/'.$attachment->filename)/1024/1024), 2);
$fsize= number_format($fsize, 2, ', ', ' ');
if($attachment->published == 1 && $attachment->trashed == 0){
$mimetype = substr($attachment->filename, -3);
if($mimetype == 'ocx') $fname = 'doc';
elseif($mimetype == 'lsx') $fname  = 'xls';
elseif($mimetype == 'ptx') $fname  = 'ppt';
else $fname  = $mimetype;
?>
		    <div class="attach" style="background: url(templates/urbis/images/<?php echo $fname;?>.png) left top no-repeat;">
			  <p>  <a class="<?php echo $mimetype; ?>" title="<?php echo $attachment->filetit; ?>" href="<?php echo 'media/k2/attachments/'.$attachment->filename; ?>" target="_blank"><?php echo $attachment->title; ?></a><span class="small"> (<?php echo $mimetype; ?>, <?php echo $fsize; ?> MB)</span></p>
			    
		    </div>
<?php
}
}
?>
</div>
<?php
}
?>