<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_footer
 * @copyright	Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
$document = &JFactory::getDocument();

?>

<div class="NivoSzakiSlider" style="text-align: center;">
<div class="nivoSlider" style="position: relative; width: 550px;">
<img src="templates/urbis/images/slides/slide1.jpg" >
<img src="templates/urbis/images/slides/slide2.jpg" style="display: none;">
<img src="templates/urbis/images/slides/slide3.jpg" style="display: none;">
</div></div>