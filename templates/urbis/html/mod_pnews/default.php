<script type="text/javascript">
	$(document).ready(function(){
		$("#featured").tabs({fx:{opacity: "toggle"}}).tabs("rotate", 5000, true);
	});
</script>

<div id="featured" >

<ul class="ui-tabs-nav">

<?php 
if($params->get('header_text1') && $params->get('slide1')){
echo '<li class="ui-tabs-nav-item ui-tabs-selected" id="nav-fragment-1"><a href="#fragment-1"><span>'.$params->get('header_text1').'</span></a></li>';
}

if($params->get('header_text2') && $params->get('slide2')){
echo '<li class="ui-tabs-nav-item" id="nav-fragment-2"><a href="#fragment-2"><span>'.$params->get('header_text2').'</span></a></li>';
}

if($params->get('header_text3') && $params->get('slide3')){
echo '<li class="ui-tabs-nav-item" id="nav-fragment-3"><a href="#fragment-3"><span>'.$params->get('header_text3').'</span></a></li>';
}

if($params->get('header_text4') && $params->get('slide4')){
echo '<li class="ui-tabs-nav-item" id="nav-fragment-4"><a href="#fragment-4"><span>'.$params->get('header_text4').'</span></a></li>';
}
?>

</ul>

<?php 
if($params->get('header_text1') && $params->get('slide1')){
echo '<div id="fragment-1" class="ui-tabs-panel" style="">';
echo '<a href="'.$params->get('link1').'"><img src="'.$params->get('slide1').'" alt="'.$params->get('header_text1').'" /></a>';
if($params->get('link_text1')){
echo '<div class="info"><p>'.$params->get('link_text1').'</p></div>';
}
echo '</div>';
}


if($params->get('header_text2') && $params->get('slide2')){
echo '<div id="fragment-2" class="ui-tabs-panel ui-tabs-hide" style="">';
echo '<a href="'.$params->get('link2').'"><img src="'.$params->get('slide2').'" alt="'.$params->get('header_text2').'" /></a>';
if($params->get('link_text2')){
echo '<div class="info"><p>'.$params->get('link_text2').'</p></div>';
}
echo '</div>';
}

if($params->get('header_text3') && $params->get('slide3')){
echo '<div id="fragment-3" class="ui-tabs-panel ui-tabs-hide" style="">';
echo '<a href="'.$params->get('link3').'"><img src="'.$params->get('slide3').'" alt="'.$params->get('header_text3').'" /></a>';
if($params->get('link_text3')){
echo '<div class="info"><p>'.$params->get('link_text3').'</p></div>';
}
echo '</div>';
}

if($params->get('header_text4') && $params->get('slide4')){
echo '<div id="fragment-4" class="ui-tabs-panel ui-tabs-hide" style="">';
echo '<a href="'.$params->get('link4').'"><img src="'.$params->get('slide4').'" alt="'.$params->get('header_text4').'" /></a>';
if($params->get('link_text4')){
echo '<div class="info"><p>'.$params->get('link_text4').'</p></div>';
}
echo '</div>';
}

?>


		
		

		</div>