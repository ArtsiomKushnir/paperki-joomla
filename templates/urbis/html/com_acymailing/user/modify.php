<?php
/**
 * @package	AcyMailing for Joomla!
 * @version	4.5.1
 * @author	acyba.com
 * @copyright	(C) 2009-2013 ACYBA S.A.R.L. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><div id="acymodifyform">
<?php if($this->values->show_page_heading){ ?>
<h1 class="contentheading<?php echo $this->values->suffix; ?>"><?php echo $this->values->page_heading; ?></h1>
<?php } ?>
<?php if(!empty($this->introtext)){ echo '<span class="acymailing_introtext">'.$this->introtext.'</span>'; } ?>
<form action="<?php echo JRoute::_( 'index.php' );?>" method="post" name="adminForm" id="adminForm" <?php if(!empty($this->fieldsClass->formoption)) echo $this->fieldsClass->formoption; ?> >
	
	<?php if($this->displayLists){?>
	<fieldset class="adminform acy_subscription_list" style="padding: 1px 10px;">
		<legend><span>Подписка на e-mail рассылку</span></legend>
		<table cellspacing="1" align="center" width="100%" id="acyusersubscription">
	
			<tbody>
				<?php
				$k = 0;
				foreach($this->subscription as $row){
					if(empty($row->published) OR !$row->visible) continue;
					$listClass = 'acy_list_status_' . str_replace('-','m',(int) @$row->status);
					?>
				<tr class="<?php echo "row$k $listClass"; ?>">
					<td align="center" nowrap="nowrap" valign="top" class="acystatus"><br/>
						<span><?php echo $this->status->display("data[listsub][".$row->listid."][status]",@$row->status); ?></span>
					</td>
					<td valign="top"><br/>
						<div class="list_name"><?php echo $row->name ?></div>
						<div class="list_description"><?php echo $row->description ?></div>
<br/>
					</td>
				</tr>
				<?php
					$k = 1 - $k;
				} ?>
			</tbody>
		</table>
	<p class="acymodifybutton">
		<input class="button btn btn-primary" type="submit" onclick="return checkChangeForm();" value="<?php echo empty($this->subscriber->subid) ? $this->escape(JText::_('SUBSCRIBE')) :  $this->escape('Сохранить изменения рассылки')?>"/>
	</p>
	</fieldset>
	<?php } ?>
	<br/>
	<input type="hidden" name="option" value="<?php echo ACYMAILING_COMPONENT; ?>" />
	<input type="hidden" name="task" value="savechanges" />
	<input type="hidden" name="ctrl" value="user" />
<input type="hidden" name="redirect" value="<?php echo urlencode('index.php?option=com_virtuemart&view=user&layout=edit'); ?>"/>
	<?php echo JHTML::_( 'form.token' ); ?>
	<input type="hidden" name="subid" value="<?php echo $this->subscriber->subid; ?>" />
	<?php if(JRequest::getCmd('tmpl') == 'component'){ ?><input type="hidden" name="tmpl" value="component" /><?php } ?>
	<input type="hidden" name="key" value="<?php echo $this->subscriber->key; ?>" />

</form>
<?php if(!empty($this->finaltext)){ echo '<span class="acymailing_finaltext">'.$this->finaltext.'</span>'; } ?>
</div>
