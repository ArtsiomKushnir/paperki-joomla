<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_users
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 * @since		1.5
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidation');
?>
<div class="reset<?php echo $this->pageclass_sfx?>">
<div class="vestheading">
<h1>Восстановление пароля</h1>
</div>

	<form id="user-registration" action="<?php echo JRoute::_('index.php?option=com_users&task=reset.request'); ?>" method="post" class="form-validate">

		<?php foreach ($this->form->getFieldsets() as $fieldset): ?>
		<p><?php echo JText::_($fieldset->label); ?></p>		<fieldset style="padding: 10px; margin: 15px 0;">
			
			<?php foreach ($this->form->getFieldset($fieldset->name) as $name => $field): ?>
				<?php echo $field->label; ?>
				<?php echo $field->input; ?>
			<?php endforeach; ?>
			
		</fieldset>
		<?php endforeach; ?>

		<div>
			<input type="submit" class="validate vm-button-correct" style="padding: 1px 10px 0px; height: 20px;" value="<?php echo JText::_('JSUBMIT'); ?>" />
			<?php echo JHtml::_('form.token'); ?>
		</div>
	</form>
</div>
