<?php
// no direct access
defined('_JEXEC') or die('Restricted access');

if (!class_exists('VirtueMartCart')) {require(JPATH_VM_SITE . DS . 'helpers' . DS . 'cart.php');}
$carter = VirtueMartCart::getCart();

if($carter->pricesUnformatted['salesPrice'] > 100){
$req = " #__virtuemart_product_categories.virtuemart_category_id = '1350'";
}
if($carter->pricesUnformatted['salesPrice'] > 200){
$req.= " OR #__virtuemart_product_categories.virtuemart_category_id = '1351'";
}
if($carter->pricesUnformatted['salesPrice'] > 400){
$req.= " OR #__virtuemart_product_categories.virtuemart_category_id = '1352'";
}
if($carter->pricesUnformatted['salesPrice'] > 650){
$req.= " OR #__virtuemart_product_categories.virtuemart_category_id = '1353'";
}
if($carter->pricesUnformatted['salesPrice'] > 950){
$req.= " OR #__virtuemart_product_categories.virtuemart_category_id = '1354'";
}
if($carter->pricesUnformatted['salesPrice'] > 1300){
$req.= " OR #__virtuemart_product_categories.virtuemart_category_id = '1355'";
}
if($carter->pricesUnformatted['salesPrice'] > 1650){
$req.= " OR #__virtuemart_product_categories.virtuemart_category_id = '1356'";
}
if($carter->pricesUnformatted['salesPrice'] > 2000){
$req.= " OR #__virtuemart_product_categories.virtuemart_category_id = '1357'";
}



////////////////////////////////////////////////////////////
$db =& JFactory::getDBO();
$query = "SELECT #__virtuemart_products.virtuemart_product_id FROM #__virtuemart_products LEFT JOIN #__virtuemart_product_categories ON #__virtuemart_products.virtuemart_product_id = #__virtuemart_product_categories.virtuemart_product_id WHERE ".$req;
$db->setQuery($query);
$ids = $db->loadResultArray();
shuffle($ids); 
$productModel = VmModel::getModel('Product');
$producters = $productModel->getProducts ($ids, true, true, false, false);
$categoryModel = VmModel::getModel('category');


$products_per_row = 3 ;
$verticalseparator = " vertical-separator";
$cellwidth = ' width'.floor ( 100 / $products_per_row );
?>
<script type="text/javascript" src="templates/urbis/js/jquery.jcarousel.min.js" ></script>

<script type="text/javascript">

jQuery(document).ready(function() {
    jQuery('#mycarouselpod').jcarousel({
auto: 0,
wrap: 'circular',
scroll: 1
    });
});

</script>

<div class="akcii" id="supergifter" >
<div id="closepod" title="Закрыть панель подарков">
</div>

<ul id="mycarouselpod" class="jcarousel-skin-simple">
<?php 
$cont = 0;
foreach ($producters as $product) {
$cont++;
if($cont <20){
 ?>
<li caption="<?php echo $product->virtuemart_product_id; ?>">	
<?php


$product_sku = str_replace("m","", $product->product_sku);
////image finder
$icode = '00000000';
$len = strlen($product_sku);
if ($len == '1') {
$icode = '0000000'.$product_sku;
}else if ($len == '2') {$icode = '000000'.$product_sku;}
else if ($len == '3') {$icode = '00000'.$product_sku;}
else if ($len == '4') {$icode = '0000'.$product_sku;}
else if ($len == '5') {$icode = '000'.$product_sku;}
else if ($len == '6') {$icode = '00'.$product_sku;}
else if ($len == '7') {$icode = '0'.$product_sku;}
else if ($len == '8') {$icode =  $product_sku;}
$full_image = '';
$pas = 'images/catalog/'.$icode.'.jpg';
$pas2 = 'images/catalog/'.$icode.'.png';
$pas3 = 'images/catalog/'.$icode.'.gif';
$pas4 = 'images/catalog/'.$icode.'.jpeg';

if (file_exists($pas)) {
$full_image = $pas;
}
else if (file_exists($pas2)) {$full_image = $pas2;}
else if (file_exists($pas3)) {$full_image = $pas3;}
else if (file_exists($pas4)) {$full_image = $pas4;}
else{$full_image = 'images/catalog/noimage.jpg';}

$full_image2 = '';
$pas = 'images/catalog/'.$icode.'-1.jpg';
$pas2 = 'images/catalog/'.$icode.'-1.png';
$pas3 = 'images/catalog/'.$icode.'-1.gif';
$pas4 = 'images/catalog/'.$icode.'-1.jpeg';

if (file_exists($pas)) {
$full_image2 = $pas;
}
else if (file_exists($pas2)) {$full_image2 = $pas2;}
else if (file_exists($pas3)) {$full_image2 = $pas3;}
else if (file_exists($pas4)) {$full_image2 = $pas4;}
//////end image finder
?>

<div class="product-main center floatleft width25">
<a class="getthisprodtogift">
<div class="product-mainimage">
<div class="imagecontainer">
<div class="prodimage">
<img src="<?php echo $full_image; ?>"   alt="<?php echo $product->product_name ?>" class="lazy" />
</div>
</div></div>
<div class="clear"></div>
<h3>
<?php 
echo '<strong>'.$product->product_name.'</strong><br/>'; 
?>
</h3>
</a>
</div>





</li>
<?php	}

} ?>
</ul>
<div class="clear"></div>
</div>

<script type="text/javascript">
jQuery.noConflict();
jQuery(document).ready(function(){
jQuery('#supergifter').hide();
var tempcapt = 0;
var temppname = 0;
var himg = '';
jQuery('a.getthisprodtogift').click(function() {
tempcapt = jQuery(this).parents('li').attr('caption');
temppname = jQuery(this).children('h3').html();
jQuery('#selectgifttext').html("Выбранный вами подарок: <br/>");
jQuery('#chosegifttext').text("Выбрать другой подарок " + jQuery('#selectedgift').val());
jQuery('#selectedgifttext').html("<br/>" + temppname + "<br/><br/><br/>");
himg = jQuery(this).find('img').attr('src') ;
jQuery('#selectedimage').attr('src',himg );
jQuery('#selectedimagebig').attr('href',himg );
jQuery('#selectedimagebig').show();
jQuery('#supergifter').fadeOut(150);
jQuery('#requestres').css('margin-left','10px');
jQuery('#requestloading').show();
jQuery('#requestrestext').fadeIn();
jQuery('#requestrestext').html('Сохранение...');
jQuery.ajax({
url: "index.php?option=com_virtuemart&view=cart&selectedgiftprod="+tempcapt,
			cache: false,
			success: function(html){
if(html.length > 0) {
jQuery('#requestloading').hide();
jQuery('#requestrestext').html('Ваш подарок сохранен!');
jQuery('#requestrestext').fadeOut(3000);

}
}
});


});


jQuery('a#chosegifttext').click(function() {
jQuery('#supergifter').css('margin-left','-100px')
jQuery('#supergifter').fadeIn(300);
});

jQuery('#closepod').click(function() {
jQuery('#supergifter').fadeOut(150);

});


});
</script>
