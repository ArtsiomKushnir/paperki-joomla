<?php // no direct access
defined('_JEXEC') or die('Restricted access');

$cache = JFactory::getCache('com_virtuemart','callback');

//JHTML::stylesheet ( 'menucss.css', 'modules/mod_virtuemart_category/css/', false );
?>
</div>
<div class="moduletable">
<ul class="menu<?php echo $class_sfx ?>" >
<?php 
foreach ($categories as $category) {
$html = '';
$show = 0;
if($category->virtuemart_category_id != '1349' ) {
		 $active_menu = 'class="';
		$caturl = JRoute::_('index.php?option=com_virtuemart&view=category&virtuemart_category_id='.$category->virtuemart_category_id);
		$cattext = $category->category_name;
		//if ($active_category_id == $category->virtuemart_category_id) $active_menu = 'class="active"';
		if (in_array( $category->virtuemart_category_id, $parentCategories)) $active_menu .= 'active';
if ($category->childs ) {$active_menu .= ' parent"';} else $active_menu .= '"';

$html .= '<li '.$active_menu.'>';
if ($category->childs ) {
$html .= '<div class="forbg devider"><span class="iconbg">'.$cattext.'</span></div>';
$html .= '<ul class="menu'.$class_sfx.'">';

foreach ($category->childs as $child) {
if($categoryModel->countProducts($child->virtuemart_category_id) > 0){
$show = 1; 
$caturl = JRoute::_('index.php?option=com_virtuemart&view=category&virtuemart_category_id='.$child->virtuemart_category_id);
$cattext = $child->category_name;
if (in_array( $child->virtuemart_category_id, $parentCategories)) $active_child = ' class="active"'; 
else $active_child = '';
$html .= '<li'.$active_child.'>';
$html .= '<div >'.JHTML::link($caturl, $cattext).'</div></li>';
} 
}
$html .= '</ul>';

} else $html .= JHTML::link($caturl, $cattext);
$html .= '</li>';
if($show == 1) echo $html;
}
}
 ?>
</ul>
