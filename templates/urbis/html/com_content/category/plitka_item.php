<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_content
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

// Create a shortcut for params.
$params = &$this->item->params;
$images = json_decode($this->item->images);
$canEdit	= $this->item->params->get('access-edit');
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.framework');
include_once("templates/urbis/html/simple_html_dom.php");

?>

<?php if ($this->item->state == 0) : ?>
<div class="system-unpublished">
<?php endif; ?>




<?php if (!$params->get('show_intro')) : ?>
	<?php echo $this->item->event->afterDisplayTitle; ?>
<?php endif; ?>

<?php echo $this->item->event->beforeDisplayContent; ?>

<?php // to do not that elegant would be nice to group the params ?>



<?php if ($params->get('show_publish_date')) : ?>
		<dd class="published">
		<?php echo JText::sprintf('COM_CONTENT_PUBLISHED_DATE_ON', JHtml::_('date', $this->item->publish_up, JText::_('DATE_FORMAT_LC2'))); ?>
		</dd>
<?php endif; ?>


<?php
$this->item->text = $this->item->introtext . $this->item->fulltext;
$html = new simple_html_dom_not_conflict;
$html->load($this->item->text, true);
$i =0;
        foreach($html->find('img') as $element)  {
$i++;
if($i == '1') {
            $time=array();
            $time["src"]=$element->src;
            $time["height"]=$element->height;
            $time["width"]=$element->width;
            $time["alt"]=$element->alt;
            $time["title"]=$element->title;
       }
        }



?>

<div class="catItemImage floatleft">
<a class="thumbnail" href="<?php echo JRoute::_(ContentHelperRoute::getArticleRoute($this->item->slug, $this->item->catid)); ?>">
<img src="<?php echo $time['src'];?>"  style="width:165px; height:155px;"  />
</a>
</div>


<?php 
$this->item->introtext=preg_replace("/<img (.*?)>/", '', $this->item->introtext, 1);
echo $this->item->introtext;
$suffix = '...';
$maxlength = 550;
$string = $this->item->introtext;	
 /* original code here */
			if(strlen($string)<=$maxlength) {$temp = $string;}
else {
			$string = substr($string,0,$maxlength);
			$index = strrpos($string, ' ');
			if($index===FALSE) {
				$temp = $string;
			} else {
				$temp = substr($string,0,$index).$suffix;
			}
		}
//echo $temp; ?>



<?php 
	if ($params->get('access-view')) :
		$link = JRoute::_(ContentHelperRoute::getArticleRoute($this->item->slug, $this->item->catid));
	else :
		$menu = JFactory::getApplication()->getMenu();
		$active = $menu->getActive();
		$itemId = $active->id;
		$link1 = JRoute::_('index.php?option=com_users&view=login&Itemid=' . $itemId);
		$returnURL = JRoute::_(ContentHelperRoute::getArticleRoute($this->item->slug, $this->item->catid));
		$link = new JURI($link1);
		$link->setVar('return', base64_encode($returnURL));
	endif;
?>
		<div class="readmore">
<br/>
				<a href="<?php echo $link; ?>" class="vm-button-correct">
					<?php if (!$params->get('access-view')) :
						echo JText::_('COM_CONTENT_REGISTER_TO_READ_MORE');
					elseif ($readmore = $this->item->alternative_readmore) :
						echo $readmore;
						if ($params->get('show_readmore_title', 0) != 0) :
						    echo JHtml::_('string.truncate', ($this->item->title), $params->get('readmore_limit'));
						endif;
					elseif ($params->get('show_readmore_title', 0) == 0) :
						echo JText::sprintf('COM_CONTENT_READ_MORE_TITLE');
					else :
						echo JText::_('COM_CONTENT_READ_MORE');
						echo JHtml::_('string.truncate', ($this->item->title), $params->get('readmore_limit'));
					endif; ?></a>
		</div>


<?php if ($this->item->state == 0) : ?>
</div>
<?php endif; ?>
<div style="clear: both;"> <br/></div>
<div class="item-separator"></div>
<?php echo $this->item->event->afterDisplayContent; ?>
