<?php // no direct access
defined ('_JEXEC') or die('Restricted access');
$col = 1;
$pwidth = ' width33';
if ($products_per_row > 1) {
	$float = "floatleft";
} else {
	$float = "center";
}
?>

<script type="text/javascript">

jQuery(document).ready(function() {
    jQuery('#mycarouselurb').jcarousel();
});

</script>

<ul id="mycarouselurb" class="jcarousel-skin-ie7">

			<?php foreach ($products as $product) { 
		
?>
<li>
					<?php
echo '<a href="'.JRoute::_ ( 'index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $product->virtuemart_product_id . '&virtuemart_category_id=' . $product->virtuemart_category_id ).'">';
echo '<img src="'.$product->images[0]->file_url.'" style="width: 225px; height: 335px;" /></a>';
					$url = JRoute::_ ('index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $product->virtuemart_product_id . '&virtuemart_category_id=' .$product->virtuemart_category_id); ?>
<a href="<?php echo $url ?>"><div class="tokart"></div> </a>
<div class="tit">
<h3><a href="<?php echo $url ?>"><?php echo $product->product_name ?></a></h3> 
</div>       
<?php 

					if ($show_price) {
						if (!empty($product->prices['salesPrice'])) {
							echo $currency->createPriceDiv ('salesPrice', '', $product->prices, FALSE, FALSE, 1.0, TRUE);
						}
					}

		} ?>
		</ul>
	
