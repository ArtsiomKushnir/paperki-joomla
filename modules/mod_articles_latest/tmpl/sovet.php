<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_articles_latest
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
?>
<?php foreach ($list as $item) :  
?>
<div class="sovetone">
<div class="sovdate">
<?php echo date('d.m.Y', strtotime($item->created)); ?>
</div>
	<div>
		<b><a href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a></b>
</br>
<?php
$sub = mb_substr(strip_tags($item->introtext),0,124,'utf-8');
 echo $sub; ?>...
</div>
<div class="readm">
<a class="readmore" href="<?php echo $item->link; ?>">подробнее...</a>
</div>
</div>
<?php endforeach; ?>

