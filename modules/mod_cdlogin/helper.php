<?php
/**
 * Core Design Login module for Joomla! 2.5
 * @author		Daniel Rataj, <info@greatjoomla.com>
 * @package		Joomla 
 * @subpackage	Content
 * @category	Module
 * @version		2.5.x.2.0.4
 * @copyright	Copyright (C) 2007 - 2012 Great Joomla!, http://www.greatjoomla.com
 * @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL 3
 * 
 * This file is part of Great Joomla! extension.   
 * This extension is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This extension is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// no direct access
defined('_JEXEC') or die;

class modCdLoginHelper
{
	static function getReturnURL($params, $type)
	{
		$app	= JFactory::getApplication();
		$router = $app->getRouter();
		$url = null;
		if ($itemid =  $params->get($type))
		{
			$db		= JFactory::getDbo();
			$query	= $db->getQuery(true);

			$query->select($db->nameQuote('link'));
			$query->from($db->nameQuote('#__menu'));
			$query->where($db->nameQuote('published') . '=1');
			$query->where($db->nameQuote('id') . '=' . $db->quote($itemid));

			$db->setQuery($query);
			if ($link = $db->loadResult()) {
				if ($router->getMode() == JROUTER_MODE_SEF) {
					$url = 'index.php?Itemid='.$itemid;
				}
				else {
					$url = $link.'&Itemid='.$itemid;
				}
			}
		}
		if (!$url)
		{
			// stay on the same page
			$uri = clone JFactory::getURI();
			$vars = $router->parse($uri);
			unset($vars['lang']);
			if ($router->getMode() == JROUTER_MODE_SEF)
			{
				if (isset($vars['Itemid']))
				{
					$itemid = $vars['Itemid'];
					$menu = $app->getMenu();
					$item = $menu->getItem($itemid);
					unset($vars['Itemid']);
					if ($vars == $item->query) {
						$url = 'index.php?Itemid='.$itemid;
					}
					else {
						$url = 'index.php?'.JURI::buildQuery($vars).'&Itemid='.$itemid;
					}
				}
				else
				{
					$url = 'index.php?'.JURI::buildQuery($vars);
				}
			}
			else
			{
				$url = 'index.php?'.JURI::buildQuery($vars);
			}
		}

		return base64_encode($url);
	}
	
	static function getType()
	{
		$user = JFactory::getUser();
		return (!$user->get('guest')) ? 'logout' : 'login';
	}
	
	static function loadScripts($name)
	{
		$document = JFactory::getDocument();
		$live_path = JURI::base(true) . '/'; // define live site

		// add CSS stylesheet
		$document->addStyleSheet($live_path . "modules/$name/tmpl/css/$name.css", "text/css");
	}

	static function setForgotUsernameLink($define_links, $custom_link_forgot_username)
	{
		switch ($define_links)
		{
			case '0':
			default:
				$forgot_username_link = JRoute::_('index.php?option=com_users&view=remind');
				break;
			case '1':
				$forgot_username_link = JRoute::_($custom_link_forgot_username);
				break;
		}
		return $forgot_username_link;
	}

	static function setForgotPasswordLink($define_links, $custom_link_forgot_password)
	{
		switch ($define_links)
		{
			case '0':
			default:
				$forgot_password_link = JRoute::_('index.php?option=com_users&view=reset');
				break;
			case '1':
				$forgot_password_link = JRoute::_($custom_link_forgot_password);
				break;
		}
		return $forgot_password_link;
	}

	static function setRegisterLink($define_links, $custom_link_register)
	{
		switch ($define_links)
		{
			case '0':
			default:
				$register_link = JRoute::_('index.php?option=com_users&view=registration');
				break;
			case '1':
				$register_link = JRoute::_($custom_link_register);
				break;
		}
		return $register_link;
	}

	static function getFormName($form_name, $greeting)
	{
		$user = JFactory::getUser();
		if ($form_name)
		{
			$form_name = $user->get('name');
		} else
		{
			$form_name = $user->get('username');
		}

		if ($greeting) {
			$form_name = JText::sprintf('MOD_CDLOGIN_HINAME', $form_name);
		} else {
			$form_name =  JText::_('MOD_CDLOGIN_HI_LOGOUT');
		}
		return $form_name;
	}

}
?>
