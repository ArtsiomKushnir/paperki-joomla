<?php defined('_JEXEC') or die;?>


<?php
if (!class_exists( 'VmConfig' )) require(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_virtuemart'.DS.'helpers'.DS.'config.php');

vmJsApi::jPrice();
	// preventing 2 x load javascript
	$version = new JVersion();
	static $textinputjs;
	if ($textinputjs) return true;
	$textinputjs = true ;
	$document = JFactory::getDocument();


$document->addScriptDeclaration('
		// set this if it doesnt come from vm! 
		if (typeof vmCartText == "undefined") vmCartText = "'. JText::_('CATPRODUCT_WAS_ADDED_TO_YOUR_CART') .'" ;
		
		jQuery(document).ready(function() {
			if (typeof emptyQuantity == "function") { 
				emptyQuantity();
			}
			jQuery(".cell_customfields input").click(function () {
				row = jQuery(this).parents(".row_article").attr("id");
				row = row.replace("row_article_","");
				getPrice(row);
			});
			jQuery(".cell_customfields select").change(function () {
				row = jQuery(this).parents(".row_article").attr("id");
				row = row.replace("row_article_","");
				getPrice(row);
			});
			jQuery(".cell_parent_customfields input").click(function () {
				jQuery(".row_article").each(function() { getPrice(jQuery(this).attr("id").replace("row_article_","")); });
			});
			jQuery(".cell_parent_customfields select").change(function () {
				jQuery(".row_article").each(function() { getPrice(jQuery(this).attr("id").replace("row_article_","")); });
			});

		});
		var symbol_position = 1;
		var symbol_space = " ";
		var thousandssep = " ";
		var decimalsymbol = " ,";
		var decimalplaces = "0";
		var updateprice = 0;
		var noproducterror = "Товар не был выбран!";
		var noquantityerror = "Пожалуйста, установите количество!";
		var addparentoriginal = 0;
		var originaladdtocartareclass = ".productdetails-view .addtocart-area";
		var checkstock = "0";
		');


	$document->addScriptDeclaration('
	function updateSumPrice(art_id) {
		var uom = jQuery("#product_weight_uom_"+art_id).val();
		var unit = "p.";
		quantity = getQuantity(art_id); 
		sum_field(art_id,quantity,"product_weight_","sum_weight_");
		sum_field(art_id,quantity,"basePrice_","sum_basePrice_");
		sum_field(art_id,quantity,"basePriceWithTax_","sum_basePriceWithTax_");
		sum_field(art_id,quantity,"taxAmount_","sum_taxAmount_");
		sum_field(art_id,quantity,"discountAmount_","sum_discountAmount_");
		sum_field(art_id,quantity,"priceWithoutTax_","sum_priceWithoutTax_");
		sum_field(art_id,quantity,"salesPrice_","sum_salesPrice_");
	}
	');	
	
	$document->addScriptDeclaration('function removeNoQ (text) {
 return text.replace("Пожалуйста, введите количество этого элемента.","Товар был добавлен в корзину.");
	}');
	if ($version->RELEASE <> '1.5') {
		$document->addScript(JURI::root(true). "/plugins/vmcustom/catproduct/catproduct/js/javascript.js");
		$document->addStyleSheet(JURI::root(true). "/plugins/vmcustom/catproduct/catproduct/css/catproduct.css");
	}
	else {
		$document->addScript(JURI::root(true). "/plugins/vmcustom/catproduct/js/javascript.js");
		$document->addStyleSheet(JURI::root(true). "/plugins/vmcustom/catproduct/css/catproduct.css");
	}

 ?>



<style>
#facebox .popup h4{
display: none !important;
}
</style>

<?php for($t=1; $t<10; $t++){
$gtit = '';
$gp = '';
$gop = '';
$gtit = $params->get('gruppa'.$t);
$gop = $params->get('gruppa'.$t.'o');
$gp = $params->get('gruppa'.$t.'p');
if(!empty($gtit) && !empty($gp)){

?>		
<div class="klass">
<h3><?php echo $gtit;?></h3>
<div class="sdesc">
<?php echo $gop;?>
</div>
<div class="clear"></br></div>
<div class="table">
<fieldset>


<form  ax:nowrap="1" action="index.php" method="post" name="catproduct_form_<?php echo $t;?>" id="catproduct_form" onsubmit="handleToCart(<?php echo $t;?>); return false;">
<table class="cart-summary width100 product-table catproducttable">
			<tr><th width="40px">Фото</th>
			<th align="center" width="70px">Код</th>
			<th align="left" >Название</th>
			<th align="center" width="130px">Цена с НДС</th>
</tr>





<?php 
$pprods = $gp;

$pkodes = explode(',', $pprods);
$products = array();


	if (!class_exists ('CurrencyDisplay')) {
		require(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_virtuemart' . DS . 'helpers' . DS . 'currencydisplay.php');
	}
	if (!class_exists ('VirtueMartModelProduct')) {
		JLoader::import ('product', JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_virtuemart' . DS . 'models');
	}
VmConfig::loadConfig();
VmConfig::loadJLang('mod_virtuemart_product', true);
$currency = CurrencyDisplay::getInstance( );
$catModel = VmModel::getModel('Category');
$product_model = VmModel::getModel('product');
$db =JFactory::getDBO();
foreach ($pkodes as $pkode) {
$q = 'SELECT `virtuemart_product_id` FROM `#__virtuemart_products` WHERE `published`=1
AND `product_sku`= "' . $pkode . '"';
$db->setQuery ($q);
$prodids = $db->loadObject();
$product = $product_model->getProduct($prodids->virtuemart_product_id,TRUE,TRUE,TRUE, 1);
$products[$prodids->virtuemart_product_id] = $product;
}

$i = 1;
$nom = 0;
foreach ($products as $pkey => $prow) {
$nom++;
?>

<tr valign="top" id="row_article_2" class="row_article sectiontableentry<?php echo $i ?>">
	
<td class="product-tableimage"  width="30px">
<input class="product_id_<?php echo $t;?>" name="product_id" id="product_id_<?php echo $nom ?>" value="<?php echo $prow->virtuemart_product_id;?>" type="hidden">
<input class="product_name" name="product_name" id="product_name_<?php echo $nom ?>" value="<?php echo $prow->product_name;?>" type="hidden">
<input type="hidden" class="group_id" name="group_id" id="group_id_<?php echo $nom ?>" value="1" >
<input type="hidden" class="def_group_qty" name="def_group_qty" id="def_group_qty_<?php echo $nom ?>" value="1" >
<input class="quantity-input js-recalculate" size="2" id="quantity_<?php echo $nom ?>" name="quantity[]" value="1" type="hidden" onblur='changeQuantity(2, "input", 0, 0, 0)'>
<?php 
$product_sku = $prow->product_sku;
$icode = '00000000';
$len = strlen($product_sku);
if ($len == '1') {
$icode = '0000000'.$product_sku;
}else if ($len == '2') {$icode = '000000'.$product_sku;}
else if ($len == '3') {$icode = '00000'.$product_sku;}
else if ($len == '4') {$icode = '0000'.$product_sku;}
else if ($len == '5') {$icode = '000'.$product_sku;}
else if ($len == '6') {$icode = '00'.$product_sku;}
else if ($len == '7') {$icode = '0'.$product_sku;}
else if ($len == '8') {$icode =  $product_sku;}
$full_image = '';
$pas = 'images/catalog/'.$icode.'.jpg';
$pas2 = 'images/catalog/'.$icode.'.png';
$pas3 = 'images/catalog/'.$icode.'.gif';
$pas4 = 'images/catalog/'.$icode.'.jpeg';

if (file_exists($pas)) {
$full_image = $pas;
}
else if (file_exists($pas2)) {$full_image = $pas2;}
else if (file_exists($pas3)) {$full_image = $pas3;}
else if (file_exists($pas4)) {$full_image = $pas4;}
else{
$full_image = 'images/catalog/noimage.jpg';}

$full_image2 = '';
$pas = 'images/catalog/'.$icode.'-1.jpg';
$pas2 = 'images/catalog/'.$icode.'-1.png';
$pas3 = 'images/catalog/'.$icode.'-1.gif';
$pas4 = 'images/catalog/'.$icode.'-1.jpeg';

if (file_exists($pas)) {
$full_image2 = $pas;
}
else if (file_exists($pas2)) {$full_image2 = $pas2;}
else if (file_exists($pas3)) {$full_image2 = $pas3;}
else if (file_exists($pas4)) {$full_image2 = $pas4;}
$mainframe = Jfactory::getApplication();
$newcat = $catModel->getCategory(max($prow->categories));

?>

<a href="images/catalog/<?php echo $icode; ?>.jpg" onclick="return hs.expand(this,{captionText: this.getAttribute('caption'), slideshowGroup: 'group<?php echo $prow->id;?>'})" class="highslide"  title="<?php echo $newcat->category_name.' '.$prow->product_name ?>"  caption="<?php echo $newcat->category_name.' '.$prow->product_name ?>" target="_blank" >
<img src="templates/urbis/images/grey.gif" data-original="<?php echo JURI::base(true).'/'.$full_image; ?>"   alt="<?php echo $prow->product_name ?>" class="lazy" />
</a>

</td>
<td align="center"><?php echo substr($icode, -5, 5);?></td>

<td align="left" ><h3>
<?php echo JHTML::link ( JRoute::_ ( 'index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $prow->virtuemart_product_id . '&virtuemart_category_id=' . $newcat->virtuemart_category_id ), $newcat->category_name, array ('title' => $prow->product_name ) ); 
echo '<br/>'.$prow->product_name;
?>
</h3></td>
<td align="center"><?php echo $currency->createPriceDiv ('salesPrice', '', $prow->product_price, FALSE); ?></td>
</tr>
<?php } ?>
</table>
</fieldset>
		<span class="addtocart-button" style="float:right;">
  <input type="submit" name="addtocart" class="addtocart-button" value="Купить" title="Купить">
</span>

  <input name="option" value="com_virtuemart" type="hidden">
  <input name="view" value="cart" type="hidden">
  <input name="task" value="addJS" type="hidden">	
  <input name="format" value="json" type="hidden">	
</form>

<div id="catproduct-loading">
    <img src="plugins/vmcustom/catproduct/catproduct/css/ajax-loader.gif" />
 </div>


</div>
</div>

<?php } 
}
?>
