<?php
/*------------------------------------------------------------------------
# com_universal_ajaxlivesearch - Universal AJAX Live Search 
# ------------------------------------------------------------------------
# author    Janos Biro 
# copyright Copyright (C) 2011 Offlajn.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.offlajn.com
-------------------------------------------------------------------------*/
// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
ob_end_clean();
if ($_GET['search_exp']!=''){
 
  require_once( dirname(__FILE__).'/helpers/functions.php' );
  require_once( dirname(__FILE__).'/helpers/caching.php' );
  require_once JPATH_SITE . DS . 'administrator' . DS . 'components' . DS . 'com_search/helpers/search.php';
  jimport( 'joomla.html.parameter' );

  $areas="";
  $db =& JFactory::getDBO();
  $pluginlist = array();
  $searchresult = array();
  settype($_GET['module_id'], 'integer');
  $q =  sprintf("SELECT params FROM #__modules WHERE id = %d " ,$_GET['module_id']);
  SearchHelper::logSearch($_GET['search_exp']);
  $db->setQuery($q);
  $res = $db->loadResult();
  $params = new JParameter("");
  parseParams($params, $res);
  $plugins = $params->get('plugins', '');
  $suggestionlang = $params->get('suggestionlang', 'en');
  $introlength = $params->get('introlength', 50);
  jimport( 'joomla.application.component.model' );
  JPluginHelper::importPlugin('search');
  $imagecache = new OfflajnImageCaching;
  $searchparams = array($_GET['search_exp'],'all','newest');
  
  $dispatcher =& JDispatcher::getInstance();
  $results = $dispatcher->trigger( 'onContentSearch', $searchparams );

  $db->setQuery("SELECT extension_id, name FROM #__extensions WHERE type = 'plugin' AND folder = 'search' AND enabled =1 ORDER BY ordering");

  $pluginlist = $db->loadRowList();
  $pluginnames = get_object_vars($plugins->name);
  
  // $plugin : the plugin's search result
  foreach ($results as $pluginkey=>$plugin) {
  	if (count($plugin)){
      if(!in_array($pluginlist[$pluginkey][0], $plugins->enabled)) continue; // Skip if the plugin disabled in the module configuration.
      
      $pluginname = $pluginnames[$pluginlist[$pluginkey][0]];
      $i=0;
      foreach ($plugin as $key=>$value) {

$db = JFactory::getDBO();
$sql = "SELECT MAX(product_price) FROM #__virtuemart_product_prices WHERE virtuemart_product_id = '".$value->virtuemart_product_id."' AND product_price > 0";
$db->setQuery($sql);
$prices = $db->loadResult();
$icode = '00000000';
$product_sku = $value->product_sku;
$len = strlen($value->product_sku);
if ($len == '1') {
$icode = '0000000'.$product_sku;}
else if ($len == '2') {$icode = '000000'.$product_sku;}
else if ($len == '3') {$icode = '00000'.$product_sku;}
else if ($len == '4') {$icode = '0000'.$product_sku;}
else if ($len == '5') {$icode = '000'.$product_sku;}
else if ($len == '6') {$icode = '00'.$product_sku;}
else if ($len == '7') {$icode = '0'.$product_sku;}
else if ($len == '8') {$icode =  $product_sku;}

$pas = 'images/catalog/'.$icode.'.jpg';
$pas2 = 'images/catalog/'.$icode.'.png';
$pas3 = 'images/catalog/'.$icode.'.gif';
$pas4 = 'images/catalog/'.$icode.'.jpeg';
$image_url = '';
if (file_exists($pas)) {
$image_url = $pas;}
else if (file_exists($pas2)) {$image_url = $pas2;}
else if (file_exists($pas3)) {$image_url = $pas3;}
else if (file_exists($pas4)) {$image_url = $pas4;}
else $image_url = 'images/catalog/noimage.jpg';

$searchresult[$pluginname][$i] = new stdClass();

        $searchresult[$pluginname][$i]->title = '<img src="'.JURI::root().$image_url.'" style="float: left; width: 40px; margin-right: 10px;"/>'.$value->section.'<br/><small>'.$value->title."</br>Цена с НДС: </small><font style='font-weight: bold; color: #D92236;'>".number_format($prices, 2, '.', ' ')." р.</font>"; 
        $searchresult[$pluginname][$i]->text = $value->title."</br><font style='font-weight: normal;'>Цена с НДС: <b style='color: #ff0000;'>".number_format($prices, 2, '.', ' ')."</b> р.</font>";
        $searchresult[$pluginname][$i]->href = html_entity_decode(JRoute::_($value->href));
        $i++;
      }
    }
  }
  
  if ((!is_array($searchresult) || count($searchresult)<=0) &&  $params->get('suggest', 0)) {
   $sugg = "";
   if(function_exists("curl_init") && $params->get('usecurl', 0)==1 && curl_init("http://google.com")!==false) {
    $curl = curl_init("http://www.google.com/complete/search?output=toolbar&ie=utf-8&oe=utf-8&hl=$suggestionlang&q=".urlencode($_GET['search_exp']));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $sugg = curl_exec ($curl);
    curl_close ($curl);
   } else if(@file_get_contents("http://google.com")!==false && $params->get('usecurl', 0)==0) {
    $sugg = file_get_contents("http://www.google.com/complete/search?output=toolbar&ie=utf-8&oe=utf-8&hl=$suggestionlang&q=".urlencode($_GET['search_exp']));
   }
   $tags = explode('<suggestion data="', $sugg);
   $i = 0;
   foreach ($tags as $tag) {
    if ($i != 0) {    
      $temp = explode('"/>', $tag);
      $tag = $temp[0];
      $searchresult['nores'][]->tag = $tag;
    }
    $i++;
   }
  }
  
  echo json_encode($searchresult);
  exit();
}
?>