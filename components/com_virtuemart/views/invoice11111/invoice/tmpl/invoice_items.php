<?php
/**
*
* Order items view
*
* @package	VirtueMart
* @subpackage Orders
* @author Max Milbers, Valerie Isaksen
* @link http://www.virtuemart.net
* @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* @version $Id: details_items.php 5432 2012-02-14 02:20:35Z Milbo $
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
$catModel = VmModel::getModel('Category');
$prodModel = VmModel::getModel('Product');
 if ( VmConfig::get('show_tax')) {
    $colspan=7;
 } else {
    $colspan=8;
 }

$db =& JFactory::getDBO();
$mainframe = Jfactory::getApplication();
$curid = $mainframe->getUserStateFromRequest( "virtuemart_currency_id", 'virtuemart_currency_id',JRequest::getInt('virtuemart_currency_id', $vendor_currency['vendor_currency']) );
//print_r($this);
if(!$curid) $curid = $this->orderDetails['details']['BT']->user_currency_id;
$curid = 194;
$query = 'SELECT currency_name FROM #__virtuemart_currencies WHERE virtuemart_currency_id = '.$curid;
$db->setQuery($query);
$curname = $db->loadResult();
?>
<table class="html-email" width="100%" cellspacing="0" cellpadding="0" border="1">
	<tr align="left" class="sectiontableheader">
		<td align="left" colspan="2" width="38%" ><strong><?php echo JText::_('COM_VIRTUEMART_PRODUCT_NAME_TITLE') ?></strong></td>
		<td align="center" width="10%"><strong><?php echo JText::_('COM_VIRTUEMART_ORDER_PRINT_PRODUCT_STATUS') ?></strong></td>
		<td align="center" width="14%" ><strong><?php echo JText::_('COM_VIRTUEMART_ORDER_PRINT_PRICE') ?></strong></td>
		<td align="center" width="6%"><strong><?php echo JText::_('COM_VIRTUEMART_ORDER_PRINT_QTY') ?></strong></td>
		<td align="right" width="20%"><strong>Всего без НДС, <?php echo $curname; ?></strong></td>
	</tr>

<?php



	foreach($this->orderDetails['items'] as $item) {
		$qtt = $item->product_quantity ;
		$product_link = JURI::root().'index.php?option=com_virtuemart&view=productdetails&virtuemart_category_id=' . $item->virtuemart_category_id .
			'&virtuemart_product_id=' . $item->virtuemart_product_id;

		?>
		<tr valign="top">

			<td align="left" colspan="2" >
<?php 

$pros = $prodModel->getProduct($item->virtuemart_product_id);
$newcat = $catModel->getCategory(max($pros->categories));
echo JHTML::link ( JRoute::_ ( 'index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $item->virtuemart_product_id . '&virtuemart_category_id=' . $newcat->virtuemart_category_id ), $newcat->category_name, array ('title' =>$item->order_item_name) ); 
echo '<br/><small>'.$item->order_item_name.'</small>'; 

?></a>
				<?php
// 				vmdebug('$item',$item);
					if (!empty($item->product_attribute)) {
							if(!class_exists('VirtueMartModelCustomfields'))require(JPATH_VM_ADMINISTRATOR.DS.'models'.DS.'customfields.php');
							$product_attribute = VirtueMartModelCustomfields::CustomsFieldOrderDisplay($item,'FE');
						echo $product_attribute;
					}
				?>
			</td>
			<td align="center">
				<?php echo $this->orderstatuses[$item->order_status]; ?>
			</td>
 <td style="text-align: center;">
<?php 
echo str_replace('$', ' ', ($this->currency->priceDisplay($item->product_final_price, $curid))); ?>
		    </td>
			<td align="center" >
				<?php echo $qtt; ?>
			</td>


			<td align="right"  class="priceCol">
				<?php
echo str_replace('$', ' ', ($this->currency->priceDisplay(  $item->product_subtotal_with_tax ,$curid))); //No quantity or you must use product_final_price
 ?>
			</td>
		</tr>

<?php
	}
?>
 <tr class="sectiontableentry1">
			<td colspan="5" align="right"><?php echo JText::_('COM_VIRTUEMART_ORDER_PRINT_PRODUCT_PRICES_TOTAL'); ?></td>

			<td align="right"><?php echo str_replace('$', ' ', ($this->currency->priceDisplay($this->orderDetails['details']['BT']->order_salesPrice, $curid))) ?></td>
		  </tr>
<?php
if ($this->orderDetails['details']['BT']->coupon_discount <> 0.00) {
    $coupon_code=$this->orderDetails['details']['BT']->coupon_code?' ('.$this->orderDetails['details']['BT']->coupon_code.')':'';
	?>
	<tr>
		<td align="right" class="pricePad" colspan="3"><?php echo JText::_('COM_VIRTUEMART_COUPON_DISCOUNT').$coupon_code ?></td>
			<td align="right"></td>

			<?php if ( VmConfig::get('show_tax')) { ?>
				<td align="right"> </td>
                                <?php } ?>
		<td align="right"><?php echo '- '.$this->currency->priceDisplay($this->orderDetails['details']['BT']->coupon_discount, $this->currency); ?></td>
		<td align="right"></td>
	</tr>
<?php  } ?>


	

	<tr>
		<td align="right" class="pricePad" colspan="5"><?php 
echo $this->orderDetails['shipmentName'] ?></td>
		<td align="right"><?php echo str_replace('$', ' ', ($this->currency->priceDisplay($this->orderDetails['details']['BT']->order_shipment, $curid))); ?></td>
	</tr>



	<tr>
		<td align="right" class="pricePad" colspan="5"><strong><?php echo JText::_('COM_VIRTUEMART_ORDER_PRINT_TOTAL') ?></strong></td>
		<td align="right"><strong><?php echo str_replace('$', ' ', ($this->currency->priceDisplay($this->orderDetails['details']['BT']->order_total, $curid)));
 ?></strong></td>
	</tr>

</table>
