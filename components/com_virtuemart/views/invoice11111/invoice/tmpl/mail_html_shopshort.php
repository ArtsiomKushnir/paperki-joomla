<?php


// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
?>


<table width="100%" border="0" cellpadding="0" cellspacing="0" class="html-email">

<tr>
    <td colspan="3">
<b>Уважаемый(ая) 
<?php foreach ($this->userfields['fields'] as $field) {
if ($field['name'] == 'name') { ?>
<span class="values vm2<?php echo '-' . $field['name'] ?>" ><?php echo $this->escape($field['value']) ?></span>
<?php }

if ($field['name'] == 'first_name') { 
?>
<span class="values vm2<?php echo '-' . $field['name'] ?>" ><?php echo $this->escape($field['value']) ?></span>
<?php }

if ($field['name'] == 'middle_name') { ?>
<span class="values vm2<?php echo '-' . $field['name'] ?>" ><?php echo $this->escape($field['value']) ?></span>
<?php }
}
?>, благодарим за заказ в интернет-магазине <?php echo $this->vendor->vendor_store_name; ?>!
</b> 
</br>
</br>
Ваш заказ получен и передан в торговый отдел!</br>
После подтверждения администратором Вы получите соответствующее уведомление и счет на оплату.

 </td>
  </tr>


  <tr>
    <td width="30%">
		<?php echo JText::_('COM_VIRTUEMART_MAIL_SHOPPER_YOUR_ORDER'); ?><br />
		<strong><?php echo $this->orderDetails['details']['BT']->order_number ?></strong>

	</td>
    <td width="30%">
		<?php echo JText::_('COM_VIRTUEMART_MAIL_SHOPPER_YOUR_PASSWORD'); ?><br />
		<strong><?php echo $this->orderDetails['details']['BT']->order_pass ?></strong>
	</td>
    <td width="40%">
    	<p>
 			<a class="default" title="<?php echo $this->vendor->vendor_store_name ?>" href="<?php echo JURI::root().'index.php?option=com_virtuemart&view=orders&layout=details&order_number='.$this->orderDetails['details']['BT']->order_number.'&order_pass='.$this->orderDetails['details']['BT']->order_pass; ?>">
			<?php echo JText::_('COM_VIRTUEMART_MAIL_SHOPPER_YOUR_ORDER_LINK'); ?></a>
		</p>
	</td>
  </tr>
</table>
<table class="html-email" cellspacing="0" cellpadding="0" border="0" width="100%">  <tr  >
	<th width="100%">
	    Содержимое вашего заказа
	</th>
	
    </tr>
</table>