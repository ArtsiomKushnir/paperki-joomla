<?php

if (!defined('_JEXEC'))
die('Direct Access to ' . basename(__FILE__) . ' is not allowed.');

/**
 * Calculation plugin for quantity based price rules
 *
 * @version $Id:
 * @package VirtueMart
 * @subpackage Plugins - quantity
 * @author Max Milbers
 * @copyright Copyright (C) 2011 iStraxx - All rights reserved.
 * @license license.txt Proprietary License. This code belongs to iStraxx UG
 * You are not allowed to distribute or sell this code. You bought only a license to use it for ONE virtuemart installation.
 * You are not allowed to modify this code.
 *
 *
 */

if (!class_exists('vmCalculationPlugin')) require(JPATH_VM_PLUGINS.DS.'vmcalculationplugin.php');

class plgVmCalculationIstraxx_quantity extends vmCalculationPlugin {

	// instance of class
	// 	public static $_this = false;

	function __construct(& $subject, $config) {
		// 		if(self::$_this) return self::$_this;
		parent::__construct($subject, $config);

		$this->tableFields = array('id','virtuemart_calc_id','calc_affected','calc_amount_cond','calc_amount_cond_min','calc_amount_cond_max','calc_amount_dimunit','calc_quantity_relvalue');

		$this->_tableId = 'id';
		// 		self::$_this = $this;
	}


	function plgVmOnStoreInstallPluginTable() {
		return $this->plgVmOnStoreInstallPluginTable('calculation');
	}


	/**
	 * Gets the sql for creation of the table
	 * @author Max Milbers
	 */
	public function getVmPluginCreateTableSQL() {

		return "CREATE TABLE IF NOT EXISTS `" . $this->_tablename . "` (
			    `id` mediumint(1) unsigned NOT NULL AUTO_INCREMENT ,
			    `virtuemart_calc_id` mediumint(1) UNSIGNED DEFAULT NULL,
			    `calc_affected` char(6),
			    `calc_amount_cond` blob,
			    `calc_amount_cond_min` blob,
			    `calc_amount_cond_max` blob,
			    `calc_amount_dimunit` char(12),
			    `calc_quantity_relvalue` blob,
			    `created_on` datetime NOT NULL default '0000-00-00 00:00:00',
			    `created_by` int(11) NOT NULL DEFAULT 0,
			    `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
			    `modified_by` int(11) NOT NULL DEFAULT 0,
			    `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
			    `locked_by` int(11) NOT NULL DEFAULT 0,
			     PRIMARY KEY (`id`),
			     KEY `idx_virtuemart_calc_id` (`virtuemart_calc_id`)
			) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Weight Countries Table' AUTO_INCREMENT=1 ;";
	}


	function plgVmAddMathOp(&$entryPoints){
		$entryPoints[] = array('calc_value_mathop' => '-log', 'calc_value_mathop_name' => '-log Plugin');
	}

	function plgVmOnDisplayEdit(&$calc,&$html){
		$html = '
			<tr>
				<td width="110" class="key">
					<label for="calc_amount_cond">
					'.JText::_('VMCALCULATION_ISTRAXX_QUANTITY_AMOUNT_COND').'
					</label>
				</td>
				<td colspan="4">
					<div class="floatleft width25">
						'.JText::_('VMCALCULATION_ISTRAXX_QUANTITY_AMOUNT_COND_OPERATOR').'<br/><input class="inputbox" type="text" name="calc_amount_cond" id="calc_amount_cond" size="4" value="'.$calc->calc_amount_cond.'" />
					</div>
					<div class="floatleft width75 js_to_qtt_istraxx">
					<span class="floatleft width25">'.JText::_('VMCALCULATION_ISTRAXX_QUANTITY_AMOUNT_COND_MIN').'</span>
					<span class="floatleft width25">'.JText::_('VMCALCULATION_ISTRAXX_QUANTITY_AMOUNT_COND_MAX').'</span>
					<span class="floatleft width25">'.JText::_('VMCALCULATION_ISTRAXX_QUANTITY_RELVALUE').'</span>
					<div class="clear"></div>';
		for($i=0;$i<count($calc->calc_amount_cond_min);$i++ ) {
			$html .= '
					<div class="removable">
					&nbsp;<input class="inputbox width25" type="text" name="calc_amount_cond_min[]" id="calc_amount_cond_min" size="4" value="'.$calc->calc_amount_cond_min[$i].'" />
					&nbsp;<input class="inputbox width25" type="text" name="calc_amount_cond_max[]" id="calc_amount_cond_max" size="4" value="'.$calc->calc_amount_cond_max[$i].'" />
					&nbsp;<input class="inputbox width25" type="text" name="calc_quantity_relvalue[]" id="calc_quantity_relvalue" size="4" value="'.$calc->calc_quantity_relvalue[$i].'" />
					<span class="vmicon vmicon-16-remove"></span>
					</div>';
		}
		$html.= '
					</div>
					<div class="clear js_add_qtt_istraxx">'.JText::_('VMCALCULATION_ISTRAXX_QUANTITY_ADD').'</div>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					'.JText::_('VMCALCULATION_ISTRAXX_QUANTITY_AMOUNT_COND_EXPLAIN').'
				</td>
			</tr>
			<tr>
				<td width="110" class="key">
					<label for="calc_amount_dimunit">
					'.JText::_('VMCALCULATION_ISTRAXX_QUANTITY_AMOUNT_DIMUNIT').'
					</label>
				</td>
				<td colspan="2">
					<input class="inputbox" type="text" name="calc_amount_dimunit" id="calc_amount_dimunit" size="4" value="'.$calc->calc_amount_dimunit.'" />
				</td>
			</tr>';
		static $js;
		if ($js) return true;
		$js = true ;
		//javascript to update price
		$document = JFactory::getDocument();
		$document->addScriptDeclaration('
	jQuery(document).ready( function($) {
		$(".js_add_qtt_istraxx").click(function() {
			var removable=\'<div class="removable">\';
				removable+=\'<input class="inputbox width25" type="text" name="calc_amount_cond_min[]" id="calc_amount_cond_min" size="4" value="" />\';
				removable+=\'<input class="inputbox width25" type="text" name="calc_amount_cond_max[]" id="calc_amount_cond_max" size="4" value="" />\';
				removable+=\'<input class="inputbox width25" type="text" name="calc_quantity_relvalue[]" id="calc_quantity_relvalue" size="4" value="" />\';
				removable+=\'<span class="vmicon vmicon-16-remove"></span>\';
				removable+=\'</div>\' ;
				$(".js_to_qtt_istraxx" ).append(removable);
		});
		$("#admin-ui-tabs" ).delegate("span.vmicon-16-remove", "click",function() {
			$(this).closest(".removable").fadeOut("500",function() {$(this).remove()});
		});
	});
		');
		return true;
	}


	function plgVmInGatherEffectRulesProduct(&$calculationHelper,&$rules){

		foreach ($rules as $r => $rule) {
			$ruleData = null;
			$this->getPluginInternalDataCalc($ruleData,$rule['virtuemart_calc_id']);

			//Check for is = , this works like an override, the table is not used then

			if(strpos($ruleData->calc_amount_cond,'=')===0 ){

				$quantity = (int)substr($ruleData->calc_amount_cond,1);


				if(!empty($quantity) and $quantity != $calculationHelper->_amount){
					//The rule does not hit, so unset the rule
					unset($rules[$r]);
				}
				continue;
			}

$app =& JFactory::getApplication();
    if ($app->isSite())  {
foreach ($calculationHelper->_cart->products as $key => $cobj)
{  $i+= $cobj->amount;}
}
$wi = JRequest::getVar('view');
if($wi == 'productdetails'){
$calculationHelper->_amount += $i;
} else $calculationHelper->_amount = $i;
//exit($i."</br></br>".print_r($wi." "));
			$index = false;
			$withinRange = true;
			for($i = 0;$i<count($ruleData->calc_amount_cond_min);$i++){
				$index = $i;
				//Check if the rule fits into the
				$cacmax = empty($ruleData->calc_amount_cond_max[$i]);
				$cacmin = empty($ruleData->calc_amount_cond_min[$i]);
				$withinRange = false;
				if(!$cacmax || !$cacmin){
					if($cacmax){
						if($ruleData->calc_amount_cond_min[$i] <= $calculationHelper->_amount){
							$withinRange = true;
// 							vmdebug('In Range min '.$i);
							break;
						}

					} else if(!$cacmin){
						if($ruleData->calc_amount_cond_min[$i] <= $calculationHelper->_amount &&  $calculationHelper->_amount<= $ruleData->calc_amount_cond_max[$i]){
							// <= $ruleData->calc_amount_cond_max){
							$withinRange = true;
// 							vmdebug('In Range max min '.$i);
							break;
						}
					} else {
						if($ruleData->calc_amount_cond_max[$i] >= $calculationHelper->_amount){
							$withinRange = true;
// 							vmdebug('In Range max '.$i);
							break;
						}
					}
				} else {
					$withinRange = true;
// 					vmdebug('In Range nothing set '.$i);
					break;
				}
			}

			$caccond = empty($ruleData->calc_amount_cond);
			if(!$withinRange){

				unset($rules[$r]);
				continue;
// 				vmdebug('plgVmInGatherEffectRulesProduct unsetting rule not in range '.$rule['calc_name'].' '.$i.' '.count($rules));
			} else
			//The quantity is within the range, so we must check the quantity condition
			if(!$caccond){

				//Check for modulo
				if(strpos($ruleData->calc_amount_cond,'%')==0){
					$quantity = substr($ruleData->calc_amount_cond,1);
// 					vmdebug('plgVmInGatherEffectRulesProduct ');
					if($calculationHelper->_amount%$quantity!=0){
						//The rule does not hit, so unset the rule
						unset($rules[$r]);
						// 						vmdebug('plgVmInGatherEffectRulesProduct unsetting rule mod '.$rule['calc_name'].' '.$i.' '.count($rules));
						continue;
					}
				} else {
					//Check for no extra char
					$quantity = $ruleData->calc_amount_cond;

					if(!empty($quantity) and $quantity != $calculationHelper->_amount){
						//The rule does not hit, so unset the rule
						unset($rules[$r]);
						continue;
					}
				}
			}

			if(isset($rules[$r]) and !empty($ruleData->calc_quantity_relvalue[$index])){
				$rules[$r]['calc_value'] = 50;
// 				vmdebug('change rule value '.$rules[$r]['calc_value']);
			} else {
// 				vmdebug('dont change rule value '.$rules[$r]['calc_value']);
			}
		}
	}


	function interpreteMathOp ($calculationHelper, $mathop, $value, $price, $currency=0){

		$calculated = false;
		$sign = substr($mathop, 0, 1);

		if(empty($value)) return false;
		if (strlen($mathop) == 4) {
			$cmd = substr($mathop, 1);
			if ($cmd == 'log') {
				//Nice thing, try this with value 2
				if($value>0){
					$base = 1;
					$calculated = 0;
				}

				// 				vmdebug('interpreteMathOp plugin, price '.$price.' '.$sign.' '.$calculated.' = '.($price - $calculated));
				if($calculated){
					if($sign == '+'){
						return 0;
					} else if($sign == '-'){
						// 						vmdebug('return $price - $calculated '.($price - $calculated));
						return 0;
					}
				}

			}
		}

		return false;
	}

	public function plgVmStorePluginInternalDataCalc(&$data){


		$containsData = false;
		$plgData = array('calc_amount_cond_min','calc_amount_cond_max','calc_quantity_relvalue');
		foreach($plgData as $field){

			if(!empty($data[$field])){
				$data[$field] = serialize($data[$field]);
				$containsData = true;
			}
		}

		if(!$containsData and empty($data['calc_amount_cond'])){
			$this->removePluginInternalData($data['virtuemart_calc_id']);
		} else {
			if(!empty($data['virtuemart_calc_id'])){
				// 			vmdebug('plgVmStorePluginInternalDataCalc $data',$data);
				$ruleData->calc_amount_cond = trim($ruleData->calc_amount_cond);
				$this->storePluginInternalData($data);
			} else {
				vmError('Cant store quantity rule, virtuemart_calc_id empty');
			}

		}


	}

	public function plgVmGetPluginInternalDataCalc(&$calcData){

		return $this->getPluginInternalDataCalc($calcData,$calcData->virtuemart_calc_id);
	}

	protected function getPluginInternalDataCalc(&$calcData,$id){

		$datas = $this->getPluginInternalData($id,'virtuemart_calc_id');

		//vmdebug('getPluginInternalDataCalc',$datas);
		if($datas){
			$attribsCalc = get_object_vars($datas);

			unset($attribsCalc['virtuemart_calc_id']);
			foreach($attribsCalc as $k=>$v){
				$calcData->$k = $v;
			}

			$plgData = array('calc_amount_cond_min','calc_amount_cond_max','calc_quantity_relvalue');
			foreach($plgData as $field){
				if(!empty($calcData->$field) && !is_int($calcData->$field)){
					$calcData->$field = unserialize($calcData->$field);
				}
			}
		}

		return true;

	}

	public function plgVmInGatherEffectRulesBill(&$calculationHelper,&$rules){

		return false;
	}

	public function plgVmDeleteCalculationRow($id){
		$this->removePluginInternalData($id);
	}
}

// No closing tag
