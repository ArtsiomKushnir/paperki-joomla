<?php
/*
* @package VirtueMart
* @subpackage payment
* @author assist.ru
* @copyright Copyright (C) 2014 assist.ru. All rights reserved.
*/

defined('_JEXEC') or die;

if (!class_exists('vmPSPlugin'))
	require(JPATH_VM_PLUGINS.DS.'vmpsplugin.php');

class plgVMPaymentAssist extends vmPSPlugin {

    public static $_this = false;

    function __construct($subject, $config) {
		
		parent::__construct($subject, $config);

		$this->_loggable = true;
		$this->tableFields = array_keys($this->getTableSQLFields());
		$this->_tablepkey = 'id';
		$this->_tableId = 'id';
		$varsToPush = $this->getVarsToPush();
		$this->setConfigParameterable($this->_configTableFieldName, $varsToPush);
	}

    protected function getVmPluginCreateTableSQL() {
		return $this->createTableSQL('Payment '.$this->_name.' Table');
    }

	function getTableSQLFields() {

		$SQLfields = array(
			'id' => 'int(11) UNSIGNED NOT NULL AUTO_INCREMENT',
			'virtuemart_order_id' => 'int(1) UNSIGNED',
			'order_number' => 'char(64)',
			'virtuemart_paymentmethod_id' => 'mediumint(1) UNSIGNED',
			'payment_name' => 'varchar(5000)',
			'payment_order_total' => 'decimal(15,5) NOT NULL DEFAULT \'0.00000\'',
			'payment_currency' => 'char(3)',
			'cost_per_transaction' => 'decimal(10,2)',
			'cost_percent_total' => 'decimal(10,2)',
			'tax_id' => 'smallint(1)',
			$this->_name.'_custom' => 'varchar(255)',
			$this->_name.'response_raw' => 'varchar(512)'
		);

		return $SQLfields;
	}

	function plgVmConfirmedOrder($cart, $order) {

		if (!($method = $this->getVmPluginMethod($order['details']['BT']->virtuemart_paymentmethod_id)))
	   		return null;

		if (!$this->selectedThisElement($method->payment_element))
	    	return false;

		$session = JFactory::getSession();
		$return_context = $session->getId();
		$this->_debug = $method->debug;
		$this->logInfo('plgVmConfirmedOrder order number: '.$order['details']['BT']->order_number, 'message');

		if (!class_exists('VirtueMartModelOrders'))
			require(JPATH_VM_ADMINISTRATOR.DS.'models'.DS.'orders.php');
		
		if (!class_exists('VirtueMartModelCurrency'))
			require(JPATH_VM_ADMINISTRATOR.DS.'models'.DS.'currency.php');

		$address = isset($order['details']['ST']) ? $order['details']['ST'] : $order['details']['BT'];

		if (!class_exists('TableVendors'))
    		require(JPATH_VM_ADMINISTRATOR.DS.'table'.DS.'vendors.php');
		
		$vendorModel = VmModel::getModel('Vendor');
		$vendorModel->setId(1);
		$vendor = $vendorModel->getVendor();
		$vendorModel->addImages($vendor, 1);
		$this->getPaymentCurrency($method);

		$q = 'SELECT `currency_code_3` FROM `#__virtuemart_currencies` WHERE `virtuemart_currency_id` = "'.$method->payment_currency.'"';
		$db = JFactory::getDBO();
		$db->setQuery($q);
		$currency_code_3 = $db->loadResult();

		$paymentCurrency = CurrencyDisplay::getInstance($method->payment_currency);
		$totalInPaymentCurrency = round($paymentCurrency->convertCurrencyTo($method->payment_currency, $order['details']['BT']->order_total, false), 2);
		$cd = CurrencyDisplay::getInstance($cart->pricesCurrency);
		
		if ($totalInPaymentCurrency <= 0) {
			vmInfo(JText::_('Ошибка: Цена товара не может быть отрицательной!'));
			return false;
		}

		//$debug = $this->_debug($method);
		$merchant_id = $this->_getmerchant_id($method);
		
		if (empty($merchant_id)) {
	    	vmInfo('Ошибка: Идентификатор магазина не верный');
	    	return false;
		}
		
		$merchant_url = $this->_getmerchant_url($method);
		
		if (empty($merchant_url)) {
	    	vmInfo('Ошибка: URL не верный!');
	    	return false;
		}

		$order_id = $order['details']['BT']->order_number;
		
		$virtuemart_paymentmethod_id = $order['details']['BT']->virtuemart_paymentmethod_id;
		
		$post_variables = array(
			'Merchant_ID' => $merchant_id,
			'OrderNumber' => $order_id,
			'OrderAmount' => $totalInPaymentCurrency,
			'OrderCurrency' => $currency_code_3,
			'FirstName' => $address->first_name,
			'LastName' => $address->last_name,
			'Middlename' => $address->middle_name,
			'Email' => $address->email,
			'Address' => $address->address_1,
			'HomePhone' => $address->phone_2,
			'MobilePhone' => $address->phone_1,
			'Fax' => $address->fax,
			'Country' => ShopFunctions::getCountryByID($address->virtuemart_country_id, 'country_3_code'),
			'State' => isset($address->virtuemart_state_id) ? ShopFunctions::getStateByID($address->virtuemart_state_id) : '',
			'City' => $address->city,
			'Zip' => $address->zip,
			'URL_RETURN_OK' => JROUTE::_(JURI::root().'index.php?option=com_virtuemart&view=pluginresponse&task=pluginresponsereceived&pm='.$virtuemart_paymentmethod_id.'&order_id='.$order_id),
			'URL_RETURN_NO' => JROUTE::_(JURI::root().'index.php?option=com_virtuemart&view=pluginresponse&task=pluginUserPaymentCancel&on='.$order_id.'&pm='.$virtuemart_paymentmethod_id),
'CheckValue' => strtoupper(md5(strtoupper(md5($method->secret_word).md5($merchant_id.$order_id.$totalInPaymentCurrency.$currency_code_3))))
		);
		
		$dbValues['order_number'] = $order['details']['BT']->order_number;
		$dbValues['payment_name'] = $this->renderPluginName($method, $order);
		$dbValues['virtuemart_paymentmethod_id'] = $cart->virtuemart_paymentmethod_id;
		$dbValues[$this->_name.'_custom'] = $return_context;
		$dbValues['cost_per_transaction'] = $method->cost_per_transaction;
		$dbValues['cost_percent_total'] = $method->cost_percent_total;
		$dbValues['payment_currency'] = $method->payment_currency;
		$dbValues['payment_order_total'] = $totalInPaymentCurrency;
		$dbValues['tax_id'] = $method->tax_id;
		$this->storePSPluginInternalData($dbValues);

		$url = $merchant_url;
		$url = 'https://'.$method->merchant_url.'/pay/order.cfm';
		$html = '<html><head><title>Перенаправление...</title></head><body><div style="margin: auto; text-align: center;">';
		$html .= '<form action="'.$url.'" method="post" name="vm_'.$this->_name.'_form">';
		$html .= '<input type="image" name="submit" src="http://www.assist.ru/images/assist_logo.gif" alt="assist.ru" />';
		
		foreach ($post_variables as $name => $value) {
			if (trim($value))
				$html.= '<input type="hidden" name="'.$name.'" value="'.htmlspecialchars($value).'" />';
		}
		
		$html .= '</form></div>';
		$html .= '<script type="text/javascript">';
		$html .= 'document.vm_'.$this->_name.'_form.submit();';
		$html .= '</script></body></html>';

		$cart->_confirmDone = false;
		$cart->_dataValidated = false;
		$cart->setCartIntoSession();
		JRequest::setVar('html', $html);
	}
	
    function _getmerchant_id($method) {
		return $method->merchant_id;
    }

	function _debug($method) {
		return $method->debug;
    }
	
	function _getmerchant_url($method) {
		return $method->merchant_url;
    }
	
	function plgVmgetPaymentCurrency($virtuemart_paymentmethod_id, $paymentCurrencyId) {

		if (!($method = $this->getVmPluginMethod($virtuemart_paymentmethod_id)))
	    	return null;
			
		if (!$this->selectedThisElement($method->payment_element))
	    	return false;

		$this->getPaymentCurrency($method);
		$paymentCurrencyId = $method->payment_currency;
	}

    function plgVmOnPaymentResponseReceived($html) {
		
		$virtuemart_paymentmethod_id = JRequest::getInt('pm', 0);
		$vendorId = 0;
		
		if (!($method = $this->getVmPluginMethod($virtuemart_paymentmethod_id)))
	    	return null;
			
		if (!$this->selectedThisElement($method->payment_element))
	    	return null;

		$payment_data = JRequest::get('get');

		//foreach ($payment_data as $key => $val) {
		//	$this->logInfo($key . ' --> ' . $val, '1-post');
		//}
		//$payment_data_ = JRequest::get('get');
		//foreach ($payment_data_  as $key => $val) {
		//	$this->logInfo($key . ' --> ' . $val, '1-get');
		//}

		vmdebug('plgVmOnPaymentResponseReceived', $payment_data);
 
		$order_number = $payment_data['order_id'];
		
		if (!class_exists('VirtueMartModelOrders'))
	    	require(JPATH_VM_ADMINISTRATOR.DS.'models'.DS.'orders.php');
			
		if (!($virtuemart_order_id = VirtueMartModelOrders::getOrderIdByOrderNumber($order_number)))
	    	return null;

		if (!($paymentTable = $this->getDataByOrderId($virtuemart_order_id) ))
	    	return '';

		$payment_name = $this->renderPluginName($method);
		$html = $this->_getPaymentResponseHtml($paymentTable, $payment_name);
		
		if (!class_exists('VirtueMartCart'))
			require(JPATH_VM_SITE.DS.'helpers'.DS.'cart.php');

		$cart = VirtueMartCart::getCart();
		$cart->emptyCart();
			
		return true;
	}
	
	function _getPaymentResponseHtml($paymentTable, $payment_name) {
		
		$html = '<table>'."\n";
		$html .= $this->getHtmlRow('PAYPAL_PAYMENT_NAME', $payment_name);
		
		if (!empty($paymentTable))
	    	$html .= $this->getHtmlRow('PAYPAL_ORDER_NUMBER', $paymentTable->order_number);
		
		$html .= '</table>'."\n";

		return $html;
	}


    function plgVmOnUserPaymentCancel() {

		if (!class_exists('VirtueMartModelOrders'))
	    	require(JPATH_VM_ADMINISTRATOR.DS.'models'.DS.'orders.php');

		$order_number = JRequest::getString('on', '');
		$virtuemart_paymentmethod_id = JRequest::getInt('pm', '');
		
		if (empty($order_number) or empty($virtuemart_paymentmethod_id) or !$this->selectedThisByMethodId($virtuemart_paymentmethod_id))
	    	return null;

		if (!($virtuemart_order_id = VirtueMartModelOrders::getOrderIdByOrderNumber($order_number)))
			return null;

		if (!($paymentTable = $this->getDataByOrderId($virtuemart_order_id)))
	    	return null;
	
		$session = JFactory::getSession();
		$return_context = $session->getId();
		$field = $this->_name.'_custom';
		
		if (strcmp($paymentTable->$field, $return_context) === 0)
			//$this->handlePaymentUserCancel($virtuemart_order_id);

		return true;
	}

    function plgVmOnPaymentNotification() {

		if(JRequest::getVar('method','')!='assist') {
			return NULL;
		}
		
		if (!class_exists('VirtueMartModelOrders'))
	    	require(JPATH_VM_ADMINISTRATOR.DS.'models'.DS.'orders.php');
	
		$payment_data = JRequest::get('post');

		//foreach ($payment_data as $key => $val) {
		//	$this->logInfo($key . ' --> ' . $val, 'notification');
		//}
		
		if (!isset($payment_data['ordernumber']))
	    	return;

		$order_number = $payment_data['ordernumber'];
		
		if (!($virtuemart_order_id = VirtueMartModelOrders::getOrderIdByOrderNumber($order_number)))
	    	return;
		
		$this->logInfo('plgVmOnPaymentNotification: virtuemart_order_id  found '.$virtuemart_order_id, 'message');


		$vendorId = 0;
		
		if (!($payment = $this->getDataByOrderId($virtuemart_order_id)))
	    	return;


		$method = $this->getVmPluginMethod($payment->virtuemart_paymentmethod_id);

		$this->logInfo($method, 'method');
		$this->logInfo($this->selectedThisElement($method->payment_element), 'method is');

		if (!$this->selectedThisElement($method->payment_element))
	    	return false;

		$this->_debug = $method->debug;
	
		$this->logInfo($this->_name.'_data '.implode('   ', $payment_data), 'message');

		$db = JFactory::getDBO();
		$query = 'SHOW COLUMNS FROM `'.$this->_tablename.'`';
		$db->setQuery($query);
		$columns = $db->loadResultArray(0);
		
		$post_msg = '';
		foreach ($payment_data as $key => $value) {
			$post_msg .= $key.'='.$value.'<br />';
			$table_key = $this->_name.'_response_'.$key;
			
			if (in_array($table_key, $columns)) {
				$response_fields[$table_key] = $value;
				
			}
		}
		
		$response_fields['payment_name'] = $this->renderPluginName($method);
		$response_fields[$this->_name.'response_raw'] = $post_msg;
		$response_fields['order_number'] = $order_number;
		$response_fields['virtuemart_order_id'] = $virtuemart_order_id;

		$this->storePSPluginInternalData($response_fields);
	     
		$hash = strtoupper(md5(strtoupper(md5($method->secret_word).md5($payment_data['merchant_id'].$payment_data['ordernumber'].$payment_data['amount'].$payment_data['currency'].$payment_data['orderstate']))));

		$order['comments'] = 'Платеж по заказу '.$order_number.' (биллинговый номер: ' . $payment_data['billnumber'] . ') зафиксирован в системе Assist со статусом: ' . $payment_data['orderstate'] . ' (' . $payment_data['customermessage'] . ')';

		$flag_error = "0";

		if ($hash != $payment_data['checkvalue']) {
		$order['comments'] = 'Платеж по заказу '.$order_number.' (биллинговый номер: ' . $payment_data['billnumber'] . ') зафиксирован со статусом: Ошибка! Не совпадают контрольные суммы';
		$flag_error = "1";
		}

		//$this->logInfo($payment->payment_order_total, 'ORDER AMOUNT');
		if ($payment_data['orderamount'] != $payment->payment_order_total) {
		$order['comments'] = 'Платеж по заказу '.$order_number.' (биллинговый номер: ' . $payment_data['billnumber'] . ') зафиксирован со статусом: Ошибка! Не совпадает сумма платежа';
		$flag_error = "1";
		}
		$new_status = "P";
		if ( $payment_data['orderstate'] == "Approved" && $flag_error == "0") {
				$new_status = "C";
		}
		if ( $payment_data['orderstate'] == "Declined" && $flag_error == "0") {
				$new_status = "X";
		}
		//$this->logInfo($new_status, 'status_real');

	    if (!class_exists('VirtueMartModelOrders'))
			require(JPATH_VM_ADMINISTRATOR.DS.'models'.DS.'orders.php');
	    
		$modelOrder = VmModel::getModel('orders');
	    $order['order_status'] = $new_status;
	    $order['virtuemart_order_id'] = $virtuemart_order_id;
	    $order['customer_notified'] = 1;
	    $modelOrder->updateStatusForOneOrder($virtuemart_order_id, $order, true);
		
		$this->emptyCart();
	}
   
    function getCosts(VirtueMartCart $cart, $method, $cart_prices) {
		if (preg_match('/%$/', $method->cost_percent_total))
	    	$cost_percent_total = substr($method->cost_percent_total, 0, -1);
		else
	    	$cost_percent_total = $method->cost_percent_total;

		return $method->cost_per_transaction + ($cart_prices['salesPrice'] * $cost_percent_total * 0.01);
    }

    protected function checkConditions($cart, $method, $cart_prices) {
		$address = $cart->ST == 0 ? $cart->BT : $cart->ST;
		$amount = $cart_prices['salesPrice'];
		$amount_cond = $amount >= $method->min_amount AND $amount <= $method->max_amount OR ($method->min_amount <= $amount AND $method->max_amount == 0);
		$countries = array();
	
		if (!empty($method->countries)) {
	    	if (!is_array($method->countries))
				$countries[0] = $method->countries;
	    	else
				$countries = $method->countries;
		}

		if (!is_array($address)) {
	    	$address = array();
	    	$address['virtuemart_country_id'] = 0;
		}

		if (!isset($address['virtuemart_country_id']))
	    	$address['virtuemart_country_id'] = 0;
		
		if (in_array($address['virtuemart_country_id'], $countries) || count($countries) == 0) {
	    	if ($amount_cond)
				return true;
		}

		return false;
	}
	
	function plgVmOnStoreInstallPaymentPluginTable($jplugin_id) {
		return $this->onStoreInstallPluginTable($jplugin_id);
    }

    public function plgVmOnSelectCheckPayment(VirtueMartCart $cart) {
		return $this->OnSelectCheck($cart);
    }

    public function plgVmDisplayListFEPayment(VirtueMartCart $cart, $selected = 0, &$htmlIn) {
		return $this->displayListFE($cart, $selected, $htmlIn);
    }

    public function plgVmonSelectedCalculatePricePayment(VirtueMartCart $cart, array $cart_prices, &$cart_prices_name) {
		return $this->onSelectedCalculatePrice($cart, $cart_prices, $cart_prices_name);
    }

    function plgVmOnCheckAutomaticSelectedPayment(VirtueMartCart $cart, array $cart_prices = array()) {
		return $this->onCheckAutomaticSelected($cart, $cart_prices);
    }

    public function plgVmOnShowOrderFEPayment($virtuemart_order_id, $virtuemart_paymentmethod_id, $payment_name) {
		$this->onShowOrderFE($virtuemart_order_id, $virtuemart_paymentmethod_id, $payment_name);
    }

	function plgVmonShowOrderPrintPayment($order_number, $method_id) {
		return $this->onShowOrderPrint($order_number, $method_id);
    }

	function plgVmDeclarePluginParamsPayment($name, $id, $data) {
		return $this->declarePluginParams('payment', $name, $id, $data);
    }

    function plgVmSetOnTablePluginParamsPayment($name, $id, $table) {
		return $this->setOnTablePluginParams($name, $id, $table);
    }

	function logInfo ($text, $type = 'message') {
		$file = JPATH_ROOT . "/logs/" . $this->_name . ".log";
		$date = JFactory::getDate ();
		$fp = fopen ($file, 'a');
		fwrite ($fp, "\n" . $date->toFormat ('%Y-%m-%d %H:%M:%S'));
		fwrite ($fp, "  " . $type . ': ' . $text);
		fclose ($fp);
	}
}