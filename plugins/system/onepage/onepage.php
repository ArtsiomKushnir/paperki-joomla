<?php
/*
 * Created on Mar 11, 2012
 *
 * Author: Linelab.org
 * Project: plg_onepage_vm2
 */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.plugin.plugin');

class plgSystemOnePage extends JPlugin {
	function __construct($config,$params) {
		parent::__construct($config,$params);
	}
	
	function onAfterRoute() {
		if(JFactory::getApplication()->isAdmin()) {
			return;
		}
		if(JRequest::getCmd('type')=='onepage') {
			require_once JPATH_SITE.DS.'templates'.DS.JFactory::getApplication()->getTemplate().DS.'html'.DS.'com_virtuemart'.DS.'cart'.DS.'helper.php';
			$helper=new CartHelper();
			switch(JRequest::getCmd('task')) {
				case 'set_payment':
					echo json_encode($helper->setPayment());
					break;
				case 'set_shipment':
					echo json_encode($helper->setShipment());
					break;
				case 'set_address':
					$helper->setAddress();
					echo json_encode(array());
					break;
				case 'register':
					$helper->register();
					echo json_encode(array());
					break;
				case 'update_product':
					$ret=$helper->updateProduct();
					echo json_encode($ret);
					break;
				case 'remove_product':
					$ret=$helper->removeProduct();
					echo json_encode($ret);
					break;
				case 'set_coupon':
					$ret=$helper->setCoupon();
					echo json_encode($ret);
					break;
				case 'get_shipments':
					JFactory::getLanguage()->load('com_virtuemart');
					$helper->lSelectShipment();
					echo json_encode($helper->shipments_shipment_rates);
					break;
				case 'get_payments':
					JFactory::getLanguage()->load('com_virtuemart');
					$helper->lSelectPayment();
					echo json_encode($helper->paymentplugins_payments);
					break;
				case 'update_prices':
					echo json_encode($helper->updatePrices());
					break;
				
			}
			JFactory::getApplication()->close();
		}
	}
}
?>