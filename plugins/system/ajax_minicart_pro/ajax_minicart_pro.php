<?php
/**
 * @package Sj_Ajax_Minicart_Pro
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @copyright (c) 2009-2012 YouTech Company. All Rights Reserved.
 * @author YouTech Company http://www.smartaddons.com
 *
 */

defined('_JEXEC') or die;

jimport('joomla.plugin.plugin');

class plgSystemAjax_MiniCart_Pro extends JPlugin {
	
	function onBeforeRender(){
		$is_ajax = !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
		$is_ajax_from_minicart_pro = (int)JRequest::getVar('minicart_ajax', 0);

		if ($is_ajax && $is_ajax_from_minicart_pro){
			if (!class_exists( 'VmConfig' )) require(JPATH_ADMINISTRATOR.'/components/com_virtuemart/helpers/config.php');
			VmConfig::loadConfig();
			if (!class_exists( 'calculationHelper' )) require(JPATH_VM_SITE.'/helpers/cart.php');
			switch( JRequest::getCmd('minicart_task') ){
				case 'setcoupon':
					$coupon_code = JRequest::getVar('coupon_code', '');
					$cart = &VirtueMartCart::getCart();
					$result = new stdClass();
					if ($cart) {
						$msg = $cart->setCouponCode($coupon_code);
						if ( !empty($msg) ) {
							$result->status = 0;
							$result->message = $msg;
								
						} else {
							$result->status = 1;
							$result->message = $cart->couponCode;
						}
					} else {
						$result->status = 0;
						$result->message = 'no cart';
					}
						
					echo json_encode($result);
					exit;
					break;
				
				case 'update':
					$cart_virtuemart_product_id = JRequest::getVar('cart_virtuemart_product_id',array(),'POST', 'array');
					$quantity =  JRequest::getVar('quantity',array(),'POST', 'array');
					$cart = &VirtueMartCart::getCart();
					$result = new stdClass();
					if($cart){
						$count1 = 0;
						$count2 = 0;
						for($i=0; $i<count($cart_virtuemart_product_id);$i++){
							$update_id = $cart_virtuemart_product_id[$i];
							$update_qty = $quantity[$i];
							JRequest::setVar('quantity', $update_qty);
							if( $msg = $cart->updateProductcart($cart_virtuemart_product_id[$i]) ){
								$count1++;
							} else {
								$count2++;
							}
						}
						$result->status = '1';
						$result->message = $count1.'/'.($count1+$count2).' success update.';
						$result->quantity =$quantity;
					} else {
						$result->status = 0;
						$result->message = 'no cart';
					}
					echo json_encode($result);
					exit;
					break;
				case 'refresh':
					ob_start();
					$db = JFactory::getDbo();
					$db->setQuery( 'SELECT * FROM #__modules WHERE id='.JRequest::getInt('minicart_modid') );
					$result = $db->loadObject();
					if (isset($result->module)){
						jimport('joomla.application.module.helper');
						echo JModuleHelper::renderModule($result);
					}
					$list_html = ob_get_contents();
					ob_end_clean();
					$cart = &VirtueMartCart::getCart();
					$vm_currency_display = &CurrencyDisplay::getInstance();
					$lang = JFactory::getLanguage();
					$extension = 'com_virtuemart';
					$lang->load($extension);
					$cart->billTotal = $lang->_('COM_VIRTUEMART_CART_TOTAL').' : <strong>'. $vm_currency_display->priceDisplay($cart->pricesUnformatted['billTotal']) .'</strong>';
					$result = new stdClass();
					$result->list_html = $list_html;
					$result->billTotal = $cart->billTotal;
					$result->length = count($cart->products);
					echo json_encode($result);
					exit;
					break;
				 case 'delete':
					$cart_virtuemart_duct_id = JRequest::getVar('cart_virtuemart_product_id');
					$cart = &VirtueMartCart::getCart();
					$result = new stdClass();
					if($cart){
						$msg = $cart->removeProductCart($cart_virtuemart_duct_id);
						$result->status = 1;
						$result->message = 'success delte';
				
					} else {
						$result->status = 0;
						$result->message = 'no cart';
					}
					echo json_encode($result);
					exit;
					break; 
				default:
					die('invalid task');
					break;
			}
			
			die;
		}
	}
}
