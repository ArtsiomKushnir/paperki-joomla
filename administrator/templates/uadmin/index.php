<?php
/**
 * @package		Joomla.Administrator
 * @subpackage	Templates.bluestork
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.filesystem.file');

$app = JFactory::getApplication();
$doc = JFactory::getDocument();

$doc->addStyleSheet('templates/system/css/system.css');
$doc->addStyleSheet('templates/'.$this->template.'/css/template.css');

if ($this->direction == 'rtl') {
	$doc->addStyleSheet('templates/'.$this->template.'/css/template_rtl.css');
}
/** Load specific language related css */
$lang = JFactory::getLanguage();
$file = 'language/'.$lang->getTag().'/'.$lang->getTag().'.css';
if (JFile::exists($file)) {
	$doc->addStyleSheet($file);
}

if ($this->params->get('textBig')) {
	$doc->addStyleSheet('templates/'.$this->template.'/css/textbig.css');
}

if ($this->params->get('highContrast')) {
	$doc->addStyleSheet('templates/'.$this->template.'/css/highcontrast.css');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo  $this->language; ?>" lang="<?php echo  $this->language; ?>" dir="<?php echo  $this->direction; ?>" >
<head>
<jdoc:include type="head" />

<!--[if IE 7]>
<link href="templates/<?php echo  $this->template ?>/css/ie7.css" rel="stylesheet" type="text/css" />
<![endif]-->

<!--[if gte IE 8]>
<link href="templates/<?php echo  $this->template ?>/css/ie8.css" rel="stylesheet" type="text/css" />
<![endif]-->
</head>
<body id="minwidth-body">
	<div id="border-top" class="h_blue">
		<span class="logo"><a href="http://www.joomla.org" target="_blank"><img src="templates/<?php echo  $this->template ?>/images/logo.png" alt="Joomla!" /></a></span>
		<span class="title"><a href="index.php"><?php echo $this->params->get('showSiteName') ? $app->getCfg('sitename'). " " . JText::_('JADMINISTRATION') : JText::_('JADMINISTRATION') ; ?></a></span>
	</div>
	<div id="header-box">
		<div id="module-menu">
			<jdoc:include type="modules" name="menu" />
		</div>
		<div id="module-status">
			<jdoc:include type="modules" name="status" />

 <script type="text/javascript">
        function countDown(num) {      

           work_number = time_diff[num]--;
	if (work_number<0) {document.getElementById('counter'+num).innerHTML = 'сессия истекла'; } else {
           hours = Math.floor(work_number / 3600);
           work_number -= hours * 3600;
           minutes = Math.floor(work_number / 60);
           work_number -= minutes * 60;
           seconds = Math.floor(work_number % 60);
           hours = (hours < 10) ? "0"+hours : hours;
           minutes = (minutes < 10) ? "0"+minutes : minutes;
           seconds = (seconds < 10) ? "0"+seconds : seconds;
if(hours < 1) {
           values = [
              declOfNum(minutes, ["м", "м", "м"]),  //
              declOfNum(seconds, ["с", "с", "с"]) //
           ];
}
else{
           values = [
              declOfNum(hours, ["ч", "ч", "ч"]),  //
              declOfNum(minutes, ["м", "м", "м"]),  //
              declOfNum(seconds, ["с", "с", "с"]) //
           ];

}
           document.getElementById('counter'+num).innerHTML =  values.join(" ");// + ".";      
		}
        }

        function declOfNum(number, titles) {
           cases = [2, 0, 1, 1, 1, 2];
           return "<b>" + number + "</b>" + titles[ (number%100>4 && number%100<20)? 2 : cases[Math.min(number%10, 5)] ];
        }</script>

<?php
$config = JFactory::getConfig();
$lifetime = ($config->get('lifetime') * 60); ?>

<div id="mc-userinfo">
<div class="session_expire">
<div class="session-tip" id="counter1" style="opacity: 1; visibility: visible; left: -75px;"><?php echo $lifetime;?></div>
<div class="session_progress"></div>
</div></div>


     <script type="text/javascript">
var time_diff=new Array;
time_diff[1] = <?php echo $lifetime;?>;

        setInterval("countDown(1)", 1000);
     </script>
			<?php
				//Display an harcoded logout
				$task = JRequest::getCmd('task');
				if ($task == 'edit' || $task == 'editA' || JRequest::getInt('hidemainmenu')) {
					$logoutLink = '';
				} else {
					$logoutLink = JRoute::_('index.php?option=com_login&task=logout&'. JSession::getFormToken() .'=1');
				}
				$hideLinks	= JRequest::getBool('hidemainmenu');
				$output = array();
				// Print the Preview link to Main site.
				$output[] = '<span class="viewsite"><a href="'.JURI::root().'" target="_blank">'.JText::_('JGLOBAL_VIEW_SITE').'</a></span>';
				// Print the logout link.
				$output[] = '<span class="logout">' .($hideLinks ? '' : '<a href="'.$logoutLink.'">').JText::_('JLOGOUT').($hideLinks ? '' : '</a>').'</span>';
				// Output the items.
				foreach ($output as $item) :
				echo $item;
				endforeach;
			?>
		</div>
		<div class="clr"></div>
	</div>

	<div id="content-box">
		<div id="toolbar-box">
			<div class="m">
				<jdoc:include type="modules" name="toolbar" />
				<jdoc:include type="modules" name="title" />
			</div>
		</div>
		<?php if (!JRequest::getInt('hidemainmenu')): ?>
		<jdoc:include type="modules" name="submenu" style="rounded" id="submenu-box" />
		<?php endif; ?>
		<jdoc:include type="message" />
		<div id="element-box">
			<div class="m">
				<jdoc:include type="component" />
				<div class="clr"></div>
			</div>
		</div>
		<noscript>
			<?php echo  JText::_('JGLOBAL_WARNJAVASCRIPT') ?>
		</noscript>
	</div>

	<jdoc:include type="modules" name="footer" style="none"  />
</body>
</html>
