<?php defined('_JEXEC') or die('JS: No Direct Access');
echo JoomlaStats_Engine::renderFilters(false, false);
 		$retval = ''
         . "\n<input type=\"hidden\" name=\"objtype\" value=\"\" />"
         . "\n<input type=\"hidden\" name=\"afilter\" value=\"\" />"
		. '<table class="adminlist">'
		. '<thead>'
		. '<tr>'
			. '<th style="width: 1%;">#</th>'
                        . '<th style="width: 1px; text-align: center;">' . JText::_( 'COM_J4AGE_VISITS' ) . '</th>'
                        . '<th style="width: 1px; text-align: center;">' . JText::_( 'COM_J4AGE_PERCENT' ) . '</th>'
                        . '<th style="width: 100%;">' . JText::_( 'COM_J4AGE_BOT_SPIDER' ) . '</th>'
		. '</tr>'
		. '</thead>'
		. "\n"
		;

		if ( count($this->rows) > 0 ) {
			$k = 0;
			$order_nbr = $this->pagination->limitstart;
			for ($i=$order_nbr; ($i<count($this->rows) && $i<($this->pagination->limitstart+$this->pagination->limit)); $i++) {
				$row = $this->rows[$i];
				$order_nbr++;

				$retval .= ''
				. '<tr class="row' . $k . '">'
			  	. '<td style="text-align: right;"><em>'.$order_nbr.'.</em></td>'
				. '<td style="text-align: center;">' . $row->numbers . '</td>'
				. '<td align="left">' . $this->statisticsCommon->getPercentBarWithPercentNbr( $row->numbers, $this->max_value, $this->sum_all_values ) . '</td>'
				. '<td nowrap="nowrap">'
                        . '<a title="' . JText::_( 'COM_J4AGE_DETAILS' )
                	. '" href="javascript:document.adminForm.objtype.value=\'2\';document.adminForm.afilter.value=\'(c.browser_id='. rawurlencode( $row->browser_id ) . ')\';submitbutton(\'visits\');">'
                	. $row->browser
                	. '</a>'
				. '</td>'
				. '</tr>'
				. "\n"
				;

				$k = 1 - $k;
			}
		} else {
                        $retval .= '<tr><td colspan="4" style="text-align:center">'. JText::_( 'COM_J4AGE_NO_DATA' ) . '</td></tr>';
		}

		// TotalLine
		$retval .= ''
		. '<thead>'
		. '<tr>'
		. '<th>&nbsp;</th>'
		. '<th style="text-align: center;">' . $this->sum_all_values . '</th>'
        . '<th>&nbsp;</th>'
        . '<th nowrap="nowrap">'
		. ( ( $this->total == 0) ?
                        ( JText::_('COM_J4AGE_NO_BOTS') )
			:
                       ( $this->total . '&nbsp;' . (($this->total == 1) ? JText::_( 'COM_J4AGE_BOT' ) : JText::_( 'COM_J4AGE_DIFFERENT_BOTS' )))
		  )
        . '</th>'
        . '</tr>'
		. '</thead>'
		. '<tfoot><tr><td colspan="4">'.$this->pagination->getListFooter().'</td></tr></tfoot>'
		. '</table>'
		. "\n"
		;

		echo $retval;
?>


